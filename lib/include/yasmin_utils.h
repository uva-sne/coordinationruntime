/*
 * Copyright (C) 2020 Benjamin Rouxel <benjamin.rouxel@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * \file yasmin_utils.h
 * 
 * \short Contains the utility functions for the library
 */
#ifndef YASMIN_UTILS_H
#define YASMIN_UTILS_H

#ifdef __cplusplus
extern "C" {
#endif

#if defined(_DEBUG) || defined(_lint)
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#endif

#include <errno.h>
#include <time.h>    
#include <limits.h>
#include <stdint.h>
    
#include "yasmin_config.h"
#include "check_config.h"

/**
 * Conveninent boolean time
 */
#ifdef __cplusplus
typedef bool ybool;
#ifndef TRUE
#   define TRUE true
#endif
#ifndef FALSE
#   define FALSE false
#endif
#else
typedef short ybool;
#ifndef TRUE
#   define TRUE 1
#endif
#ifndef FALSE
#   define FALSE 0
#endif
#endif

#ifdef __cplusplus
#   define STR_HELPER(X) #X
#   define STRINGIFY(X) (char*) STR_HELPER(X)
#else
    #define STRINGIFY(X) #X
#endif

/**
 * Conveninent alias for the type any time related variables
 */
typedef unsigned long long yas_time_t;

/**
 * Convenient alias for the priority type. To simplify some algorithms when using
 * EDF priority assignment, the time is used which allows larger value than
 * regular priorities.
 */
#if defined(__INTERNAL_YAS_DYN_PRIO) && defined(__INTERNAL_YAS_PRIO_EDF)
typedef yas_time_t yas_priority_t;
#else
typedef uint32_t yas_priority_t;
#endif

/**
 * The smaller the priority number is, the higher is the actual priority
 */
#define YAS_HIGHEST_PRIORITY 0
#define YAS_LOWEST_PRIORITY (yas_priority_t)-1
#define yas_is_higher_priority(X, Y) (X) < (Y)
#define yas_decrease_priority(X) ++X

#if defined(_DEBUG) || defined(_lint)
#define posix_check_err(err, name) {   \
  int error = (err);        \
  if (error)           \
    (void)printf ("file %s: line %d: error '%s' during '%s'\n",  \
           __FILE__, __LINE__,    \
           strerror (error), name);     \
  }

#define y_assert(X) if(X) {\
   (void)printf ("file %s: line %d: error '%s'\n",  \
           __FILE__, __LINE__,     \
           #X);     \
   return ; }

#define y_assert_msg(X, M) if(X) {\
   (void)printf ("file %s: line %d: error '%s'",  \
           __FILE__, __LINE__,    \
           #X);     \
   (void)printf M;\
   return ; }

#define y_assert_return_val(X,V) if(X) {\
   (void)printf ("file %s: line %d: error '%s'\n",  \
           __FILE__, __LINE__,    \
           #X);     \
   return V; }

#define y_assert_return_val_msg(X,V, M) if(X) {\
   (void)printf ("file %s: line %d: error '%s'\n",  \
           __FILE__, __LINE__,    \
           #X);     \
   (void)printf M;\
   return V; }


#define DEBUG_MSG(args) (void)printf args ; (void)printf("\n");   

#else
#define posix_check_err(err, ...) if(err){;}

#define y_assert(X) if(X) {return ; }

#define y_assert_msg(X,...) if(X) { return ; }

#define y_assert_return_val(X,V) if(X) { return V; }

#define y_assert_return_val_msg(X,V,...) if(X) { return V; }

#define DEBUG_MSG(...) 

#endif /* defined(_DEBUG) || defined(_lint) */

#define UNUSED_ARGUMENT(x) (void)(x)

/* I don't know why, but setting this to be a RT signal causes a segfault. */
#define PREEMPTION_SIGNAL SIGUSR1

#define YAS_RESOURCE_NO_RESOURCE_USED -1

#define DECLARE_VALID_ARRAY(T,A,S,I, Msg) T A[] = I; \
    ((void)sizeof(char[1 - 2*!!(sizeof(A) != S*sizeof(T))]), (void)sizeof(Msg))

/**
 * Return the current time according to the configured clock 
 * 
 * Default clock is CLOCK_MONOTONIC
 * Time is return in nanosecond
 * 
 * @return time in nanosecond
 */
extern yas_time_t yas_utils_now(void);
/**
 * Compares two time values
 * 
 * @param a
 * @param b
 * @return 1 if a > b ; 0 if a == b ; -1 if a < b
 */
extern short yas_utils_cmp_time(const yas_time_t a, const yas_time_t b);

/**
 * Perform a sleep for a duration given in nanoseconds
 * 
 * @param wait sleep duration
 */
void yas_utils_nanosleep(yas_time_t wait);

/**
 * Compute the GCD of 2 values
 * 
 * GCD: Greatest Common Divisor
 * 
 * @param a
 * @param b
 * @return 
 */
yas_time_t y_utils_gcd_time(yas_time_t a, yas_time_t b);
/**
 * Compute the GCD of a set of values
 * 
 * @param array - list of values
 * @param size - size of the list
 * @return 
 */
yas_time_t y_utils_gcd_arr_time(const yas_time_t *array, const size_t size);
/**
 * Compute the LCM of a set of values
 * 
 * LCM: Least Common Multiplier
 * 
 * @param array
 * @param size
 * @return 
 */
yas_time_t y_utils_lcm_arr_time(const yas_time_t *array, const size_t size);

/**
 * Alias to easily fill up a char* with a given string in C
 */
#define yas_build_task_name(varname, name) {\
    size_t __i__; \
    char __name__[] = name; \
    for(__i__=0 ; __i__ < sizeof(__name__) && __i__ < sizeof(varname) ; ++__i__) {varname[__i__] = __name__[__i__];}\
}

#ifdef __cplusplus
}
#endif

#endif /* YASMIN_UTILS_H */

