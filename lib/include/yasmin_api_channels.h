/*
 * Copyright (C) 2020 Benjamin Rouxel <benjamin.rouxel@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * \file yasmin_api_channels.h
 * 
 * \short Contains the API declarations allowing two tasks to communicate
 * through a FIFO channel
 * 
 * FIFO channels connect dependent tasks that exchange data. The principle is
 * dataflow (data-driven) oriented, which means that the consumer can not be
 * fired before the producer has finished sending all its elements.
 * 
 * To simplify further documentation, the elements stored in a buffer are called
 * tokens.
 * 
 * The FIFO channels are implemented with circular bounded buffers. It is the 
 * responsability of the user to well dimension its channels as no specific
 * checks have been added to avoid/lock overwritting unconsumed data. In fact,
 * it would be extremely bad for application with timing guarantee to add 
 * semaphores as the waiting time would need to be bounded in order to guarantee
 * the respect of the timing constraints.
 * Following the principle of dataflow programming, we advocate for a preliminary
 * study of the buffer requirements of each task.
 * Note: I tried to implement lock-free mechanism, but didn't reach a state
 * satisfactory yet
 * 
 * Buffers are statically allocated in memory. This avoid the timing hazard
 * brought by dynamic allocation which is not desirable for real-time application.
 * 
 * This file describes a lot of macros to manipulate channels. This choice is
 * motivated by the lack of an easy way to have polymorphism with C. Thus,
 * by using macros, we can generate code adapted for the data type stored in the
 * channel.
 * Most of the macros are only internal simplification that shouldn't be used 
 * by the user. Instead, more simple, one line macros are available at the end of
 * the file.
 */

#ifndef YAS_API_CHANNELS_H
#define YAS_API_CHANNELS_H

#ifdef __cplusplus
extern "C" {
#endif
    
#ifdef YAS_CHANNEL_SIZE
    
/**
 * Type to describe a channel
 *  (size=4)  _______ 
 *    src => |_|_|_|_| => sink
 *            ^   ^
 *            T   H => 2 tokens are ready for consumption
 */
struct yas_intern_task_s;
typedef struct {
   struct yas_intern_task_s *src; //! task source producing tokens 
   struct yas_intern_task_s *sink; //! task sink consuming tokens
   size_t head; //! index in which to add the next token
   size_t tail; //! index in which to get the next unconsumed token
   size_t size; //! size of the buffer
   /*c_mutex m; */
} base_channel_t;

/**
 * Initialise the given channel
 * \param b the buffer to initialise
 * \param qty the size of the buffer
 */
void y_channel_init(base_channel_t *b, size_t qty) ;

/**
 * Close a channel. After the cleanup of a channel, it must not be used or
 * the outcome might be unpredictable.
 */
void y_channel_cleanup(base_channel_t *b);

/**
 * Check if there are tokens ready for consumption that haven't been consumed.
 * 
 * \param b the reference of a channel
 * \return false if the channel is empty
 */
ybool y_channel_are_tokens_available(base_channel_t *b) ;

/**
 * Connect 2 tasks to each side of the channel
 * 
 * \param src the id of the produceur task
 * \param sink the id of the consumer task
 * \param channel pointer to the created channel linking the task
 * 
 * \note the code is dependent of the scheduler implementation
 */
void y_channel_connect(yas_task_id_t src, yas_task_id_t sink, base_channel_t *channel);
    
/**
 * Macro to actually define a channel of a given type. 
 * 
 * The macro will declare a new type `buffer_name_t' describing the specific
 * channel, and a variable `name' to access the channel.
 * 
 * \param name unique id for the channel
 * \param type of data to store
 * \param size the amount of tokens contained in the channel
 */
#define y_declare_channel(name, type, size) \
typedef struct { \
   base_channel_t base; \
   type buffer[size+1]; \
} buffer_ ## name ## _t;\
buffer_ ## name ## _t name;

/**
 * This macro generates the code to initialise a channel
 */
#define y_declare_init(name, size) \
static inline void y_channel_init_ ## name() { \
    y_channel_init((base_channel_t*)&name, size); \
}
/**
 * This macro generates the code to pop a token from a channel
 */
#define y_declare_pop(name, type,size) \
static inline void y_pop_ ## name(type *res) {\
    /*c_mutex_wait(&name.m);*/\
    if(name.base.tail == name.base.head) return;\
    *res = (name.buffer)[name.base.tail];\
    name.base.tail = (name.base.tail+1)%(size+1);\
    /*c_mutex_post(&name.m);*/\
}
/**
 * This macro generates the code to push a token from a channel
 */
#define y_declare_push(name, type, size) \
static inline void y_push_ ## name(type val) { \
    /*c_mutex_wait(&name.m); */\
    (name.buffer)[name.base.head] = val; \
    name.base.head = (name.base.head+1)%(size+1); \
    if(name.base.tail == name.base.head) \
        name.base.tail = (name.base.tail+1)%(size+1);\
    /*c_mutex_post(&name.m); */\
}

/**
 * Simplify macro that will allow a user to finally declare a channel
 * 
 * \param name unique id of the channel
 * \param type of data stored in it
 * \param size of the channel
 */
#define yas_channel_decl(name, type, size) \
    y_declare_channel(name, type, size) \
    y_declare_init(name, size)\
    y_declare_push(name, type, size) \
    y_declare_pop(name, type, size)
    
/**
 * Simplify macro that will allow a user to connect 2 tasks through a channel
 * \param src identifier of the producer task as returned by yas_task_create
 * \param sink identifier of the consumer task as returned by yas_task_create
 * \param name the unique id of the channel
 */
#define yas_channel_connect(src, sink, name)\
    y_channel_init_ ## name ();\
    y_channel_connect(src, sink, (base_channel_t*)&name)

/**
 * Simplify macro that will allow a user to push a token in a channel
 * \param name the unique id of the channel
 * \param value the value to push
 */
#define yas_channel_push(name, value) \
    y_push_ ##name(value)
/**
 * Simplify macro that will allow a user to pop a token in a channel
 * \param name the unique id of the channel
 * \param value variable in which to store the token
 */
#define yas_channel_pop(name, value) \
    y_pop_ ##name(&(value))

#endif /* YAS_CHANNEL_SIZE */ 
    
#ifdef __cplusplus
}
#endif

#endif /* YAS_API_CHANNELS_H */

