/*
 * Copyright (C) 2020 Benjamin Rouxel <benjamin.rouxel@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * \file constants.h
 * 
 * \short Contains predifined constants to place in yasmin_config.h
 * 
 */
#ifndef CONSTANTS_H
#define CONSTANTS_H

// Use with YAS_PRIORITY_ASSIGNMENT
#define YAS_TASK_PRIORITY_RM                        1 
#define YAS_TASK_PRIORITY_DM                        2
#define YAS_TASK_PRIORITY_FIFO                      3
#define YAS_TASK_PRIORITY_USERPRIO                  4
/*  Keep fixed priority scheme < dynamic priority scheme */
/*  and keep USERPRIO as the last fixed priority */
#define YAS_TASK_PRIORITY_EDF                       5

// Use with YAS_TASK_MAPPING
#define YAS_MAPPING_GLOBAL_SCHEME                   6
#define YAS_MAPPING_PARTITIONED_SCHEME              7
#define YAS_MAPPING_STATIC_SCHEME                   8

/*  determine if the scheduler task should be handle with an ISR/alarm or within */
/*    a dedicated thread with similar property as the main */
// Use with YAS_SCHEDULER_IMPLEM
#define YAS_SCHED_IMPLEM_ISR                        9
#define YAS_SCHED_IMPLEM_THR                        10

// Use with YAS_PREEMPTION_MODEL
#define YAS_SCHED_PREEMPTIVE                        11
#define YAS_SCHED_NONPREEMPTIVE                     12

// Use with YAS_VERSION_SELECTION
#define YAS_VERSION_SELECT_TRIVIAL                  13 // should have the lowest number
#define YAS_VERSION_SELECT_LAXITY                   14
#define YAS_VERSION_SELECT_ENERGY                   15
#define YAS_VERSION_SELECT_ENERGYTIME_TRADEOFF      16
#define YAS_VERSION_SELECT_EXEC_MODE                17
#define YAS_VERSION_SELECT_PERMISSION               18
#define YAS_VERSION_SELECT_USERDEF                  19 //should have the highest number

// Use with YAS_TASK_RECURRING_MODEL
#define YAS_TASK_RECMODEL_SPORADIC                  20
#define YAS_TASK_RECMODEL_PERIODIC                  21

#endif /* CONSTANTS_H */

