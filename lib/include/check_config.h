/*
 * Copyright (C) 2020 Benjamin Rouxel <benjamin.rouxel@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * \file check_config.h
 * 
 * \short Check yasmin_config.h
 * 
 * Check if the given configuration via yasmin_config.h is sound
 */
#ifndef CHECK_CONFIG_H
#define CHECK_CONFIG_H

#ifndef YAS_HYPERPERIOD
/*An error will be printed when trying to start the schedule with a suggestion for the value*/
#   define YAS_HYPERPERIOD 0
#endif

#ifndef YAS_THREAD_SIZE
#define YAS_THREAD_SIZE 1
#endif /* YAS_THREAD_SIZE */

#ifndef YAS_THREAD_AFFINITIES
#   define YAS_THREAD_AFFINITIES {0}
#endif /* YAS_THREAD_AFFINITIES */

#ifndef YAS_THREAD_PRIORITIES
#   define YAS_THREAD_PRIORITIES {0}
#endif /* YAS_THREAD_PRIORITIES */

#ifndef YAS_THREAD_NAME
#   define YAS_THREAD_NAME "pool"
#endif /* YAS_THREAD_NAME */

//#ifndef YAS_MAIN_THREAD_AFFINITY
//#   warning "You can specify an affinity for the main thread using YAS_MAIN_THREAD_AFFINITY"
//#endif
//
//#ifndef YAS_SCHEDULER_THREAD_AFFINITY
//#   warning "You can specify an affinity for the main thread using YAS_SCHEDULER_THREAD_AFFINITY"
//#endif
//
//#ifndef YAS_MAIN_THREAD_PRIORITY
//#   define YAS_MAIN_THREAD_PRIORITY 1
//#endif
//
//#ifndef YAS_SCHEDULER_THREAD_PRIORITY
//#   define YAS_SCHEDULER_THREAD_PRIORITY 1
//#endif

#ifndef YAS_TASK_MAPPING
#   define YAS_TASK_MAPPING YAS_MAPPING_GLOBAL_SCHEME
#endif /* YAS_TASK_MAPPING */

#ifndef YAS_CLOCK
#   define YAS_CLOCK CLOCK_MONOTONIC
#endif /* YAS_CLOCK */

#if YAS_TASK_MAPPING != YAS_MAPPING_STATIC_SCHEME
#   ifndef YAS_ALARM_SIGNAL
#       define YAS_ALARM_SIGNAL SIGRTMIN + 0
#   endif /* YAS_ALARM_CLOCK */
#endif /* YAS_THREAD_SCHEDULING != YAS_MAPPING_STATIC_SCHEME */

#ifndef YAS_THREAD_SCHED_POLICY
#   define YAS_THREAD_SCHED_POLICY SCHED_OTHER
#endif /* YAS_THREAD_SCHED_POLICY */

#if YAS_TASK_MAPPING != YAS_MAPPING_STATIC_SCHEME
#   ifndef YAS_PRIORITY_ASSIGNMENT
#       define YAS_PRIORITY_ASSIGNMENT YAS_TASK_PRIORITY_EDF
#   endif /* YAS_PRIORITY_ASSIGNMENT */
#endif /* YAS_THREAD_SCHEDULING != YAS_THREAD_SCHEDULING_STATIC_SCHEME */

#if YAS_TASK_MAPPING != YAS_MAPPING_STATIC_SCHEME
#   ifndef YAS_PERIODIC_TASK_SIZE
#       define YAS_PERIODIC_TASK_SIZE 0
#   endif /* YAS_PERIODIC_TASK_SIZE */
#endif /* YAS_THREAD_SCHEDULING != YAS_THREAD_SCHEDULING_STATIC_SCHEME */

#ifndef YAS_TASK_POOL_JOBS_SIZE
#    error "YAS_TASK_POOL_JOBS_SIZE is not defined in yasmin_config.h"
#endif /* YAS_TASK_POOL_JOBS_SIZE */

#if YAS_TASK_MAPPING != YAS_MAPPING_STATIC_SCHEME
#   ifdef YAS_SCHEDULER_IMPLEM 
#       if YAS_SCHEDULER_IMPLEM != YAS_SCHED_IMPLEM_ISR && YAS_SCHEDULER_IMPLEM != YAS_SCHED_IMPLEM_THR
#           error "YAS_SCHEDULER_IMPLEM should be YAS_SCHED_IMPLEM_ISR or YAS_SCHED_IMPLEM_THR"
#       endif
#   else 
#       define YAS_SCHEDULER_IMPLEM YAS_SCHED_IMPLEM_ISR
#   endif
#endif

#ifndef YAS_SYNCHRO_FORCE_POSIX
 #   warning "Lock Free implementation is not ready" 
 #   define YAS_SYNCHRO_FORCE_POSIX 
#endif

#if YAS_TASK_MAPPING != YAS_MAPPING_STATIC_SCHEME
#   ifndef YAS_TASK_VERSION_SIZE
#       define __INTERNAL_TASK_VERSION_SIZE 1
#   elif YAS_TASK_VERSION_SIZE > 0
#       define __INTERNAL_TASK_VERSION_SIZE YAS_TASK_VERSION_SIZE
#   else
#       error "YAS_TASK_VERSION_SIZE should be > 0"
#   endif
#else 
#   define __INTERNAL_TASK_VERSION_SIZE 1
#endif

#if __INTERNAL_TASK_VERSION_SIZE > 1 && defined(YAS_VERSION_SELECTION)
#   if YAS_VERSION_SELECTION < YAS_VERSION_SELECT_TRIVIAL || YAS_VERSION_SELECTION > YAS_VERSION_SELECT_USERDEF
#       error "Unknown variant selection scheme for YAS_VERSION_SELECTION, check constants.h for the list"
#   endif
#   define __INTERNAL_VERSION_SELECT YAS_VERSION_SELECTION
#else
#   define __INTERNAL_VERSION_SELECT YAS_VERSION_SELECT_TRIVIAL
#endif

#if YAS_TASK_MAPPING != YAS_MAPPING_STATIC_SCHEME
#   ifdef YAS_PREEMPTION_MODEL
#       if YAS_PREEMPTION_MODEL != YAS_SCHED_PREEMPTIVE && YAS_PREEMPTION_MODEL != YAS_SCHED_NONPREEMPTIVE
#           error "YAS_PREEMPTION_MODEL should be set to YAS_SCHED_PREEMPTIVE or YAS_SCHED_NONPREEMPTIVE"
#       endif
#   else
#       define YAS_PREEMPTION_MODEL YAS_SCHED_PREEMPTIVE
#   endif
#else 
#   define YAS_PREEMPTION_MODEL YAS_SCHED_NONPREEMPTIVE
#endif

#if YAS_TASK_MAPPING == YAS_MAPPING_GLOBAL_SCHEME
#   define __INTERNAL_YAS_ONLINE_SCHED
#elif YAS_TASK_MAPPING == YAS_MAPPING_PARTITIONED_SCHEME
#   define __INTERNAL_YAS_ONLINE_SCHED
#elif YAS_TASK_MAPPING == YAS_MAPPING_STATIC_SCHEME
#   define __INTERNAL_YAS_OFFLINE_SCHED
#endif

#if YAS_PRIORITY_ASSIGNMENT <= YAS_TASK_PRIORITY_USERPRIO
#   define __INTERNAL_YAS_FIX_PRIO
#   if YAS_PRIORITY_ASSIGNMENT == YAS_TASK_PRIORITY_RM
#      define __INTERNAL_YAS_PRIO_RM
#   elif YAS_PRIORITY_ASSIGNMENT == YAS_TASK_PRIORITY_DM
#      define __INTERNAL_YAS_PRIO_DM
#   elif YAS_PRIORITY_ASSIGNMENT == YAS_TASK_PRIORITY_FIFO
#      define __INTERNAL_YAS_PRIO_FIFO
#   elif YAS_PRIORITY_ASSIGNMENT == YAS_TASK_PRIORITY_USERPRIO
#      define __INTERNAL_YAS_PRIO_USER
#   elif !defined(__INTERNAL_YAS_OFFLINE_SCHED)
#      error "Unknown priority assignment in YAS_THREAD_PRIORITY_ASSIGNMENT"
#   endif
#else
#   define __INTERNAL_YAS_DYN_PRIO
#   if YAS_PRIORITY_ASSIGNMENT == YAS_TASK_PRIORITY_EDF
#      define __INTERNAL_YAS_PRIO_EDF
#   elif !defined(__INTERNAL_YAS_OFFLINE_SCHED)
#      error "Unknown priority assignment in YAS_PRIORITY_ASSIGNMENT"
#   endif
#endif

#ifndef YAS_NONRECURRING_TASK_SIZE
#   define YAS_NONRECURRING_TASK_SIZE 1 /*Avoid declaration of array with size 0*/
#elif YAS_NONRECURRING_TASK_SIZE < 1
#   error "YAS_NONRECURRING_TASK_SIZE can't be inferior to 1"
#endif

#if defined(YAS_CHANNEL_SIZE) && YAS_CHANNEL_SIZE < 1
#   error "YAS_CHANNEL_SIZE can't be inferior to 1"
#endif

#ifndef YAS_TASK_RECURRING_MODEL
#   define YAS_TASK_RECURRING_MODEL YAS_TASK_RECMODEL_SPORADIC
#elif YAS_TASK_RECURRING_MODEL != YAS_TASK_RECMODEL_SPORADIC && YAS_TASK_RECURRING_MODEL != YAS_TASK_RECMODEL_PERIODIC
#   error "YAS_TASK_RECURRING_MODEL can be YAS_TASK_RECMODEL_SPORADIC or YAS_TASK_RECMODEL_PERIODIC"
#endif

#if __INTERNAL_VERSION_SELECT == YAS_VERSION_SELECT_TRIVIAL
#   define YAS_VERSION_SELECT_FUNCTION y_task_version_select_trivial
#elif __INTERNAL_VERSION_SELECT == YAS_VERSION_SELECT_LAXITY
#   define YAS_VERSION_SELECT_FUNCTION y_task_version_select_laxity
#elif __INTERNAL_VERSION_SELECT == YAS_VERSION_SELECT_ENERGY
#   define YAS_VERSION_SELECT_FUNCTION y_task_version_select_energy
#elif __INTERNAL_VERSION_SELECT == YAS_VERSION_SELECT_ENERGYTIME_TRADEOFF
#   define YAS_VERSION_SELECT_FUNCTION y_task_version_select_energytime_tradeoff
#elif __INTERNAL_VERSION_SELECT == YAS_VERSION_SELECT_EXEC_MODE
#   define YAS_VERSION_SELECT_FUNCTION y_task_version_select_exec_mode
#elif __INTERNAL_VERSION_SELECT == YAS_VERSION_SELECT_PERMISSION
#   define YAS_VERSION_SELECT_FUNCTION y_task_version_select_permission
#elif __INTERNAL_VERSION_SELECT == YAS_VERSION_SELECT_USERDEF
#   ifndef YAS_VERSION_SELECT_FUNCTION
#       error "If you want to use your own function to select variant, please provide its name by defining macro YAS_VERSION_SELECT_FUNCTION"
#   endif
#endif

#endif /* CHECK_CONFIG_H */

