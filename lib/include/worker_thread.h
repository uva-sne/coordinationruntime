/*
 * Copyright (C) 2020 Benjamin Rouxel <benjamin.rouxel@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * \file worker_thread.h
 * 
 * \short Worker threads are containers to execute tasks, like virtual cores.
 * 
 * The pool of threads contains as many threads as stated in yasmin_config.h, and 
 * execute with a system priority, on a specific core also given in the yasmin_config.h.
 * They should have a high priority in the system in addition to a system RT
 * scheduling policy to guarantee the timing of the end-user application.
 * 
 * There is only one thread pool per executing application. 
 * 
 * The worker threads are containers to execute the tasks and can be seen as 
 * virtual cores from the task point of view. Tasks are therefore mapped to threads
 * (or virtual cores) and not to physical cores. 
 * 
 * \warning the end-user core is therefore not supposed to create other threads.
 * 
 * Threads pick the highest priority task in a ready queue. The queue depends on
 * the mapping, it is a global shared queue for all threads+scheduler in case of
 * a mapping, or each thread has its own queue (only shared with the scheduler) 
 * in case of partitioned mapping.
 * The decision in which queue tasks are picked is done by `task_core_mapper.h'.
 * 
 * In case of partitioned mapping (or off-line scheduling), then the task is mapped
 * to a virtual core identified by an id. This id is the index in the array 
 * containing threads in the pool. Therefore the virtual core id=1 does not
 * neccessarily mapped to physical core with system id=1.
 * 
 * Worker threads can preempt a task (if preemption are activated through 
 * yasmin_config.h). After adding some tasks in the ready queue, the scheduler 
 * sends a signal to the threads which enter a signal handler routine. This
 * routine checks if the head of the ready queue has a higher priority than the
 * currently executing task, then the execution context is stored on the stack
 * and the context is switched to the higher priority task.
 * 
 * \todo place the preempted task in the stack instead of keeping in the structure
 */
#ifndef __WORKER_THREAD_H__
#define __WORKER_THREAD_H__

#include <signal.h>
#include <ucontext.h>

#include "yasmin_config.h"
#include "check_config.h"
#include "yasmin_utils.h"
#include "job_pool.h"
#include "threading.h" 
#include "trace.h"
#include "synchronisation.h"

#ifdef __cplusplus
extern "C" {
#endif
    
/**
 * Describe a worker thread
 * 
 * The ready queue pool is added in the structure only for partitioned mapping
 */
typedef struct {
#if YAS_TASK_MAPPING != YAS_MAPPING_GLOBAL_SCHEME
  y_task_pool_t queue; //! ready queue for jobs
#endif
  y_system_thread system_thread; //! system thread instance, e.g. pthread
  size_t thid; //! id of the thread, is equal to its index in the y_thread_pool_t structure
  int affinity; //! system core on which the thread is mapped
  yas_intern_task_t *current_task; //! task currently executing in the thread
  volatile yas_intern_task_t preempted_task; //! preempted task
} y_worker_thread_t;

/**
 * Describe the thread pool
 * 
 * The ready queue pool is added in the structure only for global mapping
 */
typedef struct {
#if YAS_TASK_MAPPING == YAS_MAPPING_GLOBAL_SCHEME
    y_task_pool_t queue; //! ready queue for jobs
#endif
    ybool running; //! TRUE if the thread pool is actively running
    y_worker_thread_t threads[YAS_THREAD_SIZE]; //! stores the worker threads
} y_thread_pool_t;

/**
 * Initialise the thread pool as a constructor would do
 */
void y_thread_pool_new(void);

/**
 * Initialise the thread pool as a destructor would do
 */
void y_thread_pool_free(void);

/**
 * Join all threads
 * 
 * Place special jobs with the highest possible priority into the ready queue.
 * It has the effect of:
 * - Waking up all "waiting for task" 
 * - If preemption is activated, the current task is preempted
 * 
 * A special job is then popped by each thread from the ready queue, which leads
 * to the terminaison of each thread. The application can then by stopped after
 * that.
 */
void y_thread_pool_join_all(void);

/**
 * Push a task into the ready queue
 * 
 * @param data
 */
extern void y_thread_pool_push(yas_intern_task_t *data);
/**
 * The scheduler notifies the thread that it doesn't have anymore task to add
 * in the queue, and that thread might want to check if there is a higher 
 * priority than the one executing if preemption are enabled.
 */
extern void y_worker_thread_sched_notify(void);
/**
 * Lock the ready queue
 */
extern void y_thread_pool_lock_queue(void);
/**
 * Lock the ready queue
 */
extern void y_thread_pool_unlock_queue(void);

/**
 * Block all signals
 * 
 * When signals are blocked, no signal are received by the thread. This allows to
 * *not* enter the preemption routine in case the thread is not executing a task
 * (i.e. busy with some other internal stuffs).
 * 
 * Therefore, signals are blocked after the completion of a task.
 */
void y_thread_pool_block_signals(void);

/**
 * Unblock signals, or activate signals
 * 
 * Activate signals before starting the execution of a task, to enable preemption.
 */
void y_thread_pool_unblock_signals(void);

/**
 * Preempt current task, and execute the one given in func
 * Order of parameter simplifies the underlying implementation
 * Implemented in ASM in arch/../preempt.S
 * @param data
 * @param func
 */
extern void y_thread_pool_preempt(volatile void *data, yas_task_func_t func);
#ifdef __cplusplus
}
#endif
#endif /* __WORKER_THREAD_H__ */
