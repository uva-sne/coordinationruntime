/*
 * Copyright (C) 2020 Benjamin Rouxel <benjamin.rouxel@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * \file job_pool.h
 * 
 * \short Contains functions to manage a ready queue of jobs
 * 
 * The queue is implemented with a Binary Heap, see https://en.wikipedia.org/wiki/Binary_heap
 * 
 * The root of the tree is at index 1 (not 0) in the array. This allows to 
 * optimise the algorithms, by using right-/left- shifts instead of division/multiplication.
 * 
 * The sorting key of each element is the priority, the root of the heap has the lowest
 * key, which is the highest priority.
 * 
 * \warning The next waiting task is executed in the same worker thread as the
 * one releasing the resource. While this behaviour is OK for global scheduling
 * it can lead to miss-placement for partitioned approach.
 * 
 * \warning The big problem with this job pool is the static allocation. If there
 * are some deadline miss, then the job pool tends to grow fast. When it reaches
 * its full, and there is a new task to add, there will be a SEGFAULT provocated
 * by a division by 0.
 */
#ifndef JOBPOOL_H
#define JOBPOOL_H

#include <ucontext.h>

#include "yasmin_config.h"
#include "check_config.h"
#include "yasmin_utils.h"
#include "locking.h"
#include "trace.h"

#include "task.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * should be at least the maximum number of jobs actives at the same time + non-periodic tasks + NB_Threads ending task
 */
#define Y_TASK_POOL_QUEUE_SIZE (YAS_TASK_POOL_JOBS_SIZE + YAS_THREAD_SIZE + 1)

/**
 * Describe a element in the binary tree
 */
typedef struct s_qType {
    yas_intern_task_t *data;
    yas_priority_t key;
} y_tpool_elt_t;

/**
 * Describe the binary tree
 */
typedef struct {
    volatile y_tpool_elt_t q_elt_pool[Y_TASK_POOL_QUEUE_SIZE]; //! the binary heap tree
    volatile uint32_t q_elt_pool_cnt; //! count how many jobs have been added so far
    
    y_sem_t cancons; //! a counting semaphore allowing threads to pop jobs if the tree is not empty
    y_mutex_t mutex; //! ensure the mutual exclusion accesses to the list
} y_task_pool_t;

/**
 * Initialise a binary heap as a constructor would do
 * @param pool
 */
void y_job_pool_init(volatile y_task_pool_t *pool);
/**
 * De-initialise a binary heap as a destructor would do.
 * If a binary tree is accessed after calling this function, the result is 
 * unpredictable.
 * @param pool
 */
void y_job_pool_cleanup(volatile y_task_pool_t *pool);

/**
 * Push a job in the queue. The queue must have been a priori 
 * unlocked (with y_job_pool_unlock_queue). This allows the scheduler to push a 
 * bunch of task at once without locking/unlocking for each task.
 * 
 * \warning the queue must be unlocked/locked by the caller
 * 
 * @param pool
 * @param job
 */
void y_job_pool_push_unlocked(volatile y_task_pool_t *pool, yas_intern_task_t *job);

/**
 * Pop the root of the heap
 * 
 * If no task are in the heap, the thread is blocked on the `cancons' semaphore.
 * The thread can also block on the mutex to access the queue.
 * 
 * @param pool
 * @param job
 */
void y_job_pool_pop(volatile y_task_pool_t *pool, yas_intern_task_t * volatile *job);

/**
 * Pop the root of the heap
 * 
 * No blocking occurs if the queue is already being accessed, or there is no job
 * in it, then `job' is set to NULL, and the function returns immediately.
 * 
 * @param pool
 * @param job
 */
void y_job_pool_pop_nonblock(volatile y_task_pool_t *pool, yas_intern_task_t * volatile *job);

/**
 * Pop the root of the heap if it has a higher priority than the given one.
 * 
 * No blocking occurs as `y_job_pool_pop_nonblock'.
 * 
 * \remark highest priority is a lower value
 * 
 * @param pool
 * @param priority
 * @param job
 */
void y_job_pool_pop_nonblock_if_higher_priority(volatile y_task_pool_t *pool, yas_priority_t priority, yas_intern_task_t * volatile *job);

/**
 * Lock the queue with the mutex
 * 
 * @param pool
 */
void y_job_pool_lock_queue(volatile y_task_pool_t *pool);

/**
 * Unlock the mutex
 * 
 * @param pool
 */
void y_job_pool_unlock_queue(volatile y_task_pool_t *pool);

/**
 * Return the priority of the root node of the heap
 * 
 * @param pool
 * @return 
 */
size_t y_job_pool_head_priority(const volatile y_task_pool_t *pool);

/**
 * Return the size of the heap, how many jobs are in
 * 
 * @param pool
 * @return 
 */
uint32_t y_job_pool_length(const volatile y_task_pool_t *pool);

#ifdef __cplusplus
}
#endif
#endif /* JOBPOOL_H */

