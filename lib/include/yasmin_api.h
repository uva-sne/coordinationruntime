/*
 * Copyright (C) 2020 Benjamin Rouxel <benjamin.rouxel@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * \file yasmin_api.h
 * 
 * \short Describe the general available API
 */
#pragma once
#ifndef YAS_API_H
#define YAS_API_H

#ifdef __cplusplus
#include <string>
extern "C" {
#endif
    
#include "yasmin_config.h"
#include "check_config.h"
#include "yasmin_utils.h"
    
/**
 * Describe the information the user must/can provide for a task depending on 
 * the targeted scheduling policy.
 */
typedef struct {
    char name[256]; //! identifier of the task
    size_t priority; //! its priority, in case of static+user-defined prio
    int virt_core_id; //! the identifier of the core in which execute the task, in case of a partitioned scheduling
    yas_time_t deadline; //! the deadline to meet, default the period
    yas_time_t period; //! the period of the task
    yas_time_t offset; //! the starting offset, default: 0
} yas_task_data_t;

/**
 * Alias type for the unique id of a task
 */
typedef int yas_task_id_t;
/**
 * Alias type for the unique id of a version
 */
typedef int yas_version_id_t;
/**
 * Alias type for the function to call in order to execute the task
 */
typedef void   (*yas_task_func_t)          (void *user_data);

/**
 * Alias type for an accelerator, if the use of accelerators is requested
 */
typedef int yas_hwaccel_id_t;
    
/**
 * Depending on the method configured by the user to chose the correct version
 * for each task, the alias type `yas_version_select_t' will differ.
 * This is required for a proper type checking at compilation.
 * This is required as the information required to select a version are not the
 * same according of the method, e.g. some energy budget and a user defined 
 * function is compulsory when selecting a version based on its energy consumption
 * while it is not when selection a version on its execution duration.
 * 
 * @todo needs to be simplified
 */
#   if __INTERNAL_VERSION_SELECT == YAS_VERSION_SELECT_TRIVIAL
typedef void* yas_version_select_t;
#   elif __INTERNAL_VERSION_SELECT == YAS_VERSION_SELECT_LAXITY
typedef struct {
    uint64_t WCET;
} yas_version_select_t;
#   elif __INTERNAL_VERSION_SELECT == YAS_VERSION_SELECT_ENERGY
typedef uint64_t (*func_battery) ();
typedef struct {
    uint64_t energy_budget;
    func_battery get_battery_status;
} yas_version_select_t;
#   elif __INTERNAL_VERSION_SELECT == YAS_VERSION_SELECT_ENERGYTIME_TRADEOFF
typedef uint64_t (*func_battery) ();
typedef struct {
    uint64_t WCET;
    uint64_t energy_budget;
} yas_version_select_t;
#   elif __INTERNAL_VERSION_SELECT == YAS_VERSION_SELECT_EXEC_MODE
typedef int32_t (*func_mode) ();
typedef struct {
    int32_t exec_mode;
    func_mode get_current_exec_mode;
} yas_version_select_t;
#   elif __INTERNAL_VERSION_SELECT == YAS_VERSION_SELECT_PERMISSION
typedef ybool (*func_permission) (uint16_t);
typedef struct {
    uint16_t permission;
    func_permission is_allowed;
} yas_version_select_t;
#   elif __INTERNAL_VERSION_SELECT == YAS_VERSION_SELECT_USERDEF
#       ifndef YAS_VERSION_SELECT_PROPERTIES_TYPE
#           error "If you want to use your own function to select variant, you also need to provide a type name that contains information to discriminate variants by defining macro YAS_VERSION_SELECT_PROPERTIES_TYPE"
#       endif
typedef YAS_VERSION_SELECT_PROPERTIES_TYPE yas_version_select_t;
#   endif

/*******************************************************************************/
/*          Global & common API                                                */
/*******************************************************************************/

/**
 * First function to call in order to initialise the library at runtime
 */
void yas_init(void);
/**
 * Last function to call to cleanup any trace left by the library
 */
void yas_cleanup(void);

/**
 * This function starts the user application scheduling
 * 
 * @return true if the schedule start, false otherwise
 */
ybool yas_start(void);
/**
 * This function stops the user application. 
 * 
 * Tasks currently executing are not preempted. However, tasks waiting in the 
 * ready queue are not executed and the ready queue is emptied
 */
void yas_stop(void); //Stops the scheduler in a way that no new tasks will be put in ready queue(s), however tasks already in enqueued will be processed

/**
 * Ask the time to the scheduler
 * 
 * @return the time in nanosecond
 */
yas_time_t yas_get_sched_clock(void);


/**
 * Declare a task to the scheduler
 * 
 * Declare a task to the scheduler providing the informations of the task in the
 * `yas_task_data_t' struct, a function to call the actual task, and some fixed
 * arguments that needs to be passed to the task. 
 * 
 * The fixed arguments are arguments that do not change accross calls of the task.
 * If the task requires more than one fixed argument, you should consider passing 
 * a pointer to a structure.
 * 
 * In case of tasks with multi-version, the function pointer and the fixed args 
 * **must** be set to NULL, and version must be added later with `yas_version_decl',
 * even if the task has only one version.
 * 
 * @param t the task information data
 * @param func the function to call, can be NULL if using multiple version
 * @param fixed_args a pointer to the fixed arguments, can be NULL if no fixed args is needed
 * @return a uniq id for the task
 */
yas_task_id_t yas_task_decl(const yas_task_data_t *t, yas_task_func_t func, void *fixed_args);

/**
 * Alter the period of a task
 * 
 * This can be done only if the scheduler is not running, either before a 
 * `yas_start' or after a  `yas_stop'.
 * 
 * This is ignored for a offline scheduler or for a task that hasn't been 
 * previously declared periodic.
 * 
 * @param id the task id
 * @param period 
 * @return true if it has been done
 */
ybool yas_task_alter_period(yas_task_id_t id, yas_time_t period);
/**
 * Alter the deadline of a task
 * 
 * This can be done only if the scheduler is not running, either before a 
 * `yas_start' or after a  `yas_stop'.
 * 
 * This is ignored for a offline scheduler.
 * 
 * @param id the task id
 * @param deadline 
 * @return true if it has been done
 */
ybool yas_task_alter_deadline(yas_task_id_t id, yas_time_t deadline);
/**
 * Alter the priority of a task
 * 
 * This can be done only if the scheduler is not running, either before a 
 * `yas_start' or after a  `yas_stop'.
 * 
 * This is ignored if the configured priority assignment is based on scheduler
 * decision (e.g. EDF, RM)
 * 
 * @param id the task id
 * @param priority 
 * @return true if it has been done
 */
ybool yas_task_alter_priority(yas_task_id_t id, size_t priority);
/**
 * Alter the mapping of a task
 * 
 * This can be done only if the scheduler is not running, either before a 
 * `yas_start' or after a  `yas_stop'.
 * 
 * This is ignored if the mapping strategy is global.
 * 
 * @param id the task id
 * @param mapping 
 * @return true if it has been done
 */
ybool yas_task_alter_mapping(yas_task_id_t id, int mapping);
/**
 * Alter the offset of a task
 * 
 * This can be done only if the scheduler is not running, either before a 
 * `yas_start' or after a  `yas_stop'.
 * 
 * @param id the task id
 * @param offset 
 * @return true if it has been done
 */
ybool yas_task_alter_offset(yas_task_id_t id, yas_time_t offset);

/**
 * Declare a version for a multi-version task
 * 
 * It adds a possible version for the given task. 
 * 
 * The alternative function and its fixed arguments follow the same logic as 
 * `yas_task_decl'. 
 * 
 * The version property is a structure specific to the strategy to apply for the
 * versoin selection at runtime. See above.
 * 
 * @param id the uniq id of the task returned by `yas_task_decl'
 * @param altfunc  the function to call
 * @param static_argument a pointer to the fixed arguments
 * @param variant_properties the version selection information
 * @return an id for the version
 */
yas_version_id_t yas_version_decl(yas_task_id_t id, yas_task_func_t altfunc, void *static_argument, yas_version_select_t variant_properties);

/**
 * Activate a aperiodic task
 * 
 * Task not following a regular pattern such as periodic or aperiodic needs to
 * be activated by the user, or another task at the moment decided by the user.
 * This can be done using this function. The task is then placed in the scheduler
 * ready queue and will be executed following mapping/priority rules a priori 
 * configured.
 * 
 * This has no effect on the scheduler if it is configured to follow an off-line
 * strategy.
 * 
 * @param id of the task to activate
 */
void yas_task_activate(yas_task_id_t);

/**
 * Declare a hardware accelerator
 * 
 * A hardware acceleretator can be any shared resource that follows mutual 
 * exclusion accesses. The scheduler then ensures that only one task sharing
 * the same hwaccel are schedule at a time, thus avoiding to waste time in 
 * waiting and blocking a core.
 * 
 * @param name the name of the hardware accelerator
 * @return a uniq id
 */
yas_hwaccel_id_t yas_hwaccel_decl(char *name);
/**
 * Declare that the given task uses the given resource
 * 
 * If not using multiple version, vid will be ignored and can be set to anything.
 * 
 * @param task the task id
 * @param vid the version of the task id
 * @param ressid the resource id return by `yas_hwaccel_decl'
 */
void yas_hwaccel_use(yas_task_id_t task, yas_version_id_t vid, yas_hwaccel_id_t ressid);

#ifdef __cplusplus
}
#endif
#endif /* YAS_API_H */
