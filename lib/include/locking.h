/*
 * Copyright (C) 2020 Benjamin Rouxel <benjamin.rouxel@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * \file locking.h
 * 
 * \short Contains primitives functions for locking mechanisms
 * 
 * In the real-time domain, locking mechanisms are extremely tedious as the blocking
 * time must be bounded.
 * I tried to implement some lock-free locking protocols but it is extremely buggy.
 * 
 * As a fallback, it is possible to use POSIX available locking primitives that
 * are abstracted here. When the lock-free version will be working properly, then
 * no code change will be possible just a macro definition as a switch.
 * 
 * The available locking mechanism I am interested in are:
 * - semaphores
 * - mutex
 * - barrier
 * - conditional barrier
 * 
 * Semaphores are used:
 * - for the access to a shared hw accelerators (see hw_accelerators.c)
 * - to control the possibility for thread to pick a task from a ready queue. If
 *    there is 3 tasks in the ready queue, then 3 threads are allowed to pass the
 *    semaphore. (see job_pool.c)
 * 
 * Mutexes are used:
 * - for the shared ready queue in case of a global on-line scheduler (see job_pool.c)
 * - for the conditioanl barrier in case of an on-line dispatcher for off-line schedule (see below)
 * 
 * A barrier is only require at the initialisation of the thread, therefore it 
 * has no impact on the real-time behavior of the application as ones all threads
 * are initialised the barrier is destroyed. (see worker_thread.c)
 * 
 * Conditional barriers are only used for the on-line dispatcher of off-line schedules.
 * When a core has executed all the tasks it was assigned, it waits on the cond barrier
 * for the other threads to arrive. When they all have finish executing their set
 * of tasks, they restart the execution of the static schedule. (see scheduler_offline.c)
 * 
 * Further reading:
 * - Mellor et al, 1991, Algorithms for scalable synchronization on shared-memory multiprocessors
 * - Anderson et al, 1997, Real-time computing with lock-free shared objects
 * 
 */
#ifndef LOCKING_H
#define LOCKING_H

#ifdef __cplusplus
extern "C" {
#endif
    
#include <math.h>

#include "yasmin_config.h"
#include "check_config.h"
#include "yasmin_utils.h"
    
#include "synchronisation.h" 
#ifdef YAS_SYNCHRO_FORCE_POSIX
#   include <semaphore.h>
#endif

#   include "threading.h"
    
#define YAS_LOCK_FIFO_SIZE (YAS_THREAD_SIZE+1) /* nb threads + main */
    

#ifdef YAS_SYNCHRO_FORCE_POSIX
typedef sem_t y_sem_t;
typedef pthread_mutex_t y_mutex_t;
typedef pthread_cond_t y_cond_t;
typedef pthread_barrier_t y_barrier_t;
#else
typedef struct qNode_s {
    struct qNode_s  *next;
    ybool locked;
} qNode;
typedef struct {
    qNode *lock;
    qNode pool[YAS_LOCK_FIFO_SIZE];
    pthread_t thd[YAS_LOCK_FIFO_SIZE];
    size_t index;
    volatile int lockvar;
} y_mutex_t;
typedef y_mutex_t y_sem_t;

typedef struct {
    size_t head;
    size_t tail;
    size_t nb_waiters;
    ybool wait[YAS_LOCK_FIFO_SIZE];
} y_cond_t;

typedef struct treeNode_s {
    ybool childnotready[4];
    ybool havechild[4];
    ybool *parentpointer;
    ybool parentsense;
    ybool *childpointers[2];
    ybool dummy;
} treeNode;
typedef struct {
    treeNode nodes[YAS_LOCK_FIFO_SIZE];
    size_t index;
} y_barrier_t;
#endif /* YAS_SYNCHRO_FORCE_POSIX */

/* Initialize semaphore object SEM to VALUE.  If PSHARED then share it
   with other processes.  */
void y_sem_init (y_sem_t *sem, int shared, unsigned int val);

/* Free resources associated with semaphore object SEM.  */
void y_sem_destroy (y_sem_t *sem);

/* Wait for SEM being posted.  */
void y_sem_wait (y_sem_t *sem);
/* Wait for SEM being posted.  */
ybool y_sem_wait_nonblock (y_sem_t *sem);

/* Post SEM.  */
void y_sem_post (y_sem_t *sem);

/* Initialize semaphore object SEM to VALUE.  If PSHARED then share it
   with other processes.  */
void y_mutex_init (y_mutex_t *sem);

/* Free resources associated with semaphore object SEM.  */
void y_mutex_destroy (y_mutex_t *sem);

/* Wait for SEM being posted.  */
void y_mutex_wait (y_mutex_t *sem);
/* Wait for SEM being posted.  */
ybool y_mutex_wait_nonblock (y_mutex_t *sem);

/* Post SEM.  */
void y_mutex_post (y_mutex_t *sem);

void y_cond_init(y_cond_t *c);
void y_cond_destroy(y_cond_t *c);
void y_cond_wait(y_cond_t *c, y_mutex_t *m);
void y_cond_signal(y_cond_t *c);
void y_cond_broadcast(y_cond_t *c);

void y_barrier_init(y_barrier_t *b, unsigned nbwaiters);
void y_barrier_destroy(y_barrier_t *b);
void y_barrier_wait(y_barrier_t *b);

#ifdef __cplusplus
}
#endif

#endif /* LOCKING_H */

