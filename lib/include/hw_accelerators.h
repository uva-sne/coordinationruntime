/*
 * Copyright (C) 2020 Benjamin Rouxel <benjamin.rouxel@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * \file hw_accelerators.h
 * 
 * \short Contains functions to manage hardware accelerator reservation
 * 
 * Hardware accelerators are handled as mutually exclusive access resources. A
 * task requests to acquire a resource, if it is available it gets an exclusive
 * access. If not, the task is placed in a waiting list but the thread is not
 * blocked.
 * Once the current task using a resource is done, it releases the resource. If
 * there are other task waiting for the same resource, then they are released
 * in a FIFO manner.
 * 
 * \warning Tasks released after waiting for the resource are actually executed
 * in the worker thread of the 1st caller of the resource. While this behaviour
 * can be OK for global mapping scheme, it might lead to miss-placement for
 * partitioned mapping scheme.
 * 
 * \warning A task is considered to reserve the access its whole execution. In
 * fact it is the scheduler that calls acquire/release not the task user-code.
 * 
 * \todo would it be better to place the tasks back into the scheduler queue for
 * re-schedule?
 * 
 * \todo to allow the user to manage the acquire/release, we would need to save
 * the context, and preempt the task if the resource is not available.
 */
#ifndef HWACCEL_H
#define HWACCEL_H

#ifdef __cplusplus
extern "C" {
#endif
    
#include "yasmin_config.h"
#include "check_config.h"
#include "yasmin_utils.h"
#include "task.h"
#include "yasmin_api.h"

#ifdef YAS_HWACCEL_SIZE
    
#   if YAS_TASK_MAPPING != YAS_MAPPING_GLOBAL_SCHEME
#       warning "Hardware accelerators can be buggy with other mapping scheme than global" 
#   endif
    
/**
 * Initialise internal structure
 */
void y_hwaccel_init(void);
/**
 * The given task request to acquire a hardware accelerator resource. 
 * 
 * If the hardware accelerator is not available, the task is placed into a 
 * waiting list.
 * 
 * @param id
 * @param requester
 * @return 
 */
ybool y_hwaccel_acquire(yas_hwaccel_id_t id, yas_intern_task_t *requester);

/**
 * Releases a hardware accelerator.
 * 
 * If tasks are waiting for this resource, they are executed in a FIFO order.
 * 
 * \warning Tasks released after waiting for the resource are actually executed
 * in the worker thread of the 1st caller of the resource. While this behaviour
 * can be OK for global mapping scheme, it might lead to miss-placement for
 * partitioned mapping scheme.
 * 
 * @param id
 */
void y_hwaccel_release(yas_hwaccel_id_t id);

/**
 * Check if a resource is available or busy.
 * 
 * @param id
 * @return TRUE if the task is available
 */
ybool y_hwaccel_is_available(yas_hwaccel_id_t id);
    
#endif /*YAS_HWACCEL_SIZE*/

#ifdef __cplusplus
}
#endif

#endif /* HWACCEL_H */

