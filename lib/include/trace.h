/*
 * Copyright (C) 2020 Benjamin Rouxel <benjamin.rouxel@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * \file trace.h
 * 
 * \short Allow tracing of events
 * 
 * Event tracing shouldn't, of course, used for production or WCET analysis. It 
 * is the only module that uses dynamic allocation as it is not possible to know 
 * in advance how long will be the trace.
 */
#ifndef TRACE_H
#define TRACE_H

#include "yasmin_config.h"
#include "yasmin_utils.h"
#include "synchronisation.h"

#ifdef __cplusplus
extern "C" {
#endif
    
typedef enum {
    TRACE_BEGIN_SCHEDULING,
    TRACE_END_SCHEDULING,
    TRACE_BEGIN_PREEMPTION,
    TRACE_END_PREEMPTION,
    TRACE_SWITCH_TO,
    TRACE_BEGIN_SWITCH_AWAY,
    TRACE_END_SWITCH_AWAY,
    TRACE_BEGIN_REQUEST_PREEMPTION,
    TRACE_END_REQUEST_PREEMPTION,
    TRACE_SEND_PREEMPTION_SIGNAL,
    TRACE_BEGIN_GET_TASK,
    TRACE_END_GET_TASK,
    TRACE_BEGIN_ACTIVATE_TASK,
    TRACE_END_ACTIVATE_TASK,
    TRACE_BEGIN_FINALIZE_TASK,
    TRACE_END_FINALIZE_TASK,
    TRACE_EVENT_LATENCY,
    TRACE_ACTIVATED_TASK_VARIANT,
    TRACE_RESOURCE_ACQUIRE,
    TRACE_RESOURCE_RELEASE,
    TRACE_RESOURCE_BUSY
} trace_id;

#ifdef YAS_TRACE

/**
 * Batch allocate step
 * Instead of allocating trace_items one by one, allocate a bunch at once as defined
 * by this constant
 */
#define TRACE_ALLOC_STEP 100000

/**
 * Describe a trace item
 */
typedef struct {
    yas_time_t time; //! time of the trace
    trace_id trid; //! id of the information
    int64_t elid; //! a possible value
} trace_item;
/**
 * Dynamically allocated buffer for the trace
 */
extern trace_item *trace_dyn_buf;
/**
 * Current size of the buffer
 */
extern uint64_t trace_dyn_size;
/**
 * Index of for the next trace item in the buffer
 */
extern uint64_t trace_dyn_index;

/**
 * Initialise the tracer
 */
void y_trace_init(void);

/**
 * Print and free the memory
 */
void y_trace_print_and_clear(void);
/**
 * Print a single trace item element
 * 
 * @param item
 */
void y_trace_print_trace_item(const trace_item *item);

/**
 * Add a trace item that doesn't require a value
 * @param trid
 * @param elid
 */
void __y_trace(const trace_id trid, const int64_t elid);

/**
 * Add a trace item with a value
 * @param trid
 * @param elid
 * @param t
 */
void __y_trace_val(const trace_id trid, const int64_t elid, const yas_time_t t);

/**
 * Alias definition to allow to deactivate trace without changing the code and
 * keeping it clear, and without adding extra useless code when traces are deactivated
 */
#define y_trace(trid, elid) __y_trace(trid, (int64_t)elid)
#define y_trace_val(trid, elid, t) __y_trace_val(trid, (int64_t)elid, t)

#else
#define y_trace(trid, elid) (void)(trid);(void)(elid); /*avoid pclint warning*/
#define y_trace_val(trid, elid, t) y_trace(trid,elid);(void)(t); /*avoid pclint warning*/
#endif

#ifdef __cplusplus
}
#endif

#endif /* TRACE_H */

