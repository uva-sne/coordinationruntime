/*
 * Copyright (C) 2020 Benjamin Rouxel <benjamin.rouxel@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * \file hooks.h
 * 
 * \short Contains some pre-defined hooks, mostly for demo
 * 
 * Hooks can be attached to the scheduler to execute some user code before or
 * after a task executes. The function called can be set in the yasmin_config.h
 * given at compile time with 
 * - `YAS_HOOK_PRE_TASK_CALL' : to execute the function before the task
 * - `YAS_HOOK_POST_TASK_CALL' : to execute the function after the task
 * 
 * The hook function receives a `void*' pointer which can be cast to 
 * `yas_intern_task_t*'.
 * 
 * Example:
 * #define YAS_HOOK_PRE_TASK_CALL yas_task_prehook_WCET
 */
#ifndef HOOKS_H
#define HOOKS_H

#include "yasmin_config.h"
#include "job_pool.h"
#include "yasmin_utils.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * This hooks stores the start time of a task
 * @param ptdata
 */
void yas_task_prehook_exectime(void *ptdata);
/**
 * This hooks stores the stop time of a task and allow to compute its execution time
 * @param ptdata
 */
void yas_task_posthook_exectime(void *ptdata);

/**
 * This hooks allows to check if the deadline is met
 * @param ptdata
 */
void yas_task_posthook_check_D(void *ptdata);

/**
 * This hooks computes the scheduling jitter for strict periodic task
 * 
 * @param ptdata
 */
void yas_task_prehook_jitter(void *ptdata);

#ifdef __cplusplus
}
#endif
#endif /* HOOKS_H */

