/*
 * Copyright (C) 2020 Benjamin Rouxel <benjamin.rouxel@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * \file task_core_mapper.h
 * 
 * \short Describe the function to manage the mapping of taks
 * 
 * Managing the mapping, is here about determining in which ready queue to 
 * place a task, should it be in a global queue, or in a thread assigned one.
 * 
 * As of writing 2 implementations of each function exists, one to allow a
 * global mapping scheme, where task can execute on any thread, and one to allow a
 * partitioned mapping scheme. The decision of compiling one OR the other is done
 * at compile time through the yasmin_config.h provided by the user.
 * 
 * When using a partitioned mapping scheme, the thread on which to execute the task
 * should have been decide beforehand, and information is available through the
 * task descriptor,field virt_core_id.
 * 
 * For a glbal mapping scheme, the ready queue is shared among all threads, thus
 * a locking mechanism is unforced to guarantee the mutual exclusion access of
 * the shared queue. Except when specically stated, each function in this file
 * manages the locking/unlocking in its body.
 */
#ifndef TASK_CORE_MAPPER_H
#define TASK_CORE_MAPPER_H

#ifdef __cplusplus
extern "C" {
#endif

#include "yasmin_config.h"
#include "check_config.h"
#include "worker_thread.h"
    
#if YAS_TASK_MAPPING == YAS_MAPPING_GLOBAL_SCHEME
#   define __INTERNAL__C_MAP_THRD_GLOBAL
#elif YAS_TASK_MAPPING == YAS_MAPPING_PARTITIONED_SCHEME
#   define __INTERNAL__C_MAP_THRD_PARTI
#elif YAS_TASK_MAPPING == YAS_MAPPING_STATIC_SCHEME
#  define __INTERNAL__C_MAP_THRD_PARTI
#endif
    
/**
 * \remark Functions are marked extern to allow inlining as they are quite small
 * and are called from very few places, but many times
 */
    
/**
 * Initialise a pool of threads with default value as a constructor
 * 
 * @param pool
 */
extern void y_mapper_init(volatile y_thread_pool_t *pool);
/**
 * De-initialise a pool of threads as a destructor would do
 * further access to the pool can result in unpredictable behavior
 * 
 * @param pool
 */
extern void y_mapper_cleanup(volatile y_thread_pool_t *pool);

/**
 * Push a task into the correct ready queue. The queue must have been a priori 
 * unlocked (with y_mapper_unlock_queue). This allows the scheduler to push a 
 * bunch of task at once without locking/unlocking for each task.
 * 
 * \warning the queue must be unlocked/locked by the caller
 * 
 * @param pool
 * @param data
 */
extern void y_mapper_push_unlocked(volatile y_thread_pool_t *pool, yas_intern_task_t *data);

/**
 * Return the next task to execute
 * 
 * The next task to execute is the head of the list, it is the highest priority
 * assuming that the ready queue has been reordered by the scheduler after
 * adding all new ready tasks.
 * 
 * @param pool
 * @param thread
 * @param task will contain the task to execute
 */
extern void y_mapper_get_next_task(volatile y_thread_pool_t *pool, volatile y_worker_thread_t *thread, yas_intern_task_t * volatile *task);

/**
 * Return the next task to execute, if it has a higher priority than the given one
 * 
 * The next task to execute is the head of the list, it is the highest priority
 * assuming that the ready queue has been reordered by the scheduler after
 * adding all new ready tasks.
 * 
 * It the head of the ready queue doesn't have a higher priority than the given 
 * one, then output parameter `task' is set to NULL.
 * 
 * @param pool
 * @param thread
 * @param priority the reference priority
 * @param task will contain the task to execute
 */
extern void y_mapper_get_next_task_if_higher_prio(volatile y_thread_pool_t *pool, volatile y_worker_thread_t *thread, yas_priority_t priority, yas_intern_task_t * volatile *task);

/**
 * Notify threads that tasks have been added into the ready queue and that they
 * should check it if they have nothing to do.
 * 
 * @param pool
 */
extern void y_mapper_sched_notify(volatile y_thread_pool_t *pool);

/**
 * End the schedule on each thread
 * 
 * When a thread has nothing to execute it just waits that task are added to its
 * ready queue of interests. When the application wants to stop, a ending marker
 * is inserted for each thread to wake them up. See worker_thread.h
 * 
 * After inserting the ending marker, thread are notified that a new task is 
 * available.
 * 
 * @param pool
 * @param ending_task
 */
extern void y_mapper_wakeup_all(volatile y_thread_pool_t *pool, yas_intern_task_t *ending_task);

/**
 * Lock the queue to ensure a mutually exclusive access
 * 
 * @param pool
 */
extern void y_mapper_lock_queue(volatile y_thread_pool_t *pool);
/**
 * Un-Lock the queue to ensure a mutually exclusive access
 * 
 * @param pool
 */
extern void y_mapper_unlock_queue(volatile y_thread_pool_t *pool);

#ifdef __cplusplus
}
#endif

#endif /* TASK_CORE_MAPPER_H */

