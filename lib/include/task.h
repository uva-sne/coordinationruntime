/*
 * Copyright (C) 2020 Benjamin Rouxel <benjamin.rouxel@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * \file task.h
 * 
 * \short Contains function to manage tasks
 */
#ifndef TASKS_H
#define TASKS_H

#ifdef __cplusplus
#include <string>
extern "C" {
#endif

#include "yasmin_config.h"
#include "check_config.h"
    
#include "yasmin_utils.h"
#include "yasmin_api.h"
#include "yasmin_api_channels.h"

/**
 * Type describing a version
 * This is filled by the scheduler when receiving the declaration of a new version
 */
typedef struct {
    yas_version_select_t select_properties; //! mutable structure used to select it or not depending on the strategy
    void *fix_func_args; //! fixed args passed each call of the function of the version
    yas_task_func_t func; //! function to call when executing the version
    
#ifdef YAS_HWACCEL_SIZE
    yas_hwaccel_id_t resource; //! resource that the version requires
    //@todo make it a table if the version needs more than 1 resource
#endif
    
} yas_intern_version_data_t;

/**
 * Type describing a task
 * This is filled by the scheduler when receiving the declaration of a task
 * 
 * \warning Don't forget to modify c_task_pool_copytask if you modify the struct
 */
typedef struct yas_intern_task_s {
    int _internal_id; //! internal id of the task, corresponds to the index in the global array storing tasks (depends on the scheduler strategy)
    yas_task_func_t _internal_thd_func; //! wrapper function used internally for the task
    
    char name[256]; //! name of the task
    
    yas_priority_t priority; //! task's current priority
    int virt_code_id; //! task's mapping, corresponds to the index of the thread to run into, not the physical core. It's either fixed by the user when using a partitioned mapping scheme, or updated by the scheduler and worker thread for global mapping scheme

    yas_intern_version_data_t versions[__INTERNAL_TASK_VERSION_SIZE]; //! the set of versions, there is at least 1 version for the task to be executed
    uint32_t versions_size; //! how many version have been added

    yas_time_t deadline; //! the deadline of the task
    yas_time_t period; //! the period of the task
    yas_time_t offset; //! the offset of the task, time at which the 1st job is released, then it is offset+period*i
    
    yas_time_t last_activation; //! the scheduler sets the last time it released it, \warning it might differ from the time it starts executing
    yas_time_t last_completion; //! the scheduler sets the last time the task completed
    
#ifdef YAS_CHANNEL_SIZE
    uint32_t output_channels_size; //! how many out channels the task has
    base_channel_t *output_channels[YAS_CHANNEL_SIZE]; //! the list of output channels \remark a task can be triggered only when all its channels have been filled
    size_t input_channels_size; //! how many in channels the task has
    base_channel_t * input_channels[YAS_CHANNEL_SIZE]; //! the list of input chanels
#endif
    
} yas_intern_task_t;

/**
 * Type describing a functioin that will sort tasks
 * return -1 if a < b, 0 if a == b, and +1 otherwise
 */
typedef int (*y_task_sort_func_t) (const yas_intern_task_t* a, const yas_intern_task_t* b);

/**
 * Declaration of the function that will select the version to execute
 * \remark redefinition to check that when using a user-defined function, the definition matches
 */
extern yas_version_id_t YAS_VERSION_SELECT_FUNCTION (const yas_intern_task_t *t); // compile ERROR: your variant selection function doesn't match the required prototype

/**
 * Initialise a task with default value for the fields, as a constructor would do 
 * @param t
 */
void y_task_init(yas_intern_task_t *t);
/**
 * Perform a copy of fields from src to dst
 * @param dst task in which copy the fields
 * @param src task from which information are gathered
 */
void y_task_copy(yas_intern_task_t *dst, const yas_intern_task_t *src);

/**
 * Compare 2 tasks according to their period
 * 
 * Used for the Rate-Monotonic (RM) priority assignment
 * If the 2 periods are equal, a comparison of the uniq id is done. This id is
 * dependent on the order of declaration in the user main function
 * 
 * @param a
 * @param b
 * @return -1 if a < b, 0 if a == b, and +1 otherwise
 */
int y_task_compare_period(const yas_intern_task_t *a, const yas_intern_task_t *b);

/**
 * Compare 2 tasks according to their relative deadline
 * 
 * Used for the Deadline-Monotonic (DM) priority assignment
 * If the 2 deadlines are equal, a comparison of the uniq id is done. This id is
 * dependent on the order of declaration in the user main function
 * 
 * @param a
 * @param b
 * @return -1 if a < b, 0 if a == b, and +1 otherwise
 */
int y_task_compare_deadline(const yas_intern_task_t *a, const yas_intern_task_t *b);

/**
 * Compare 2 tasks according to their user-defined priorities
 * 
 * If the 2 priorities are equal, a comparison of the uniq id is done. This id is
 * dependent on the order of declaration in the user main function
 * 
 * @param a
 * @param b
 * @return -1 if a < b, 0 if a == b, and +1 otherwise
 */
int y_task_compare_userprio(const yas_intern_task_t *ta, const yas_intern_task_t *tb);

/**
 * Compare 2 tasks according to their absolute deadline
 * 
 * Used for the Earliest Deadline First (EDF) priority assignment.
 * The absolute deadline is computed using the last_activation and the current 
 * scheduler clock.
 * 
 * If the 2 absolute deadlines are equal, a comparison of the uniq id is done. 
 * This id is dependent on the order of declaration in the user main function
 * 
 * @param a
 * @param b
 * @return -1 if a < b, 0 if a == b, and +1 otherwise
 */
int y_task_compare_edf(const yas_intern_task_t *ta, const yas_intern_task_t *tb);

/**
 * Sort an array of task following a given function to order 2 tasks.
 * 
 * The implemented algorithm is: quick sort
 * 
 * This is a recursive function that recurses "sort_nbelt / 2" maximum times
 * 
 * @param array_values the tasks to order
 * @param comp a comparison function
 * @param start_index the index at which the sorting needs to start
 * @param sort_nbelt the number of element to start
 */
void y_task_sort_array(yas_intern_task_t *array_values, y_task_sort_func_t comp, size_t start_index, size_t sort_nbelt);

#ifdef __cplusplus
}
#endif

#endif /* TASKS_H */

