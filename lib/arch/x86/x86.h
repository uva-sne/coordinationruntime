/*
 * Copyright (C) 2020 Benjamin Rouxel <benjamin.rouxel@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef X86_H
#define X86_H

#ifdef __cplusplus
extern "C" {
#endif

static inline void y_sync_atomic_fence(void) {
    __asm __volatile("pause\n\tmfence");
}
//
//static inline void atomic_store(char **dst, char **src) {
//    __asm __volatile(
//        "lock xchg %0, %1\n\tmfence" 
//        : "=m" (*dst) 
//        : "r" (*src) 
//        : "memory");
////    __atomic_store(dst, src, __ATOMIC_ACQUIRE);
//}
//
//# define __pointer_type(type) (__builtin_classify_type ((type) 0) == 5)
//
///* intptr_t if P is true, or T if P is false.  */
//# define __integer_if_pointer_type_sub(T, P) \
//  __typeof__ (*(0 ? (__typeof__ (0 ? (T *) 0 : (void *) (P))) 0 \
//		  : (__typeof__ (0 ? (intptr_t *) 0 : (void *) (!(P)))) 0))
//
///* intptr_t if EXPR has a pointer type, or the type of EXPR otherwise.  */
//# define __integer_if_pointer_type(expr) \
//  __integer_if_pointer_type_sub(__typeof__ ((__typeof__ (expr)) 0), \
//				__pointer_type (__typeof__ (expr)))
//
///* Cast an integer or a pointer VAL to integer with proper type.  */
//# define cast_to_integer(val) ((__integer_if_pointer_type (val)) (val))
//
//#ifdef __x86_64__
//# define __arch_c_compare_and_exchange(mem, newval, oldval, storeold)   \
//  ({ unsigned char rzf=0;						\
//     __asm __volatile (                                                 \
//		       "lock cmpxchgq %q3, %1 ; sete %2"		\
//		       : "=a" (storeold), "=m" (*mem), "=qm" (rzf)	\
//		       : "q" ((int64_t) cast_to_integer (newval)),	\
//			 "m" (*mem),					\
//			 "0" ((int64_t) cast_to_integer (oldval))	\
//		       : "memory");	      \
//                    \
//     rzf; })
//
//#else 
//#define __arch_c_compare_and_exchange(mem, newval, oldval, storeold)    \
//  ({ unsigned char rzf=0;						\
//     __asm __volatile (                                                 \
//		       "lock cmpxchgl %3, %1 ; sete %2\n\tmfence"			\
//		       : "=a" (storeold), "=m" (*mem), "=rm" (rzf)	\
//		       : "r" (newval), "m" (*mem), "0" (oldval)         \
//			: "memory");                                    \
//     rzf; })
//#endif
//
//static inline unsigned char atomic_CAS(char **dst, char *expected, char *desired) {
//    char **tmp;
//    return __arch_c_compare_and_exchange(dst, desired, expected, tmp);
//}
//
//static inline unsigned char atomic_fetch_CAS(char **dst, char *expected, char *desired, char **storeold) {
//    return __arch_c_compare_and_exchange(dst, desired, expected, *storeold);
//}
//
//#define atomic_increment(mem, i) \
//    __asm __volatile("lock xadd %1, %0\n\tmfence"					      \
//			: "=m" (*mem), "=r" (i)					      \
//			: "m" (*mem), "r" (i)); 
//
//static inline size_t atomic_incr(size_t *cnt) {
////    register size_t i = 1;
////    atomic_increment(cnt, i);
//    
//    size_t i = __atomic_fetch_add(cnt, 1, __ATOMIC_SEQ_CST);
//    __atomic_thread_fence(__ATOMIC_SEQ_CST);
//    return i;
//}
//
//#define atomic_decrement(mem, i) \
//    __asm __volatile("lock xsub %1, %0\n\tmfence"					      \
//			: "=m" (*mem), "=r" (i)					      \
//			: "m" (*mem), "r" (i)); 
//
//static inline size_t atomic_decr(size_t *cnt) {
////    register size_t i = 1;
////    atomic_increment(cnt, i);
//    
//    size_t i = __atomic_fetch_sub(cnt, 1, __ATOMIC_SEQ_CST);
//    __atomic_thread_fence(__ATOMIC_SEQ_CST);
//    return i;
//}


#ifdef __cplusplus
}
#endif
#endif /* X86_H */

