/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   arm.h
 * Author: brouxel
 *
 * Created on 3 juillet 2020, 16:35
 */

#ifndef ARM_H
#define ARM_H

#ifdef __cplusplus
extern "C" {
#endif

static inline void y_sync_atomic_fence() {
//    __asm __volatile ("dmb sy" : : : "memory");
    __atomic_thread_fence(__ATOMIC_SEQ_CST);
}


#ifdef __cplusplus
}
#endif

#endif /* ARM_H */

