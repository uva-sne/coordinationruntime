.syntax unified
	.text
.globl y_thread_pool_preempt
	.type	y_thread_pool_preempt, %function
y_thread_pool_preempt:
    push {r0-r12,r14}

    blx r1

    pop {r0-r12,r14}
    bx      r14
