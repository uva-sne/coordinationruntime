/*
 * Copyright (C) 2020 Benjamin Rouxel <benjamin.rouxel@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * \file synchronisation.h
 * 
 * \short Contains architecture-dependent primitives API for synchronisation
 * 
 * Mostly used for test with atomic operation, and lock-free algorithms.
 * 
 * https://sourceforge.net/p/predef/wiki/Architectures/
 */

#ifndef SYNCHRONISATION_H
#define SYNCHRONISATION_H

#include "unistd.h"
#include "yasmin_config.h"

#ifdef __cplusplus
extern "C" {
#endif
    
static inline void y_sync_atomic_fence(void) __attribute__((always_inline));

#if (defined(__i386) || defined(__i386__) || defined(_M_IX86) || defined(__x86_64) || defined(__x86_64__) || defined(__amd64) || defined(_M_X64))
#   include "x86/x86.h"
#elif defined(__aarch64__)
#   include "aarch64/aarch64.h"
#elif defined(__arm__) || defined(__ARM ) || defined(__arm)
#   include "arm/arm.h"
#else 
#   include "noarch/noarch.h"
#endif

#define y_sync_atomic_incr(X) __atomic_fetch_add(X, 1, __ATOMIC_SEQ_CST)
#define y_sync_atomic_decr(X) __atomic_fetch_sub(X, 1, __ATOMIC_SEQ_CST)
#define y_sync_atomic_CAS(X, Y, Z) __atomic_compare_exchange(X, Y, Z, 0, __ATOMIC_SEQ_CST, __ATOMIC_SEQ_CST)

#define y_sync_atomic_CAS_fetch(W, X, Y, Z) { \
    typeof(W) tmp = W; \
    if(y_sync_atomic_CAS(W, X, Y)) \
        Z = tmp; \
}

#define y_sync_atomic_store(X, Y) __atomic_store(X, Y, __ATOMIC_SEQ_CST)
#define y_sync_atomic_load(X, Y) __atomic_load(X, Y, __ATOMIC_SEQ_CST)

#ifdef __cplusplus
}
#endif
#endif /* SYNCHRONISATION_H */