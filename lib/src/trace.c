/*
 * Copyright (C) 2020 Benjamin Rouxel <benjamin.rouxel@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "trace.h"

#ifdef YAS_TRACE
#include <stdlib.h>
#include <stdio.h>


trace_item *trace_dyn_buf;
uint64_t trace_dyn_size;
uint64_t trace_dyn_index;

void y_trace_init() {
    trace_dyn_buf = (trace_item *)malloc(TRACE_ALLOC_STEP*sizeof(trace_item));
    trace_dyn_size = TRACE_ALLOC_STEP;
    trace_dyn_index = 0;
}

void __y_trace(trace_id trid, int64_t elid) {
    yas_time_t now= yas_utils_now();
    trace_item *tr;
    uint64_t index = y_sync_atomic_incr(&trace_dyn_index);
#ifndef _lint /*make sure the previous call that updated the size buffer is propagated through the memory hierarchy*/
    while(index >= trace_dyn_size) y_sync_atomic_fence();
#endif
    
    tr = &trace_dyn_buf[index];
    tr->time = now;
    tr->trid = trid;
    tr->elid = elid;
    
    if(trace_dyn_index == trace_dyn_size) {
        trace_dyn_buf = (trace_item *)realloc((void*)trace_dyn_buf, (trace_dyn_size+TRACE_ALLOC_STEP)*sizeof(trace_item));
        trace_dyn_size += TRACE_ALLOC_STEP;
        y_sync_atomic_fence();
    }
}

void __y_trace_val(trace_id trid, int64_t elid, yas_time_t t) {
    uint64_t index = y_sync_atomic_incr(&trace_dyn_index);
#ifndef _lint /*make sure the previous call that updated the size buffer is propagated through the memory hierarchy*/
    while(index >= trace_dyn_size) y_sync_atomic_fence();
#endif
    
    trace_dyn_buf[index].time = t;
    trace_dyn_buf[index].trid = trid;
    trace_dyn_buf[index].elid = elid;
    
    if(trace_dyn_index == trace_dyn_size) {
        trace_dyn_size += TRACE_ALLOC_STEP;
        trace_dyn_buf = (trace_item *)realloc((void*)trace_dyn_buf, trace_dyn_size*sizeof(trace_item));
        y_sync_atomic_fence();
    }
}

void y_trace_print_and_clear() {
    size_t i=0 ;
    for(; i < trace_dyn_index ; ++i) {
        y_trace_print_trace_item(&trace_dyn_buf[i]);
    }
    free(trace_dyn_buf);
    trace_dyn_buf = NULL;
    trace_dyn_index = 0;
    trace_dyn_size = 0;
}
    
void y_trace_print_trace_item(const trace_item *item) {
    switch(item->trid) {
        case TRACE_BEGIN_SCHEDULING:
            (void)printf("TRACE_BEGIN_SCHEDULING");
            break;
        case TRACE_END_SCHEDULING:
            (void)printf("TRACE_END_SCHEDULING");
            break;
        case TRACE_BEGIN_PREEMPTION:
            (void)printf("TRACE_BEGIN_PREEMPTION");
            break;
        case TRACE_END_PREEMPTION:
            (void)printf("TRACE_END_PREEMPTION");
            break;
        case TRACE_SWITCH_TO:
            (void)printf("TRACE_SWITCH_TO");
            break;
        case TRACE_BEGIN_SWITCH_AWAY:
            (void)printf("TRACE_BEGIN_SWITCH_AWAY");
            break;
        case TRACE_END_SWITCH_AWAY:
            (void)printf("TRACE_END_SWITCH_AWAY");
            break;
        case TRACE_BEGIN_REQUEST_PREEMPTION:
            (void)printf("TRACE_BEGIN_REQUEST_PREEMPTION");
            break;
        case TRACE_END_REQUEST_PREEMPTION:
            (void)printf("TRACE_END_REQUEST_PREEMPTION");
            break;
        case TRACE_SEND_PREEMPTION_SIGNAL:
            (void)printf("TRACE_SEND_PREEMPTION_SIGNAL");
            break;
        case TRACE_BEGIN_GET_TASK:
            (void)printf("TRACE_BEGIN_GET_TASK");
            break;
        case TRACE_END_GET_TASK:
            (void)printf("TRACE_END_GET_TASK");
            break;
        case TRACE_BEGIN_ACTIVATE_TASK:
            (void)printf("TRACE_BEGIN_ACTIVATE_TASK");
            break;
        case TRACE_END_ACTIVATE_TASK:
            (void)printf("TRACE_END_ACTIVATE_TASK");
            break;
        case TRACE_BEGIN_FINALIZE_TASK:
            (void)printf("TRACE_BEGIN_FINALIZE_TASK");
            break;
        case TRACE_END_FINALIZE_TASK:
            (void)printf("TRACE_END_FINALIZE_TASK");
            break;
        case TRACE_EVENT_LATENCY:
            (void)printf("TRACE_EVENT_LATENCY");
            break;
        case TRACE_ACTIVATED_TASK_VARIANT:
            (void)printf("TRACE_ACTIVATED_TASK_VARIANT");
            break;
        case TRACE_RESOURCE_ACQUIRE:
            (void)printf("TRACE_RESOURCE_ACQUIRE");
            break;
        case TRACE_RESOURCE_RELEASE:
            (void)printf("TRACE_RESOURCE_RELEASE");
            break;
        case TRACE_RESOURCE_BUSY:
            (void)printf("TRACE_RESOURCE_BUSY");
            break;
        default:
            (void)printf("Forgot to modify c_trace_print after adding a trace id");
            break;
    }
    (void)printf(" %llu %li\n", item->time, item->elid);
}

#endif //YAS_TRACE
