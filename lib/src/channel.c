/*
 * Copyright (C) 2020 Benjamin Rouxel <benjamin.rouxel@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "yasmin_config.h"
#include "check_config.h"
#include "yasmin_utils.h"
#include "yasmin_api.h"
#include "yasmin_api_channels.h"

#ifdef YAS_CHANNEL_SIZE
void y_channel_init(base_channel_t *b, size_t qty) {
    b->head = b->tail = 0;
    b->size = qty;
    b->sink = NULL;
    b->src = NULL;
    /*c_mutex_init(&name.m);*/
}

void y_channel_cleanup(base_channel_t *b) {
    /*c_mutex_destroy(&name.m);*/
}

ybool y_channel_are_tokens_available(base_channel_t *b) {
    /*c_mutex_wait(&name.m);*/
    int lhead = b->head;
    int ltail = b->tail;
    /*c_mutex_post(&name.m);*/
    size_t qty = b->size;
    
    if(qty == 0)
        return TRUE;
    else if(ltail < lhead)
        return ltail+qty <= lhead;
    else if(ltail > lhead)
        return (ltail+qty)%(qty+1) <= lhead;
    else 
        return FALSE;
}
#endif /* YAS_CHANNEL_SIZE*/
