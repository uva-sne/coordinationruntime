/*
 * Copyright (C) 2020 Benjamin Rouxel <benjamin.rouxel@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "task_core_mapper.h"

/**
 * \file mapper_global.c
 * \short Implements a global mapping scheme
 */

#ifdef __INTERNAL__C_MAP_THRD_GLOBAL

extern inline void y_mapper_init(volatile y_thread_pool_t *pool) {
    y_job_pool_init(&(pool->queue));
}

extern inline void y_mapper_cleanup(volatile y_thread_pool_t *pool) {
    y_job_pool_cleanup(&(pool->queue));
}

extern inline void y_mapper_push_unlocked(volatile y_thread_pool_t *pool, yas_intern_task_t *data) {
    y_job_pool_push_unlocked(&(pool->queue), data);
}

extern inline void y_mapper_get_next_task(volatile y_thread_pool_t *pool, volatile y_worker_thread_t *thread, yas_intern_task_t * volatile *task) {
    UNUSED_ARGUMENT(thread);
    y_job_pool_pop(&(pool->queue), task);
}
extern void y_mapper_get_next_task_if_higher_prio(volatile y_thread_pool_t *pool, volatile y_worker_thread_t *thread, yas_priority_t priority, yas_intern_task_t * volatile *task) {
    UNUSED_ARGUMENT(thread);
    return y_job_pool_pop_nonblock_if_higher_priority(&(pool->queue), priority, task);
}

void y_mapper_wakeup_all(volatile y_thread_pool_t *pool, yas_intern_task_t *ending_task) {
    size_t i;
    y_mapper_lock_queue(pool);
    /*
     * So here we're sending bogus data to the pool threads, which
     * should cause them each to wake up, and check the above
     * pool->immediate condition. However we don't want that
     * data to be sorted (since it'll crash the sorter).
     */
    for (i = 0; i < YAS_THREAD_SIZE; i++) {
        y_job_pool_push_unlocked(&(pool->queue), ending_task);
    }
    y_mapper_unlock_queue(pool);
    y_mapper_sched_notify(pool);
}

void y_mapper_sched_notify(volatile y_thread_pool_t *pool) {
    size_t i;
    yas_priority_t hprio;
    volatile y_worker_thread_t *thread;
    
    y_trace(TRACE_BEGIN_REQUEST_PREEMPTION, -1);
    hprio = y_job_pool_head_priority(&pool->queue);
    for (i = 0; i < YAS_THREAD_SIZE; ++i) {
        thread = &(pool->threads[i]);
        yas_intern_task_t *t = thread->current_task;
        if(t != NULL && yas_is_higher_priority(t->priority, hprio)) {
            y_trace(TRACE_SEND_PREEMPTION_SIGNAL, thread->system_thread);
            y_system_thread_kill(thread->system_thread, PREEMPTION_SIGNAL);
        }
    }
    y_trace(TRACE_END_REQUEST_PREEMPTION, -1);
}

extern inline void y_mapper_lock_queue(volatile y_thread_pool_t *pool) {
    y_job_pool_lock_queue(&pool->queue);
}
extern inline void y_mapper_unlock_queue(volatile y_thread_pool_t *pool) {
    y_job_pool_unlock_queue(&pool->queue);
}
#endif //__INTERNAL__C_SCHED_THRD_GLOBAL
