/*
 * Copyright (C) 2020 Benjamin Rouxel <benjamin.rouxel@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "task_core_mapper.h"

/**
 * \file mapper_partitioned.c
 * 
 * \short Implements a partitioned mapping scheme
 */

#ifdef __INTERNAL__C_MAP_THRD_PARTI

void y_mapper_init(volatile y_thread_pool_t *pool) {
    volatile y_worker_thread_t *thread;
    for (size_t i = 0; i < YAS_THREAD_SIZE; ++i) {
        thread = &(pool->threads[i]);
        y_job_pool_init(&(thread->queue));
    }
}

void y_mapper_cleanup(volatile y_thread_pool_t *pool) {
    volatile y_worker_thread_t *thread;
    for (size_t i = 0; i < YAS_THREAD_SIZE; ++i) {
        thread = &(pool->threads[i]);
        y_job_pool_cleanup(&thread->queue);
    }
}

void y_mapper_push_unlocked(volatile y_thread_pool_t *pool, yas_intern_task_t *data) {
    y_assert(data->virt_code_id < 0 || data->virt_code_id >= YAS_THREAD_SIZE);
    volatile y_worker_thread_t *thread;
    thread = &(pool->threads[data->virt_code_id]);
    y_assert_msg(thread == NULL, ("Missing mapping of task"));
    DEBUG_MSG(("Push task %s in queue %d\n", data->name, data->virt_code_id));

    y_job_pool_push_unlocked(&(thread->queue), data);
}

extern inline void y_mapper_get_next_task(volatile y_thread_pool_t *pool, volatile y_worker_thread_t *thread, yas_intern_task_t *volatile *task) {
    UNUSED_ARGUMENT(pool);
    y_job_pool_pop(&(thread->queue), task);
}

extern void y_mapper_get_next_task_if_higher_prio(volatile y_thread_pool_t *pool, volatile y_worker_thread_t *thread, yas_priority_t priority, yas_intern_task_t * volatile *task) {
    UNUSED_ARGUMENT(pool);
    return y_job_pool_pop_nonblock_if_higher_priority(&(thread->queue), priority, task);
}

void y_mapper_wakeup_all(volatile y_thread_pool_t *pool, yas_intern_task_t *ending_task) {
    volatile y_worker_thread_t *thread;
    size_t i;
    /*
     * So here we're sending bogus data to the pool threads, which
     * should cause them each to wake up, and check the above
     * pool->immediate condition. However we don't want that
     * data to be sorted (since it'll crash the sorter).
     */
    for (i = 0; i < YAS_THREAD_SIZE; i++) {
        thread =  &(pool->threads[i]);
        y_job_pool_lock_queue(&thread->queue);
        y_job_pool_push_unlocked(&(thread->queue), ending_task);
        y_job_pool_unlock_queue(&thread->queue);
    }
    y_mapper_sched_notify(pool);
}

void y_mapper_sched_notify(volatile y_thread_pool_t *pool) {
    volatile y_worker_thread_t *thread;
    size_t i;
    yas_priority_t hprio;
    
    y_trace(TRACE_BEGIN_REQUEST_PREEMPTION, -1);
    for (i = 0; i < YAS_THREAD_SIZE; ++i) {
        thread = &(pool->threads[i]);
        hprio = y_job_pool_head_priority(&thread->queue);
        yas_intern_task_t *t = thread->current_task;
        if(t != NULL && yas_is_higher_priority(t->priority, hprio)) {
            y_trace(TRACE_SEND_PREEMPTION_SIGNAL, thread->system_thread);
            y_system_thread_kill(thread->system_thread, PREEMPTION_SIGNAL);
        }
    }
    y_trace(TRACE_END_REQUEST_PREEMPTION, -1);
}

void y_mapper_lock_queue(volatile y_thread_pool_t *pool) {
    volatile y_worker_thread_t *thread;
    for (size_t i = 0; i < YAS_THREAD_SIZE; i++) {
        thread =  &(pool->threads[i]);
        y_job_pool_lock_queue(&thread->queue);
    }
}
void y_mapper_unlock_queue(volatile y_thread_pool_t *pool) {
    volatile y_worker_thread_t *thread;
    for (size_t i = 0; i < YAS_THREAD_SIZE; i++) {
        thread =  &(pool->threads[i]);
        y_job_pool_unlock_queue(&thread->queue);
    }
}
#endif /*__INTERNAL__C_SCHED_THRD_PARTI*/
