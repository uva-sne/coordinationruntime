/*
 * Copyright (C) 2020 Benjamin Rouxel <benjamin.rouxel@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * \file scheduler_online.c
 * 
 * \short Implement the function for the on-line scheduler
 * 
 * Provide the implementation of the `yasmin_api.h' for an on-line scheduler.
 * 
 * The on-line scheduler executes in its own thread (outside of the thread pool).
 * The scheduler will be periodically activated to check if there are tasks that
 * need to be released.
 * The activation period of the scheduler is determined by the periods of the application.
 * It is the Greatest Common Divisor of the period.
 * 
 * Once activated, the scheduler checks if it is time to activate a task, because
 * its period has been reached. Then it adds it into the ready queue to allow
 * worker threads to pick it up.
 * 
 * There can be non-recurring tasks, tasks that don't have a period, they are 
 * activated by the user with `yas_task_activate'. The task to activate is
 * immediately added into the ready queue.
 * 
 * I've tested 2 different implementations of the scheduler:
 * - using an interrupt sub-routine (ISR) that is activated by the system upon 
 *   the firing of an alarm
 * - using a specific thread as described above.
 * The 2 implementations can be switched using the `YAS_SCHEDULER_IMPLEM' config
 * definition in yasmin_config.h.
 * The drawback of the ISR, is that it requires more system calls, more stuffs
 * from  the libc.
 * 
 * \remark All tasks must have been a priori declared before launching the scheduler.
 * Even for non-recurring tasks.
 */
#include "yasmin_api.h"

#if YAS_SCHEDULER_IMPLEM == YAS_SCHED_IMPLEM_ISR
#include <sys/timerfd.h>
#include <unistd.h>
#include <signal.h>
#include <sys/wait.h>
#endif

#include "yasmin_config.h"
#include "yasmin_utils.h"
#include "check_config.h"
#include "task.h"
#include "yasmin_api_channels.h"
#include "hw_accelerators.h"
#include "worker_thread.h"
#include "memorymgnt.h"
#include "trace.h"
#include "job_pool.h"
#include "yasmin_api_channels.h"

#ifdef __INTERNAL_YAS_ONLINE_SCHED

/**
 * Describe the internal information needed by the online scheduler
 */
typedef struct {
    volatile ybool running; //! is the scheduler running
    
    //should it be protected with a mutex?
    ybool in_handler; //! TRUE if the scheduler is executing
    yas_time_t sched_period; //! period of the scheduler
    yas_time_t sched_clock; //! current time of the scheduler
    
#if YAS_SCHEDULER_IMPLEM == YAS_SCHED_IMPLEM_ISR
    timer_t timer; //! the timer for the scheduler alarm
#endif
    
    size_t recurring_task_size; //! quantity of recurring tasks
    size_t nonrecurring_task_size; //! quantity of non-recurring tasks
    
    yas_intern_task_t recurring_tasks[YAS_PERIODIC_TASK_SIZE]; //! stores the recurring tasks
    yas_intern_task_t nonrecurring_tasks[YAS_NONRECURRING_TASK_SIZE]; //! stores the non-recurring tasks
    
    yas_intern_task_t *tmp_task_to_add[YAS_PERIODIC_TASK_SIZE+YAS_NONRECURRING_TASK_SIZE]; //! used to temporarily store the task that needs to be released to minimise the blocking time of the ready queue
    size_t added_size; //! quantity of added tasks
} __attribute__((aligned(4))) y_task_manager_t;
static y_task_manager_t glob_online_mgnt;

#ifdef YAS_CHANNEL_SIZE
/**
 * Check if the sink of the channel can be activated
 */
static void y_channel_check(base_channel_t *chan);
#endif

yas_time_t yas_get_sched_clock() {
    return glob_online_mgnt.sched_clock;
}

/**
 * This task is the wrapper for any user task. 
 * 
 * Before calling the user task it performs some initialisations and checks. 
 * When returning from the user task, it does some cleaning.
 * 
 * @param ptdata
 */
static void thread_task_wrapper(void *ptdata) {
#ifdef YAS_CHANNEL_SIZE
    int i;
#endif
    yas_task_func_t func ;//= dummy_function; //avoid segfault if no variant selected, should I raise something?
    void* user_data;
    yas_intern_task_t *tdata;
    yas_version_id_t vid = 0;
    
    tdata = (yas_intern_task_t*) ptdata;
    y_assert(tdata == NULL);
    
#ifdef YAS_HOOK_PRE_TASK_CALL
    YAS_HOOK_PRE_TASK_CALL(ptdata);
#endif

#ifdef YAS_TASK_VERSION_SIZE //the user wants multiple version
    vid = YAS_VERSION_SELECT_FUNCTION(tdata);
#endif

    func = tdata->versions[vid].func;
    user_data = tdata->versions[vid].fix_func_args;
    
    y_trace(TRACE_ACTIVATED_TASK_VARIANT, (size_t)func);
    
#ifdef YAS_HWACCEL_SIZE
    if(__builtin_expect(tdata->versions[vid].resource != YAS_RESOURCE_NO_RESOURCE_USED, TRUE)) {
        if(y_hwaccel_acquire(tdata->versions[vid].resource, tdata)) {
            y_trace(TRACE_END_ACTIVATE_TASK, tdata->_internal_id);
            y_thread_pool_unblock_signals();

            func(user_data);

            y_thread_pool_block_signals();
            y_trace(TRACE_BEGIN_FINALIZE_TASK, tdata->_internal_id);
            
            y_hwaccel_release(tdata->versions[vid].resource);
        }
        else {
            y_trace(TRACE_END_ACTIVATE_TASK, tdata->_internal_id);
            y_trace(TRACE_RESOURCE_BUSY, tdata->versions[vid].resource);
            y_trace(TRACE_BEGIN_FINALIZE_TASK, tdata->_internal_id);
        }
    }
    else {
#endif
        y_trace(TRACE_END_ACTIVATE_TASK, tdata->_internal_id);
        y_thread_pool_unblock_signals();

        func(user_data);

        y_thread_pool_block_signals();
#if YAS_TASK_RECURRING_MODEL == YAS_TASK_RECMODEL_SPORADIC
        tdata->last_completion = yas_utils_now();
#endif
        
        y_trace(TRACE_BEGIN_FINALIZE_TASK, tdata->_internal_id);
#ifdef YAS_HWACCEL_SIZE
    }
#endif

#ifdef YAS_CHANNEL_SIZE
    for(i = 0 ; i < tdata->output_channels_size ; ++i) {
        y_channel_check(tdata->output_channels[i]);
    }
#endif
    
#ifdef YAS_HOOK_POST_TASK_CALL
            YAS_HOOK_POST_TASK_CALL(ptdata);
#endif
    
}

#ifdef YAS_PERIODIC_TASK_SIZE
/**
 * Scheduler function
 * 
 * This is the body of the scheduler function that is called periodically to
 * check if there are tasks to release.
 * 
 * When entering the scheduler the in_handler field is place to TRUE, to avoid
 * multiple instance of the scheduler running in parallel when using alarms.
 * 
 * It first stores all tasks to release in a temporary array, and then push
 * them into the ready queue. This is done in 2 steps in order to minimise spent
 * in the critical section accessing the ready queue (mutex).
 * 
 * 
 * @param sig
 */
static void y_periodic_task_scheduler(int sig) {
    yas_intern_task_t *task;
    size_t i;
    yas_time_t elapsed;
    yas_time_t stime;
    
    UNUSED_ARGUMENT(sig);
    
    if(!glob_online_mgnt.running) return;
    y_assert(glob_online_mgnt.in_handler);//handler takes too much time compared to its period
    glob_online_mgnt.in_handler = TRUE;

    stime = yas_utils_now();
    
    y_trace(TRACE_BEGIN_SCHEDULING, -1);
    
    glob_online_mgnt.sched_clock = stime - (stime % glob_online_mgnt.sched_period); // stime - (stime % freq)
//    DEBUG_MSG(("periodic start %llu - %llu - %llu", stime, (stime % glob_online_mgnt.sched_period), glob_online_mgnt.sched_clock));
    
    glob_online_mgnt.added_size = 0;
    for(i = 0 ; i < glob_online_mgnt.recurring_task_size ; ++i) {
        task = &(glob_online_mgnt.recurring_tasks[i]);
#if YAS_TASK_RECURRING_MODEL == YAS_TASK_RECMODEL_SPORADIC
        elapsed = stime - task->last_completion;
#else //periodic
        elapsed = stime - task->last_activation;
#endif
        if(yas_utils_cmp_time(task->period, elapsed) <= 0) {
            task->last_activation =  glob_online_mgnt.sched_clock;
            glob_online_mgnt.tmp_task_to_add[glob_online_mgnt.added_size] = task;
            ++glob_online_mgnt.added_size;
        }
    }

    if(__builtin_expect(glob_online_mgnt.added_size, TRUE)) {
        y_thread_pool_lock_queue();
#ifdef __INTERNAL_YAS_PRIO_EDF
        //decrease all key of the heap by sched_period, should not need a rebalancing of the heap
#endif
        for(i = 0 ; i < glob_online_mgnt.added_size ; ++i) {
            y_thread_pool_push(glob_online_mgnt.tmp_task_to_add[i]);
        }
        y_thread_pool_unlock_queue();
#if YAS_PREEMPTION_MODEL == YAS_SCHED_PREEMPTIVE
        y_worker_thread_sched_notify();
#endif
    }
    
    y_trace(TRACE_END_SCHEDULING, -1);

    glob_online_mgnt.in_handler = FALSE;
}
#endif //YAS_PERIODIC_TASK_SIZE


/**
 * Assign static priorities to task
 */
static void y_assign_priority(void) {
    size_t i;
    y_task_sort_func_t sfunc;
    
#   if defined(__INTERNAL_YAS_PRIO_RM)
    sfunc = y_task_compare_period;
#   elif defined(__INTERNAL_YAS_PRIO_DM)
    sfunc = y_task_compare_deadline;
#   else //defined(__INTERNAL_YAS_PRIO_USER)
    sfunc = y_task_compare_userprio;
#   endif
    
    y_task_sort_array(glob_online_mgnt.recurring_tasks, sfunc, 0, glob_online_mgnt.recurring_task_size);

    for(i = YAS_HIGHEST_PRIORITY ; yas_is_higher_priority(i, glob_online_mgnt.recurring_task_size) ; yas_decrease_priority(i)) {
        glob_online_mgnt.recurring_tasks[i].priority = i;
    }
}

void yas_init() {
    
    y_system_lock_memory();
    
#ifdef YAS_MAIN_THREAD_AFFINITY
    y_system_thread_setaffinity(y_system_thread_self(), YAS_MAIN_THREAD_AFFINITY);
#endif
#ifdef YAS_MAIN_THREAD_PRIORITY
    y_system_thread_setpriority(y_system_thread_self(), YAS_MAIN_THREAD_PRIORITY);
#endif

    y_thread_pool_new();

    glob_online_mgnt.running = 0;
    glob_online_mgnt.recurring_task_size = 0;
    glob_online_mgnt.nonrecurring_task_size = 0;
    
    glob_online_mgnt.in_handler = FALSE;
    
#ifdef YAS_HWACCEL_SIZE
    y_hwaccel_init();
#endif
    
#ifdef YAS_TRACE
    y_trace_init();
#endif
}

void yas_cleanup() {
    y_assert(glob_online_mgnt.running);
    y_thread_pool_join_all();
    
    y_thread_pool_free();
#ifdef YAS_TRACE
    y_trace_print_and_clear();
#endif
    
    y_system_unlock_memory();
}

#ifdef YAS_PERIODIC_TASK_SIZE
#   if YAS_SCHEDULER_IMPLEM == YAS_SCHED_IMPLEM_ISR
/**
 * Configure the alarm to have the system fire the schedule
 * @return 
 */
static ybool launch_scheduler_task() {
    int status;
    size_t i;
    
    struct sigaction sa;
    struct sigevent te;
    struct itimerspec its;
    yas_time_t stime;
    
    sa.sa_flags = SA_SIGINFO;
    sa.sa_handler = y_periodic_task_scheduler;
    status = sigemptyset(&sa.sa_mask);
    posix_check_err(status, "sigaction");
    status = sigaction(YAS_ALARM_SIGNAL, &sa, NULL);
    posix_check_err(status, "sigaction");

    //set timer, as now all threads are waiting for their predecessors to complete
    te.sigev_notify = SIGEV_SIGNAL;
    te.sigev_signo = YAS_ALARM_SIGNAL;
    te.sigev_value.sival_ptr = &glob_online_mgnt.timer;
    status = timer_create(YAS_CLOCK, &te, &glob_online_mgnt.timer);
    posix_check_err(status, "timer_create");
    
    its.it_value.tv_sec = 
        its.it_interval.tv_sec = glob_online_mgnt.sched_period / (uint64_t)1000000000l;
    its.it_interval.tv_nsec = 
        its.it_value.tv_nsec = glob_online_mgnt.sched_period % (uint64_t)999999999;
    
    stime = yas_utils_now();
    glob_online_mgnt.sched_clock = stime - (stime % glob_online_mgnt.sched_period); // stime - (stime % freq)
    for(i = 0 ; i < glob_online_mgnt.recurring_task_size ; ++i) {
        glob_online_mgnt.recurring_tasks[i].last_activation = glob_online_mgnt.sched_clock;
    }
    y_periodic_task_scheduler(42);
    
    status = timer_settime(glob_online_mgnt.timer, 0, &its, NULL);
    posix_check_err(status, "timer_settime");
    return status == 0;
}
#   else // YAS_SCHEDULER_IMPLEM == YAS_SCHED_IMPLEM_THR
static y_system_thread thrd_sched;
//extern int pthread_setname_np (c_system_thread __target_thread, const char *__name);

/**
 * Function executed by the scheduler thread.
 * 
 * It calls y_periodic_task_scheduler for the scheduling work. Only the thread
 * management, and the sleeping between 2 instances is done here.
 * 
 * @return 
 */
static void* thread_scheduler(volatile void *d) {
    size_t i;
    yas_time_t stime = yas_utils_now();
    yas_time_t elapsed;
    UNUSED_ARGUMENT(d);
    glob_online_mgnt.sched_clock = stime - (stime % glob_online_mgnt.sched_period); // stime - (stime % freq)
    for(i = 0 ; i < glob_online_mgnt.recurring_task_size ; ++i) {
        glob_online_mgnt.recurring_tasks[i].last_activation = glob_online_mgnt.sched_clock;
    }
    
    while(glob_online_mgnt.running) {
        stime = yas_utils_now();
        y_periodic_task_scheduler(42);
        elapsed = yas_utils_now() - stime;
        if(elapsed < glob_online_mgnt.sched_period) {
            yas_utils_nanosleep(glob_online_mgnt.sched_period - elapsed);
        }
        //else the scheduler was actually too long, should I raise something?
    }
    return NULL;
}

/**
 * Configure the scheduler thread
 * @return 
 */
static ybool launch_scheduler_task(void) {
    int affinity, priority, err;
    
#   ifdef YAS_SCHEDULER_THREAD_AFFINITY
    affinity = YAS_SCHEDULER_THREAD_AFFINITY;
#   else
    affinity = -1;
#   endif
#   ifdef YAS_SCHEDULER_THREAD_PRIORITY
    priority = YAS_SCHEDULER_THREAD_PRIORITY;
#   else
    priority = -1;
#   endif
    if(!y_system_thread_create(&thrd_sched, thread_scheduler, NULL, affinity, priority)) {
        DEBUG_MSG(("Couldn't start scheduler thread -- aborting"));
        return FALSE;
    }
    err = pthread_setname_np(thrd_sched, "sched");
    posix_check_err(err, "pthread_setname_np");
    return TRUE;
}
#   endif //YAS_SCHEDULER_IMPLEM
#endif //YAS_PERIODIC_TASK_SIZE

ybool yas_start() {
    size_t i;
    ybool taskset_is_ok = TRUE;
#ifdef YAS_PERIODIC_TASK_SIZE
    yas_time_t periods[YAS_PERIODIC_TASK_SIZE];
#endif //YAS_PERIODIC_TASK_SIZE
    y_assert_return_val(glob_online_mgnt.running, FALSE);

#ifdef _lint
    y_assert_return_val(glob_online_mgnt.recurring_task_size > YAS_PERIODIC_TASK_SIZE, FALSE);
    y_assert_return_val(glob_online_mgnt.recurring_task_size > YAS_NONRECURRING_TASK_SIZE, FALSE);
#endif
    
#ifdef YAS_PERIODIC_TASK_SIZE
    for(i=0 ; i < glob_online_mgnt.recurring_task_size ; ++i) {
        if(glob_online_mgnt.recurring_tasks[i].versions_size == 0) {
            DEBUG_MSG(("Missing an implementation for task %s\n", glob_online_mgnt.recurring_tasks[i].name));
            taskset_is_ok = FALSE;
        }
    }
    
    // Compute the period at which the scheduler needs to be activated
    for(i=0 ; i < glob_online_mgnt.recurring_task_size ; ++i)
        periods[i] = glob_online_mgnt.recurring_tasks[i].period;
    glob_online_mgnt.sched_period = y_utils_gcd_arr_time(periods, glob_online_mgnt.recurring_task_size);
    DEBUG_MSG(("Scheduler function period: %llu\n", glob_online_mgnt.sched_period));
#endif //YAS_PERIODIC_TASK_SIZE
    
    for(i=0 ; i < glob_online_mgnt.nonrecurring_task_size ; ++i) {
        if(glob_online_mgnt.nonrecurring_tasks[i].versions_size == 0) {
            DEBUG_MSG(("Missing an implementation for task %s\n", glob_online_mgnt.nonrecurring_tasks[i].name));
            taskset_is_ok = FALSE;
        }
    }
    y_assert_return_val(!taskset_is_ok, FALSE);

#ifdef __INTERNAL_YAS_FIX_PRIO
    y_assign_priority();
#endif
    
    glob_online_mgnt.running = TRUE;
    
    if(glob_online_mgnt.recurring_task_size > 0)
        return launch_scheduler_task();
    else
        return TRUE;
}

void yas_stop() {
    int status;
    
    y_assert(!glob_online_mgnt.running);
    
    glob_online_mgnt.running = FALSE;
   
#   if YAS_SCHEDULER_IMPLEM == YAS_SCHED_IMPLEM_ISR
    if(glob_online_mgnt.recurring_task_size > 0) {
        status = timer_delete(glob_online_mgnt.timer);
        posix_check_err(status, "timer_delete");
    }
#   endif
    
    status = 0;
    while(glob_online_mgnt.in_handler && status < 10) { 
        yas_utils_nanosleep(1000); 
        y_sync_atomic_fence();
        ++status;
    }
    y_assert_msg(status == 10, ("Probably an issue with the scheduler thread"));
}

/**
 * Copy the necessary data from the information provided by the user to the 
 * internal structure
 * 
 * @param dst
 * @param src
 */
static void y_task_copy_userdata(yas_intern_task_t *dst, const yas_task_data_t *src) {
    size_t i;
#ifdef __cplusplus
    dst->name = src->name;
#else
    for(i = 0 ; i < 256 && src->name[i] != '\0' ; ++i)
        dst->name[i] = src->name[i];
#endif
    dst->priority = src->priority;
    dst->virt_code_id = src->virt_core_id;
    dst->period = src->period;
    dst->deadline = (src->deadline == 0) ? src->period : src->deadline;
    dst->offset = src->offset;
}

yas_task_id_t yas_task_decl(const yas_task_data_t *t, yas_task_func_t func, void *fixed_args) {
    size_t id;
    yas_intern_task_t *tdata;
    y_assert_return_val(glob_online_mgnt.running, -1);
    if(t->period > 0) {
        y_assert_return_val_msg(glob_online_mgnt.recurring_task_size >= YAS_PERIODIC_TASK_SIZE, -1, ("Can't add further periodic task %s with period %llu", t->name, t->period));
        tdata = &(glob_online_mgnt.recurring_tasks[glob_online_mgnt.recurring_task_size]);
        id = glob_online_mgnt.recurring_task_size++;
    }
    else {
        y_assert_return_val(glob_online_mgnt.nonrecurring_task_size >= YAS_NONRECURRING_TASK_SIZE, -1);
        tdata = &(glob_online_mgnt.nonrecurring_tasks[glob_online_mgnt.nonrecurring_task_size]);
        id = YAS_PERIODIC_TASK_SIZE + (glob_online_mgnt.nonrecurring_task_size++);
    }
    y_task_init(tdata);
    y_task_copy_userdata(tdata, t);
    
    tdata->_internal_id = (signed int)id;
    tdata->_internal_thd_func = thread_task_wrapper;
    
    if(func != NULL) {
        tdata->versions[0].func = func;
        tdata->versions[0].fix_func_args = fixed_args;
        tdata->versions_size = 1;
    }
    
    return tdata->_internal_id;
}

/**
 * Return a task from its ID
 * 
 * @param id
 * @param task
 */
static void y_task_get_task_by_id(yas_task_id_t id, yas_intern_task_t **task) {
    size_t i;
    y_assert(id < 0);
#ifdef YAS_PERIODIC_TASK_SIZE
    if(id < YAS_PERIODIC_TASK_SIZE) {
        y_assert_msg(id <= -1 || (unsigned)id >= glob_online_mgnt.recurring_task_size, ("Recurring task id %d not in range", id) );
        for(i = 0 ; i < glob_online_mgnt.recurring_task_size ; ++i) {
            if(glob_online_mgnt.recurring_tasks[i]._internal_id == id) {
                *task = &(glob_online_mgnt.recurring_tasks[i]);
                break;
            }
        }
        return;
    }
#endif //YAS_PERIODIC_TASK_SIZE
    
    y_assert_msg(id < YAS_PERIODIC_TASK_SIZE || (unsigned)id >= YAS_PERIODIC_TASK_SIZE+glob_online_mgnt.nonrecurring_task_size, ("Non-Recurring task id %d not in range\n", id));
    for(i = 0 ; i < glob_online_mgnt.nonrecurring_task_size ; ++i) {
        if(glob_online_mgnt.nonrecurring_tasks[i]._internal_id == id) {
            *task = &(glob_online_mgnt.nonrecurring_tasks[i]);
            break;
        }
    }
}

/**
 * Activate a task
 * 
 * Lock the ready task queue before pushing the task
 * 
 * @param task
 */
static void y_activate_task(yas_intern_task_t *task) {
    task->last_activation =  yas_utils_now();
    y_thread_pool_lock_queue();
    y_thread_pool_push(task);
    y_thread_pool_unlock_queue();
}

void yas_task_activate(yas_task_id_t id) {
    yas_intern_task_t *task;
    y_assert(!glob_online_mgnt.running);
    y_assert_msg(id < YAS_PERIODIC_TASK_SIZE, ("The given task id doesn't correspond to a non-recurring task"));

    y_task_get_task_by_id(id, &task);
    y_assert(task == NULL);
    task->priority = YAS_LOWEST_PRIORITY;
    y_activate_task(task);
}

ybool yas_task_alter_period(yas_task_id_t id, yas_time_t period) {
#ifdef YAS_PERIODIC_TASK_SIZE
    yas_intern_task_t *task = NULL;
    y_assert_return_val(glob_online_mgnt.running, FALSE);
    
    y_task_get_task_by_id(id, &task);
    y_assert_return_val(task == NULL, FALSE);
    task->period = period;
#endif
    return TRUE;
}

ybool yas_task_alter_deadline(yas_task_id_t id, yas_time_t deadline) {
    yas_intern_task_t *task = NULL;
    y_assert_return_val(glob_online_mgnt.running, FALSE);
    
    y_task_get_task_by_id(id, &task);
    y_assert_return_val(task == NULL, FALSE);
    task->deadline = deadline;
    return TRUE;
}
ybool yas_task_alter_priority(yas_task_id_t id, size_t priority) {
    yas_intern_task_t *task = NULL;
    y_assert_return_val(glob_online_mgnt.running, FALSE);
    
    y_task_get_task_by_id(id, &task);
    y_assert_return_val(task == NULL, FALSE);
    task->priority = priority;
    return TRUE;
}
ybool yas_task_alter_mapping(yas_task_id_t id, int mapping) {
    yas_intern_task_t *task = NULL;
    y_assert_return_val(glob_online_mgnt.running, FALSE);
    
    y_task_get_task_by_id(id, &task);
    y_assert_return_val(task == NULL, FALSE);
    task->virt_code_id = mapping;
    return TRUE;
}
ybool yas_task_alter_offset(yas_task_id_t id, yas_time_t offset) {
    yas_intern_task_t *task = NULL;
    y_assert_return_val(glob_online_mgnt.running, FALSE);
    
    y_task_get_task_by_id(id, &task);
    y_assert_return_val(task == NULL, FALSE);
    task->offset = offset;
    return TRUE;
}

yas_version_id_t yas_version_decl(yas_task_id_t id, yas_task_func_t altfunc, void *static_argument, yas_version_select_t user_properties) {
#ifdef YAS_TASK_VERSION_SIZE //the user wants multiple version
    yas_intern_task_t *task = NULL;
    y_assert_return_val(glob_online_mgnt.running, -1);
    
    y_task_get_task_by_id(id, &task);
    y_assert_return_val(task == NULL, -1);
    y_assert_return_val(task->versions_size >= __INTERNAL_TASK_VERSION_SIZE, -1);
    
    task->versions[task->versions_size].select_properties = user_properties;
    task->versions[task->versions_size].func = altfunc;
    task->versions[task->versions_size].fix_func_args = static_argument;
    return task->versions_size++;
#else
    return -1;
#endif
}

#ifdef YAS_CHANNEL_SIZE
void y_channel_connect(yas_task_id_t srcid, yas_task_id_t sinkid, base_channel_t *channel) {
    yas_intern_task_t *src=NULL, *sink=NULL;
    
    y_task_get_task_by_id(srcid, &src);
    y_assert_msg(src == NULL, ("Can't find src with id %d\n", srcid));
    y_task_get_task_by_id(sinkid, &sink);
    y_assert_msg(sink == NULL, ("Can't find sink with id %d\n", sinkid));
    
    channel->src = src;
    channel->sink = sink;
    src->output_channels[src->output_channels_size++] = channel;
    sink->input_channels[sink->input_channels_size++] = channel;
}

static void y_channel_check(base_channel_t *chan) {
    size_t i;
    ybool fireable = TRUE;
    
    y_assert(chan == NULL);
    y_assert(chan->sink == NULL);
    
    for(i = 0 ; i < chan->sink->input_channels_size ; ++i) {
        if(!y_channel_are_tokens_available(chan->sink->input_channels[i])) {
            fireable = FALSE;
            break;
        }
    }
    if(fireable) {
        y_activate_task(chan->sink);
    }
}
#endif /* YAS_CHANNEL_SIZE */

void yas_hwaccel_use(yas_task_id_t tid, yas_version_id_t vid, yas_hwaccel_id_t ressid) {
#ifdef YAS_HWACCEL_SIZE
    yas_intern_task_t *task = NULL;
    y_assert(glob_online_mgnt.running);
    
    y_task_get_task_by_id(tid, &task);
    y_assert(task == NULL);
    
#   ifdef YAS_TASK_VERSION_SIZE //the user wants multiple version
    y_assert(vid < 0 || vid >= task->versions_size);
#   else
    vid = 0;
#   endif /*YAS_TASK_VERSION_SIZE*/
    
    task->versions[vid].resource = ressid;
#endif /*YAS_HWACCEL_SIZE*/
}

#ifdef MODE_TEST
yas_time_t mt_online_get_sched_period() {
    return glob_online_mgnt.sched_period;
}
ybool mt_online_is_running() {
    return glob_online_mgnt.running;
}
yas_intern_task_t* mt_get_recurring_tasks() {
    return glob_online_mgnt.recurring_tasks;
}
size_t mt_get_recurring_task_size() {
    return glob_online_mgnt.recurring_task_size;
}
yas_intern_task_t* mt_get_nonrecurring_tasks() {
    return glob_online_mgnt.nonrecurring_tasks;
}
size_t mt_get_nonrecurring_task_size() {
    return glob_online_mgnt.nonrecurring_task_size;
}
#endif /*MODE_TEST*/

#endif /* __INTERNAL_YAS_ONLINE_SCHED */
