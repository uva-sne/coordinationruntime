/*
 * Copyright (C) 2020 Benjamin Rouxel <benjamin.rouxel@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "job_pool.h"

void y_job_pool_init(volatile y_task_pool_t *pool) {
    pool->q_elt_pool_cnt = 0;
    for(uint32_t i = 0 ; i < Y_TASK_POOL_QUEUE_SIZE ; ++i) {
        pool->q_elt_pool[i].data = NULL;
        pool->q_elt_pool[i].key = YAS_LOWEST_PRIORITY;
    }
    
    y_sem_init((y_sem_t *)&pool->cancons, 0, 0);
    y_mutex_init((y_mutex_t *)&pool->mutex);
}

void y_job_pool_cleanup(volatile y_task_pool_t *pool) {
    y_sem_destroy((y_sem_t *)&pool->cancons);
    y_mutex_destroy((y_mutex_t *)&pool->mutex);
    pool->q_elt_pool_cnt = 0;
}

size_t y_job_pool_head_priority(const volatile y_task_pool_t *pool) {
    return pool->q_elt_pool[1].key;
}

/**
 * Re-balanced the tree after the addition of a task
 * 
 * @param pool
 * @param cur
 */
static void y_job_pool_percolate_up(volatile y_task_pool_t *pool, uint32_t cur) {
    volatile y_tpool_elt_t tmp;
    uint32_t parent;
    parent = cur >> 1; // cur/2
    while(parent > 0 && pool->q_elt_pool[parent].key > pool->q_elt_pool[cur].key) {
        tmp = pool->q_elt_pool[parent];
        pool->q_elt_pool[parent] = pool->q_elt_pool[cur];
        pool->q_elt_pool[cur] = tmp;
        cur = parent;
        parent = cur >> 1; // cur/2
    }
}

/**
 * Re-balanced the tree after the removal of the root
 * @param pool
 * @param cur
 */
static void y_job_pool_percolate_down(volatile y_task_pool_t *pool, uint32_t cur) {
    volatile y_tpool_elt_t tmp;
    uint32_t child;
    
    for(child = cur << 1 ; child <= pool->q_elt_pool_cnt ; child = cur << 1) {
        if(pool->q_elt_pool[child].key > pool->q_elt_pool[child+1].key)
            ++child;
        if(pool->q_elt_pool[cur].key <= pool->q_elt_pool[child].key)
            break;
        tmp = pool->q_elt_pool[cur];
        pool->q_elt_pool[cur] = pool->q_elt_pool[child];
        pool->q_elt_pool[child] = tmp;
        cur = child;
    }
}

void y_job_pool_push_unlocked(volatile y_task_pool_t *pool, yas_intern_task_t *job) {
    uint32_t cur;
    ybool found = FALSE;
    yas_priority_t oldprio;
    if(__builtin_expect(pool->q_elt_pool_cnt == Y_TASK_POOL_QUEUE_SIZE-1, FALSE)) {
        // heap is full, there is probably some deadline overrun going on
        // let's see if we have a job multiple times and only keep the last one
        for(cur = 1 ; cur <= pool->q_elt_pool_cnt ; ++cur) {
            if(job == pool->q_elt_pool[cur].data) {
                found = TRUE;
                break;
            }
        }
        
        if(!found)
            y_assert_msg(cur = cur / found, ("This segfault occurs because the YAS_TASK_POOL_JOBS_SIZE is too small, check your yasmin_config.h"));
        
        oldprio = pool->q_elt_pool[cur].key;
        pool->q_elt_pool[cur].key = job->priority;
        if(__builtin_expect(yas_is_higher_priority(job->priority, oldprio), FALSE)) {
            y_job_pool_percolate_up(pool, cur);
        }
        else if(__builtin_expect(yas_is_higher_priority(oldprio, job->priority), FALSE)) {
            y_job_pool_percolate_down(pool, cur);
        }
    }
    else {
        cur = ++pool->q_elt_pool_cnt; //__atomic_add_fetch(pool->q_elt_pool_cnt, __ATOMIC_SEQ_CST); //first access is cur = 1, not cur = 0
        pool->q_elt_pool[cur].key = job->priority;
        pool->q_elt_pool[cur].data = job;
        y_job_pool_percolate_up(pool, cur);
        y_sem_post((y_sem_t *)&pool->cancons);
    }
    
    y_sync_atomic_fence();
}

/**
 * Pop the root of the binary heap after it has been locked by a thread
 * 
 * @param pool
 * @param job
 */
static void _y_job_pool_pop_unblocked(volatile y_task_pool_t *pool, yas_intern_task_t * volatile *job) {
    uint32_t last;
    y_trace(TRACE_BEGIN_GET_TASK, y_system_thread_self());
    
    *job = pool->q_elt_pool[1].data;
    
    last = pool->q_elt_pool_cnt--;//__atomic_fetch_sub(pool->q_elt_pool_cnt, __ATOMIC_SEQ_CST);
    pool->q_elt_pool[1] = pool->q_elt_pool[last];
    pool->q_elt_pool[last].data = NULL;
    pool->q_elt_pool[last].key = YAS_LOWEST_PRIORITY;
    y_job_pool_percolate_down(pool, 1);
    y_sync_atomic_fence();
}

void y_job_pool_pop(volatile y_task_pool_t *pool, yas_intern_task_t * volatile *job) {
    y_sem_wait((y_sem_t *)&pool->cancons);
    y_job_pool_lock_queue(pool);
    _y_job_pool_pop_unblocked(pool, job);
    y_job_pool_unlock_queue(pool);
}

void y_job_pool_pop_nonblock(volatile y_task_pool_t *pool, yas_intern_task_t * volatile *job) {
    if(!y_sem_wait_nonblock((y_sem_t *)&pool->cancons)) {
        *job = NULL;
        return;
    }
    if(!y_mutex_wait_nonblock((y_mutex_t*)&pool->mutex)) {
        *job = NULL;
        y_sem_post((y_sem_t *)&pool->cancons);
        return;
    }
    _y_job_pool_pop_unblocked(pool, job);
    y_job_pool_unlock_queue(pool);
}

void y_job_pool_pop_nonblock_if_higher_priority(volatile y_task_pool_t *pool, yas_priority_t priority, yas_intern_task_t * volatile *job) {
    *job = NULL;
    if(__builtin_expect(!y_sem_wait_nonblock((y_sem_t *)&pool->cancons), FALSE)) {
        return;
    }
    if(__builtin_expect(!y_mutex_wait_nonblock((y_mutex_t*)&pool->mutex), FALSE)) {
        y_sem_post((y_sem_t *)&pool->cancons);
        return;
    }
    if(__builtin_expect(yas_is_higher_priority(pool->q_elt_pool[1].key, priority), TRUE))
        _y_job_pool_pop_unblocked(pool, job);
    y_job_pool_unlock_queue(pool);
}

void y_job_pool_lock_queue(volatile y_task_pool_t *pool) {
    y_mutex_wait((y_mutex_t*)&pool->mutex);
}
void y_job_pool_unlock_queue(volatile y_task_pool_t *pool) {
    y_mutex_post((y_mutex_t*)&pool->mutex);
}

uint32_t y_job_pool_length(const volatile y_task_pool_t *pool) {
    return pool->q_elt_pool_cnt;
}
