/*
 * Copyright (C) 2020 Benjamin Rouxel <benjamin.rouxel@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "task.h"
#include "yasmin_api.h" /* extern time_prop_t coord_get_sched_clock(); */

void y_task_init(yas_intern_task_t *t) {
    uint32_t i;
    t->_internal_id = -1;
#ifdef __cplusplus
    t->name = "";
#else
    for(i = 0 ; i < 256 ; ++i)
        t->name[i] = '\0';
#endif
    t->_internal_thd_func = NULL;
    t->versions_size = 0;
    t->priority = YAS_LOWEST_PRIORITY;
    t->virt_code_id = -1;
    t->deadline = 0;
    t->period = 0;
    t->last_activation = 0;
    t->last_completion = (yas_time_t)-1;
    
    for(i = 0 ; i < __INTERNAL_TASK_VERSION_SIZE ; ++i) {
#ifdef YAS_HWACCEL_SIZE
        t->versions[i].resource = YAS_RESOURCE_NO_RESOURCE_USED;
#endif
        t->versions[i].fix_func_args = NULL;
        t->versions[i].func = NULL;
    }
    
#ifdef YAS_CHANNEL_SIZE
    t->input_channels_size = 0;
    t->output_channels_size = 0;
    for(i = 0 ; i < YAS_CHANNEL_SIZE ; ++i)
        t->output_channels[i] = NULL;
    for(i = 0 ; i < YAS_CHANNEL_SIZE ; ++i)
        t->input_channels[i] = NULL;
#endif
}

void y_task_copy(yas_intern_task_t *dst, const yas_intern_task_t *src) {
    uint32_t i;
    dst->_internal_id = src->_internal_id;
    for(i = 0 ; i < 256 ; ++i)
        dst->name[i] = src->name[i];
    dst->_internal_thd_func = src->_internal_thd_func;
    dst->priority = src->priority;
    dst->virt_code_id = src->virt_code_id;
    dst->period = src->period;
    dst->deadline = (src->deadline == 0) ? src->period : src->deadline;
    dst->last_activation = src->last_activation;
    dst->offset = src->offset;
    
    dst->versions_size = src->versions_size;
    for(i = 0 ; i < dst->versions_size ; ++i) {
#ifdef YAS_HWACCEL_SIZE
        dst->versions[i].resource = src->versions[i].resource;
#endif
        dst->versions[i].fix_func_args = src->versions[i].fix_func_args;
        dst->versions[i].select_properties = src->versions[i].select_properties;
        dst->versions[i].func = src->versions[i].func;
    }

#ifdef YAS_CHANNEL_SIZE    
    dst->output_channels_size = src->output_channels_size;
    for(i = 0 ; i < dst->output_channels_size ; ++i)
        dst->output_channels[i] = src->output_channels[i];
    dst->input_channels_size = src->input_channels_size;
    for(i = 0 ; i < dst->input_channels_size ; ++i)
        dst->input_channels[i] = src->input_channels[i];
#endif
}

//rate monotonic
int y_task_compare_period(const yas_intern_task_t *ta, const yas_intern_task_t *tb) {
    short cmp = yas_utils_cmp_time(ta->period, tb->period);
    if(cmp == 0) 
        return (ta->_internal_id < tb->_internal_id) ? -1 : 1;
    return cmp;
}

//deadline monotonic
int y_task_compare_deadline(const yas_intern_task_t *ta, const yas_intern_task_t *tb) {
    short cmp = yas_utils_cmp_time(ta->deadline, tb->deadline);
    if(cmp == 0) 
        return (ta->_internal_id < tb->_internal_id) ? -1 : 1;
    return cmp;
}

//smallest priority first
int y_task_compare_userprio(const yas_intern_task_t *ta, const yas_intern_task_t *tb) {
    if ( ta->priority == tb->priority ) 
        return (ta->_internal_id < tb->_internal_id) ? -1 : 1;
    else if ( yas_is_higher_priority(ta->priority, tb->priority) ) return -1;
    else return 1;
}

//earlist deadline first
int y_task_compare_edf(const yas_intern_task_t *ta, const yas_intern_task_t *tb) {
    yas_time_t slacka = (ta->last_activation + ta->deadline) - (ta->last_activation + yas_get_sched_clock());
    yas_time_t slackb = (tb->last_activation + tb->deadline) - (tb->last_activation + yas_get_sched_clock());
    
    short cmp = yas_utils_cmp_time(slacka, slackb);
    if(cmp == 0) 
        return (ta->_internal_id < tb->_internal_id) ? -1 : 1;
    return cmp;
}

static void swap_periodic_task(yas_intern_task_t *cpx, yas_intern_task_t *cpy) {
    yas_intern_task_t swap_buf = *cpx;
    
    y_task_copy(&swap_buf, cpx);
    y_task_copy(cpx, cpy);
    y_task_copy(cpy, &swap_buf);
}

/* This function takes last element as pivot, places 
   the pivot element at its correct position in sorted 
    array, and places all smaller (smaller than pivot) 
   to left of pivot and all greater elements to right 
   of pivot */
static size_t quick_sort_partition (yas_intern_task_t *array_values, y_task_sort_func_t comp, size_t start_index, size_t sort_nbelt) { 
    size_t pivot = sort_nbelt-1;    // pivot 
    size_t i = (start_index - 1);  // Index of smaller element 
    size_t j = start_index;
    
    for (; j <= sort_nbelt- 1; j++) 
    { 
        // If current element is smaller than the pivot 
        if (comp(&array_values[j], &array_values[pivot]) < 0) { 
            i++;    // increment index of smaller element 
            if(i != j ) {
                swap_periodic_task(&array_values[i], &array_values[j]); 
            }
        } 
    } 
    if(i+1 != sort_nbelt-1) {
        swap_periodic_task(&array_values[i+1], &array_values[sort_nbelt-1]); 
    }
    return (i + 1); 
} 
  
/* The main function that implements QuickSort 
 arr[] --> Array to be sorted, 
  low  --> Starting index, 
  high  --> Ending index */
void y_task_sort_array(yas_intern_task_t *array_values, y_task_sort_func_t comp, size_t start_index, size_t sort_nbelt) { 
    size_t pi;
    if(sort_nbelt < 2) return;
    if(start_index >= sort_nbelt) return;

    /* pi is partitioning index, arr[p] is now 
       at right place */
    pi = quick_sort_partition(array_values, comp, start_index, sort_nbelt); 
    // Separately sort elements before 
    // partition and after partition 
    if(pi > 0)
        y_task_sort_array(array_values, comp, start_index, pi - 1); 
    y_task_sort_array(array_values, comp, pi + 1, sort_nbelt); 
}

