/*
 * Copyright (C) 2020 Benjamin Rouxel <benjamin.rouxel@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "locking.h"

#ifdef YAS_SYNCHRO_FORCE_POSIX

void y_sem_init(y_sem_t *m, int shared, unsigned int val) {
    int err = sem_init(m,shared, val);
    posix_check_err(err, "sem_init");
}

void y_sem_destroy(y_sem_t *m) {
    int err = sem_destroy(m);
    posix_check_err(err, "sem_destroy");
}

void y_sem_wait(y_sem_t *m) {
    int res;
    do {
        res = sem_wait(m);
    }
    while(res == -1);
}
ybool y_sem_wait_nonblock(y_sem_t *m) {
    int res = sem_trywait(m);
    if(res == 0)
        return TRUE;
    if(errno == EAGAIN)
        return FALSE;
    posix_check_err(errno, "sem_trywait");
    return FALSE;
}

void y_sem_post (y_sem_t *m) {
    int err = sem_post(m);
    posix_check_err(err, "sem_post");
}

void y_mutex_init(y_mutex_t *m) {
    pthread_mutexattr_t attr;
    int err = pthread_mutexattr_init(&attr);
    posix_check_err(err, "pthread_mutexattr_init");
    err = pthread_mutex_init(m, &attr);
    posix_check_err(err, "pthread_mutex_init");
    err = pthread_mutexattr_destroy(&attr);
    posix_check_err(err, "pthread_mutexattr_destroy");
}

void y_mutex_destroy(y_mutex_t *m) {
    int err = pthread_mutex_destroy(m);
    posix_check_err(err, "pthread_mutex_destroy");
}

void y_mutex_wait(y_mutex_t *m) {
    int err = pthread_mutex_lock(m);
    posix_check_err(err, "pthread_mutex_lock");
}
ybool y_mutex_wait_nonblock(y_mutex_t *m) {
    int err = pthread_mutex_trylock(m);
    if(err == 0)
        return TRUE;
    if(err == EBUSY)
        return FALSE;
    posix_check_err(err, "pthread_mutex_lock");
    return FALSE;
}

void y_mutex_post (y_mutex_t *m) {
    int err = pthread_mutex_unlock(m);
    posix_check_err(err, "pthread_mutex_unlock");
}

void y_cond_init(y_cond_t *c) {
    pthread_condattr_t cattr;
    int err = pthread_condattr_init(&cattr);
    posix_check_err(err, "pthread_condattr_init");
    err = pthread_cond_init(c, &cattr);
    posix_check_err(err, "pthread_cond_init");
    err = pthread_condattr_destroy(&cattr);
    posix_check_err(err, "pthread_condattr_destroy");
}
void y_cond_destroy(y_cond_t *c) {
    int err = pthread_cond_destroy(c);
    posix_check_err(err, "pthread_cond_destroy");
}
void y_cond_wait(y_cond_t *c, y_mutex_t *m) {
    int err = pthread_cond_wait(c, m);
    posix_check_err(err, "pthread_cond_wait");
}
void y_cond_signal(y_cond_t *c) {
    int err = pthread_cond_signal(c);
    posix_check_err(err, "pthread_cond_signal");
}
void y_cond_broadcast(y_cond_t *c) {
    int err = pthread_cond_broadcast(c);
    posix_check_err(err, "pthread_cond_broadcast");
}

void y_barrier_init(y_barrier_t *b, unsigned nbwaiters) {
    pthread_barrierattr_t barattr;
    int err = pthread_barrierattr_init(&barattr);
    posix_check_err(err, "pthread_barrierattr_init");
    err = pthread_barrier_init(b, &barattr, nbwaiters);
    posix_check_err(err, "pthread_barrier_init");
    err = pthread_barrierattr_destroy(&barattr);
    posix_check_err(err, "pthread_barrierattr_destroy");
}
void y_barrier_destroy(y_barrier_t *b) {
    int err = pthread_barrier_destroy(b);
    posix_check_err(err, "pthread_barrier_destroy");
}
void y_barrier_wait(y_barrier_t *b) {
    int err = pthread_barrier_wait(b);
    if(err != PTHREAD_BARRIER_SERIAL_THREAD)
        posix_check_err(err, "pthread_barrier_wait");
}

#else 

void y_sem_init(y_sem_t *m, int shared, unsigned int val) {
}

void y_sem_destroy(y_sem_t *m) {
}

void y_sem_wait(y_sem_t *m) {
}
ybool y_sem_wait_nonblock(y_sem_t *m) {
    return FALSE;
}

void y_sem_post (y_sem_t *m) {
}

void y_mutex_init(y_mutex_t *m) {
    m->index = 0;
    m->lock = NULL;
    m->lockvar = 0;
}

void y_mutex_destroy(y_mutex_t *m) {
    m->index = -1;
}

static size_t find_index(y_mutex_t *m) {
    size_t i = 0;
    for( ; i < YAS_LOCK_FIFO_SIZE ; ++i) {
        if(m->thd[i] == y_system_thread_self())
            return i;
    }
    return -1; // should never go over YAS_LOCK_FIFO_SIZE
}

void y_mutex_wait(y_mutex_t *m) {
    while (__sync_lock_test_and_set(&m->lockvar, 1)) {
        __sync_synchronize();
    }
//    size_t i = find_index(m);
//    if(i == -1)
//        i = c_sync_atomic_incr(&m->index);
//    m->thd[i] = c_system_thread_self();
//    
//    qNode *I = &(m->pool[i]);
//    I->next = NULL;
//    qNode * predecessor = NULL;
//    c_sync_atomic_CAS_fetch(m->lock, m->lock, I, predecessor);
//    if(predecessor != NULL) {
//        I->locked = TRUE;
//        predecessor->next = I;
//        while(I->locked) c_sync_atomic_fence();
//    }
}
ybool y_mutex_wait_nonblock(y_mutex_t *m) {
    return !__sync_lock_test_and_set(&m->lockvar, 1);
}

void y_mutex_post (y_mutex_t *m) {
    m->lockvar = 0;
    __sync_synchronize();
//    size_t i = find_index(m);
//    assert_return(i == -1);
//    assert_return(m->thd[i] != pthread_self());
//    qNode *I = &(m->pool[i]);
//    if(I->next == NULL) {
//        qNode *null = NULL;
//        if(c_sync_atomic_CAS(m->lock, I, null))
//            return;
//        while(I->next == NULL) c_sync_atomic_fence();
//    }
//    I->next->locked = FALSE;
}

void y_cond_init(y_cond_t *c) {
    c->head = 0;
    c->tail = 0;
    c->nb_waiters = 0;
    for(size_t i = 0 ; i < YAS_LOCK_FIFO_SIZE ; ++i)
        c->wait[i] = FALSE;
}

void y_cond_destroy(y_cond_t *c) {
    c->head = -1;
    c->tail = -1;
    c->nb_waiters = -1;
}

void y_cond_wait(y_cond_t *c, y_mutex_t *m) {
    size_t i = c->head;
    c->head = (c->head + 1) % YAS_LOCK_FIFO_SIZE;
    y_sync_atomic_incr(&c->nb_waiters);
    
    c->wait[i] = TRUE;
    while(c->wait[i]) {
        y_mutex_post(m);
        yas_utils_nanosleep(500000);
        y_mutex_wait(m);
    }
    y_sync_atomic_decr(&c->nb_waiters);
}

void y_cond_signal(y_cond_t *c) {
    if(c->nb_waiters) {
        size_t i = c->tail;
        c->tail = (c->tail + 1) % YAS_LOCK_FIFO_SIZE;
        c->wait[i] = FALSE;
    }
}

void y_cond_broadcast(y_cond_t *c) {
    if(c->nb_waiters) {
        while(c->tail != c->head) {
            size_t i = c->tail;
            c->tail = (c->tail + 1) % YAS_LOCK_FIFO_SIZE;
            c->wait[i] = FALSE;
        }
    }
}
void y_barrier_init(y_barrier_t *b, unsigned nbwaiters) {
    for(size_t i = 0 ; i < nbwaiters ; i++) {
        treeNode *n = &(b->nodes[i]);
        for(size_t j = 0 ; j < 4 ; ++j)
            n->childnotready[j] = n->havechild[j] = (4*i+j < nbwaiters-1) ? TRUE : FALSE;
        n->parentpointer = (i == 0) ? &n->dummy : &(b->nodes[(int)floor((i-1)/4)].childnotready[(i-1)%4]);
        n->childpointers[0] = (2*i+1 >= nbwaiters) ? &(b->nodes[2*i+1].dummy) : &(b->nodes[2*i+1].parentsense);
        n->childpointers[1] = (2*i+2 >= nbwaiters) ? &(b->nodes[2*i+2].dummy) : &(b->nodes[2*i+2].parentsense);
        n->parentsense = FALSE;
    }
}

void y_barrier_destroy(y_barrier_t *b) {
}


void y_barrier_wait(y_barrier_t *b) {
    ybool sense = TRUE;
    size_t i = y_sync_atomic_incr(&b->index);
    treeNode *n = &(b->nodes[i]);
    for(size_t j = 0 ; j < 4 ; ++j)
        while(n->childnotready[j] != FALSE) {y_sync_atomic_fence();}
    for(size_t j = 0 ; j < 4 ; ++j)
            n->childnotready[j] = n->havechild[j];
    *n->parentpointer = FALSE;
    if(i != 0)
        while(n->parentsense != sense) {y_sync_atomic_fence();}
    *n->childpointers[0] = sense;
    *n->childpointers[1] = sense;
    if(i == 0)
        b->index = 0;
}

#endif //YAS_SYNCHRO_FORCE_POSIX
