/*
 * Copyright (C) 2020 Benjamin Rouxel <benjamin.rouxel@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "yasmin_utils.h"
#include "yasmin_api.h"
#include "task.h"
#include "hw_accelerators.h"

#if __INTERNAL_VERSION_SELECT == YAS_VERSION_SELECT_TRIVIAL
// always select the first version
yas_version_id_t y_task_version_select_trivial(const yas_intern_task_t *tdata) {
    UNUSED_ARGUMENT(tdata);
    return 0;
}
#elif __INTERNAL_VERSION_SELECT == YAS_VERSION_SELECT_LAXITY
//return the longest version that can fit
yas_version_id_t y_task_version_select_laxity(const yas_intern_task_t *tdata) {
    uint32_t version, best_version;
    uint64_t best_wcet = (uint64_t)-1;
    
    for(version=0 ; version < tdata->versions_size ; ++version) {
        if(tdata->versions[version].select_properties.WCET < (tdata->last_activation+tdata->deadline) - yas_utils_now()
            && tdata->versions[version].select_properties.WCET < best_wcet) {
            best_version = version;
            best_wcet = tdata->versions[version].select_properties.WCET;
        }
    }
    return best_version;
}
#elif __INTERNAL_VERSION_SELECT == YAS_VERSION_SELECT_ENERGY
yas_version_id_t y_task_version_select_energy(const yas_intern_task_t *tdata) {
    uint32_t version, best_version;
    uint64_t best_energy = (uint64_t)-1;
    
    for(version=0 ; version < tdata->versions_size ; ++version) {
        if(tdata->versions[version].select_properties.energy_budget < tdata->versions[version].select_properties.get_battery_status()
            && tdata->versions[version].select_properties.energy_budget < best_energy) {
            best_version = version;
            best_energy = tdata->versions[version].select_properties.energy_budget;
        }
    }
    return best_version;
}
#elif __INTERNAL_VERSION_SELECT == YAS_VERSION_SELECT_EXEC_MODE
yas_version_id_t y_task_version_select_exec_mode(const yas_intern_task_t *tdata) {
    uint32_t version;
    int candidate = 0;
    
    for(version=0 ; version < tdata->versions_size ; ++version) {
        if(tdata->versions[version].select_properties.exec_mode >= tdata->versions[version].select_properties.get_current_exec_mode()) {
            candidate = version;
#ifdef YAS_HWACCEL_SIZE
            if(tdata->versions[version].resource == YAS_RESOURCE_NO_RESOURCE_USED || y_hwaccel_is_available(tdata->versions[version].resource ))
#endif
            return version;
        }
    }
    
    return candidate;
}
#elif __INTERNAL_VERSION_SELECT == YAS_VERSION_SELECT_PERMISSION
yas_version_id_t y_task_version_select_permission(const yas_intern_task_t *tdata) {
    uint32_t version;
    int candidate = 0;
    
    for(version=0 ; version < tdata->versions_size ; ++version) {
        if(tdata->versions[version].select_properties.is_allowed(tdata->versions[version].select_properties.permission)) {
            candidate = version;
#ifdef YAS_HWACCEL_SIZE
            if(tdata->versions[version].resource == YAS_RESOURCE_NO_RESOURCE_USED || y_hwaccel_is_available(tdata->versions[version].resource ))
#endif
                return version;
        }
    }
    
    return candidate;
}
#elif __INTERNAL_VERSION_SELECT == YAS_VERSION_SELECT_ENERGYTIME_TRADEOFF
yas_version_id_t y_task_version_select_energytime_tradeoff(const yas_intern_task_t *tdata) {
    uint32_t version, best_version = 0;
    uint64_t best_energy = (uint64_t)-1;
    
    for(version=0 ; version < tdata->versions_size ; ++version) {
        if(tdata->versions[version].select_properties.WCET < (tdata->last_activation+tdata->deadline) - yas_utils_now()
            && tdata->versions[version].select_properties.energy_budget < best_energy) {
            best_version = version;
            best_energy = tdata->versions[version].select_properties.energy_budget;
        }
    }
    return best_version;
}
#endif
