/*
 * Copyright (C) 2020 Benjamin Rouxel <benjamin.rouxel@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "yasmin_utils.h"
#include "synchronisation.h"

extern inline yas_time_t yas_utils_now() {
    struct timespec sched_time;
    int err = clock_gettime(YAS_CLOCK, &sched_time);
    posix_check_err(err, "clock_gettime");
    return (yas_time_t)sched_time.tv_sec * 1000000000 + (yas_time_t)sched_time.tv_nsec;
}

extern inline short yas_utils_cmp_time(const yas_time_t a, const yas_time_t b) {
    if(a < b) return -1;
    if(a > b) return 1;
    return 0;
}

void yas_utils_nanosleep(yas_time_t wait) {
    struct timespec twait;
    
//    int err = clock_gettime(YAS_CLOCK, &twait);
//    posix_check_err(err, "clock_gettime");
//    twait.tv_sec += (__syscall_slong_t)(wait / 1000000000);
//    twait.tv_nsec += (__syscall_slong_t)(wait % 999999999);
//    if(twait.tv_nsec > 1000000000) {
//        ++twait.tv_sec;
//        twait.tv_nsec -= 1000000000;
//    }
//    
//    while(clock_nanosleep(YAS_CLOCK, TIMER_ABSTIME, &twait, &twait) == EINTR) ;
     
    
    twait.tv_sec = (__syscall_slong_t)(wait / 1000000000);
    twait.tv_nsec = (__syscall_slong_t)(wait % 999999999);
    if(twait.tv_nsec > 1000000000) {
        ++twait.tv_sec;
        twait.tv_nsec -= 1000000000;
    }
    
    while(clock_nanosleep(YAS_CLOCK, 0, &twait, &twait) == EINTR) ;
    
    yas_time_t tresume = yas_utils_now() + wait;
    while(yas_utils_now() < tresume) y_sync_atomic_fence();
}

yas_time_t y_utils_gcd_time(yas_time_t a, yas_time_t b) {
//    if((a.tv_sec == 0 && a.tv_nsec == 0) || (b.tv_sec == 0 && b.tv_nsec == 0)) return a;
    if(a == 0 || b == 0) return a;
    if(yas_utils_cmp_time(a, b) == 0) return a;
    while(yas_utils_cmp_time(a, b) != 0) {
//        printf("=> %llu - %llu\n", a, b);
        if(yas_utils_cmp_time(a, b) == 1)
            a = a - b;
        else
            b = b - a;
    }
    return a;
}

yas_time_t y_utils_gcd_arr_time(const yas_time_t *array, const size_t size) {
    yas_time_t result = array[0]; 
    size_t i = 1 ;
    for (; i < size ; ++i) { 
        result = y_utils_gcd_time(array[i], result);
        if(result == 1)
           return result; 
    } 
    return result; 
}

yas_time_t y_utils_lcm_arr_time(const yas_time_t *array, const size_t size) {
    yas_time_t result = array[0]; 
    size_t i = 1 ;
    for (; i < size ; ++i) {
        result = (array[i] * result) / y_utils_gcd_time(array[i], result);
    }
    return result;
}
