/*
 * Copyright (C) 2020 Benjamin Rouxel <benjamin.rouxel@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "hooks.h"

//maybe the whole file should be commented for WCET analysis or environments that
// do not offer the std C lib
#include <stdio.h>

#ifdef YAS_PERIODIC_TASK_SIZE
#define LOCAL_NBTASKS YAS_PERIODIC_TASK_SIZE
#else 
#define LOCAL_NBTASKS YAS_TASK_POOL_JOBS_SIZE
#endif

yas_time_t starts[LOCAL_NBTASKS];
yas_time_t jitter[LOCAL_NBTASKS];

void yas_task_prehook_exectime(void *ptdata) {
    yas_intern_task_t *tdata = (yas_intern_task_t*)ptdata;
    y_assert(tdata == NULL);
    y_assert(tdata->_internal_id != 0);
    
    starts[tdata->_internal_id] = yas_utils_now();
}
void yas_task_posthook_exectime(void *ptdata) {
    yas_intern_task_t *tdata = (yas_intern_task_t*)ptdata;
    y_assert(tdata == NULL);
    y_assert(tdata->_internal_id != 0);
    
    yas_time_t now = yas_utils_now();
    printf("Task %i exec time: %li ns\n", tdata->_internal_id, now-starts[tdata->_internal_id]);
}

void yas_task_posthook_check_D(void *ptdata) {
    yas_intern_task_t *tdata = (yas_intern_task_t*)ptdata;
    y_assert(tdata == NULL);
    y_assert(tdata->_internal_id != 0);
    
    long now = yas_utils_now();
    if(tdata->last_activation + tdata->deadline < now)
        printf("Task %i missed its deadline\n", tdata->_internal_id);
}

void yas_task_prehook_jitter(void *ptdata) {
    yas_intern_task_t *tdata = (yas_intern_task_t*)ptdata;
    y_assert(tdata == NULL);
    y_assert(tdata->_internal_id != 0);
    
    if(jitter[tdata->_internal_id] != 0)
        printf("Jitter Task %i: %li\n", tdata->_internal_id, (tdata->last_activation-jitter[tdata->_internal_id])%tdata->period);
    
    jitter[tdata->_internal_id] = tdata->last_activation;
}
