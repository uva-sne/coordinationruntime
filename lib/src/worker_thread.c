/*
 * Copyright (C) 2020 Benjamin Rouxel <benjamin.rouxel@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "worker_thread.h"
#include "task_core_mapper.h"

static volatile y_thread_pool_t glob_pool; //! the single instance of thread pool
static y_barrier_t pool_init_complete; //! a barrier that ensures the application can start as all threads are configured and ready

/**
 * This is the function executed by the system thread.
 * It will fetch the task to execute from the ready queue, and execute them
 * 
 * @param data
 * @return 
 */
static void* y_thread_pool_thread_proxy(volatile void* data);
/**
 * Initialise a thread as a constructor would do, and start the system thread
 * 
 * @param 
 * @param affinity
 * @param priority
 * @return 
 */
static ybool y_thread_pool_thread_new(volatile y_worker_thread_t *, int affinity, int priority);

// Need a marker function to get a dummy address as NULL can't be used here
//  NULL, instead, marks the absence of function to execute
static void y_dummy_closing_func_mark(void *dummy) { UNUSED_ARGUMENT(dummy); /*printf("closing\n");*/}
static yas_intern_task_t ending_mark;

#if YAS_PREEMPTION_MODEL == YAS_SCHED_PREEMPTIVE
static void y_thread_pool_preemption(int signum, siginfo_t *info, void *context) {
    yas_intern_task_t * t = NULL, *bk_cur_task;
    y_system_thread thid;
    size_t i;
    volatile y_worker_thread_t *thread = NULL;
    UNUSED_ARGUMENT(signum);
    UNUSED_ARGUMENT(info);
    UNUSED_ARGUMENT(context);

    thid = y_system_thread_self();
    y_trace(TRACE_BEGIN_PREEMPTION, thid);
    if(__builtin_expect(!glob_pool.running, 0)) {
        y_trace(TRACE_END_PREEMPTION, thid);
        return;
    }
    
    DEBUG_MSG(("y_thread_pool_preemption 0x%lx.", thid))
    for (i = 0; i < YAS_THREAD_SIZE; ++i) {
        if(glob_pool.threads[i].system_thread == thid) {
            thread = &(glob_pool.threads[i]);
            break;
        }
    }
    if(__builtin_expect(thread == NULL, 0)) {
        DEBUG_MSG(("Can't find thread %lx in pool", thid));
        y_trace(TRACE_END_PREEMPTION, thid);
        return;
    }
    
    if(__builtin_expect(thread->current_task == NULL, FALSE)) {
        y_trace(TRACE_END_PREEMPTION, thid);
        return;
    }
    y_mapper_get_next_task_if_higher_prio(&glob_pool, thread, thread->current_task->priority, &t);
    if(__builtin_expect(t == NULL || t->_internal_thd_func == ending_mark._internal_thd_func, FALSE)) {
        y_trace(TRACE_END_GET_TASK, y_system_thread_self());
        y_trace(TRACE_END_PREEMPTION, thid);
        return;
    }

    DEBUG_MSG(("Preemption on thread 0x%lx (%i) from task %s on task %s\n", 
            thid, thread->affinity, t->name, thread->current_task->name));

    bk_cur_task = thread->current_task;
    thread->current_task = t;

    y_trace(TRACE_END_GET_TASK, y_system_thread_self());
    y_trace(TRACE_SWITCH_TO, thid);
    y_trace(TRACE_BEGIN_ACTIVATE_TASK, t->_internal_id);
#ifdef __INTERNAL__C_MAP_THRD_GLOBAL
    thread->current_task->virt_code_id = thread->affinity;
#endif
    y_thread_pool_preempt(t, t->_internal_thd_func);

    y_trace(TRACE_END_FINALIZE_TASK, t->_internal_id);
    y_trace(TRACE_BEGIN_SWITCH_AWAY, thid);
    thread->current_task = bk_cur_task;

    DEBUG_MSG(("Preemption done on 0x%lx, return to previous task %s", thid, thread->current_task->name));
    y_trace(TRACE_END_SWITCH_AWAY, thid);
}
#endif

static void* y_thread_pool_thread_proxy(volatile void* data) {
    volatile y_worker_thread_t* thread;
    y_assert_return_val(data == NULL, NULL);
    
    thread = (volatile y_worker_thread_t*) data;

    DEBUG_MSG(("thread %lx started.", y_system_thread_self()));
    
    y_barrier_wait(&pool_init_complete);
    
    while(glob_pool.running) {
        // wait for new task
        y_mapper_get_next_task(&glob_pool, thread, &thread->current_task);
        if(__builtin_expect(thread->current_task == NULL, FALSE)) {
            y_trace(TRACE_END_GET_TASK, y_system_thread_self());
            continue;
        }
        // if not running, or the fetched task is the ending marker, then stop
        if (__builtin_expect(!glob_pool.running || thread->current_task->_internal_thd_func == ending_mark._internal_thd_func, FALSE)) {
            y_trace(TRACE_END_GET_TASK, y_system_thread_self());
            thread->current_task = NULL;
            break;
        }

        /* A task was received and the thread pool is active,
         * so execute the function.
         */
        DEBUG_MSG(("thread 0x%lx (%i) calling func %s.",
                y_system_thread_self(), thread->affinity, thread->current_task->name));

        y_trace(TRACE_END_GET_TASK, y_system_thread_self());
        y_trace(TRACE_BEGIN_ACTIVATE_TASK, thread->current_task->_internal_id);
#ifdef __INTERNAL__C_MAP_THRD_GLOBAL
        thread->current_task->virt_code_id = thread->affinity;
#endif
        thread->current_task->_internal_thd_func(thread->current_task);
        y_trace(TRACE_END_FINALIZE_TASK, thread->current_task->_internal_id);

        DEBUG_MSG(("thread 0x%lx (%i) DONE calling func %s.",
                y_system_thread_self(), thread->affinity, thread->current_task->name));
        thread->current_task = NULL;
    }
    DEBUG_MSG(("thread %lx ending mapped on %i.", y_system_thread_self(), thread->affinity));

    return NULL;
}

void y_thread_pool_new() {
    size_t i;
    int err;
    struct sigaction act;
    sigset_t set; //other signals to block while this signal's handler executes
    volatile y_worker_thread_t* thread;
    
    y_task_init(&ending_mark);
    ending_mark._internal_id = 666;
    ending_mark.name[0] = 'e';
    ending_mark.name[1] = '\0';
    ending_mark._internal_thd_func = y_dummy_closing_func_mark;
    ending_mark.priority = YAS_HIGHEST_PRIORITY;
    ending_mark.last_activation = 0;
    ending_mark.last_completion = 1;
    ending_mark.deadline = 1;
    
    y_barrier_init(&pool_init_complete, YAS_THREAD_SIZE+1);  //+1 for the main

#if YAS_PREEMPTION_MODEL == YAS_SCHED_PREEMPTIVE    
    act.sa_flags = SA_SIGINFO;
    //release handler
    act.sa_sigaction = y_thread_pool_preemption;
    err = sigemptyset(&set);
    posix_check_err(err, "sigemptyset");
    err = sigaddset(&set, PREEMPTION_SIGNAL);
    posix_check_err(err, "sigaddset");
    act.sa_mask = set;
    err = sigaction(PREEMPTION_SIGNAL, &act, NULL);
    posix_check_err(err, "sigaction");
#endif

    glob_pool.running = TRUE;
    
#ifdef _lint
    int mapping[YAS_THREAD_SIZE];
    int priority[YAS_THREAD_SIZE];
#else 
    /*compiler check if the given size of the array for the mapping and priority are correct */
    DECLARE_VALID_ARRAY(int, mapping, YAS_THREAD_SIZE, YAS_THREAD_AFFINITIES, "Missing YAS_THREAD_AFFINITIES in yasmin_config.h or its size is not equal to YAS_THREAD_SIZE");
    DECLARE_VALID_ARRAY(int, priority, YAS_THREAD_SIZE, YAS_THREAD_PRIORITIES, "Missing YAS_THREAD_PRIORITIES in yasmin_config.h or its size is not equal to YAS_THREAD_SIZE");
#endif
    
    for (i = 0; i < YAS_THREAD_SIZE; ++i) {
        thread = &(glob_pool.threads[i]);
        thread->thid = i;
        if (!y_thread_pool_thread_new(thread, mapping[i], priority[i])) {
            break;
        }
    }
    
    y_mapper_init(&glob_pool);
    
    y_barrier_wait(&pool_init_complete);
    y_barrier_destroy(&pool_init_complete);
}

void y_thread_pool_free() {
    y_assert(glob_pool.running); // join should have been called before

    y_mapper_cleanup(&glob_pool);
}

void y_thread_pool_join_all() {
    int i;
    volatile y_worker_thread_t *thread;

    y_assert(!glob_pool.running);

    glob_pool.running = FALSE;
    
    y_mapper_wakeup_all(&glob_pool, &ending_mark);

    for (i = 0; i < YAS_THREAD_SIZE; i++) {
        thread = &(glob_pool.threads[i]);
        y_system_thread_join(&thread->system_thread, NULL);
    }
}

static inline ybool y_thread_pool_thread_new(volatile y_worker_thread_t *thread, int affinity, int priority) {
    thread->affinity = affinity;
    return y_system_thread_create(&thread->system_thread, y_thread_pool_thread_proxy, thread, affinity, priority);
}

void y_thread_pool_block_signals() {
    sigset_t set;
    int err;
    err = sigemptyset(&set);
    posix_check_err(err, "sigemptyset");
    err = sigaddset(&set, PREEMPTION_SIGNAL);
    posix_check_err(err, "sigaddset");
    err = pthread_sigmask(SIG_BLOCK, &set, NULL);
    posix_check_err(err, "pthread_sigmask");
}

void y_thread_pool_unblock_signals() {
    sigset_t set;
    int err;
    err = sigemptyset(&set);
    posix_check_err(err, "sigemptyset");
    err = sigaddset(&set, PREEMPTION_SIGNAL);
    posix_check_err(err, "sigaddset");
    err = pthread_sigmask(SIG_UNBLOCK, &set, NULL);
    posix_check_err(err, "pthread_sigmask");
}

extern inline void y_thread_pool_push(yas_intern_task_t *data) {
    y_assert(!glob_pool.running);
    y_mapper_push_unlocked(&glob_pool, data);
}

extern inline void y_worker_thread_sched_notify() {
    y_assert(!glob_pool.running);
    y_mapper_sched_notify(&glob_pool);
}

extern inline void y_thread_pool_lock_queue(){
    y_assert(!glob_pool.running);
    y_mapper_lock_queue(&glob_pool);
}
extern inline void y_thread_pool_unlock_queue() {
    y_assert(!glob_pool.running);
    y_mapper_unlock_queue(&glob_pool);
}

#ifdef MODE_TEST
ybool mt_are_ready_queues_empty() {
#if YAS_TASK_MAPPING == YAS_MAPPING_GLOBAL_SCHEME
    if(y_job_pool_length(&glob_pool.queue) > 0)
        return FALSE;
    for(size_t i = 0; i < YAS_THREAD_SIZE; ++i) {
        if(glob_pool.threads[i].current_task != NULL)
            return FALSE;
    }
#else     
    for (size_t i = 0; i < YAS_THREAD_SIZE; ++i) {
        if(y_job_pool_length(&glob_pool.threads[i].queue) > 0)
            return FALSE;
        if(glob_pool.threads[i].current_task != NULL)
            return FALSE;
    }
#endif
    return TRUE;
}
#endif
