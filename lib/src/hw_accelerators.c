/*
 * Copyright (C) 2020 Benjamin Rouxel <benjamin.rouxel@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "hw_accelerators.h"

#include "worker_thread.h"
#include "trace.h"
#include "locking.h"

#ifdef YAS_HWACCEL_SIZE

/**
 * The possible status of a hardware accelerator
 */
enum coord_resource_status {
    RESOURCE_FREE, RESOURCE_BUSY
};

/**
 * Describe a resource hardware accelerator
 */
typedef struct {
    char * name; //! a label
    volatile enum coord_resource_status status; //! its current status
    uint32_t waiting_size; //! count the number of waiting tasks
    yas_intern_task_t *waiter[YAS_TASK_POOL_JOBS_SIZE]; //! contains the waiting task, upper bounded with the number of jobs
    y_sem_t mutex_waiter; //! a mutex to access the waiting task list
} resource_t;

/**
 * Describes the list of available hardware resources
 * 
 * The ID of a hardware accelerators is its index in the below array
 */
typedef struct {
    size_t nb_resources; //! count the number of hw accel
    resource_t ressources[YAS_HWACCEL_SIZE]; //! store the hw accel
} resources_t;
resources_t glob_resources;

void y_hwaccel_init() {
    glob_resources.nb_resources = 0;
}

yas_hwaccel_id_t yas_hwaccel_decl(char *name) {
    uint32_t index;
    resource_t *r;
    y_assert_return_val(glob_resources.nb_resources >= YAS_HWACCEL_SIZE, YAS_RESOURCE_NO_RESOURCE_USED);
    index = glob_resources.nb_resources++;
    r = &glob_resources.ressources[index];
    r->name = name;
    r->status = RESOURCE_FREE;
    r->waiting_size = 0;
    y_sem_init(&r->mutex_waiter, 0, 1);
    return index;
}

ybool y_hwaccel_acquire(yas_hwaccel_id_t id, yas_intern_task_t *requester) {
    enum coord_resource_status desired = RESOURCE_BUSY, expected = RESOURCE_FREE;
    ybool acquired = FALSE;
    uint32_t idwaiter;
    y_assert_return_val(id < 0 || id >= glob_resources.nb_resources, FALSE);
    
    acquired = y_sync_atomic_CAS(&(glob_resources.ressources[id].status), &expected, &desired) ? TRUE : FALSE;
    if(!acquired) {
        y_sem_wait(&(glob_resources.ressources[id].mutex_waiter)); // wait max nbthread
        idwaiter = glob_resources.ressources[id].waiting_size++;
        glob_resources.ressources[id].waiter[idwaiter] = requester;
        y_sync_atomic_fence();
        y_sem_post(&(glob_resources.ressources[id].mutex_waiter));
    }
    else
        y_trace(TRACE_RESOURCE_ACQUIRE, id);
    
    return acquired;
}
void y_hwaccel_release(yas_hwaccel_id_t id) {
    enum coord_resource_status expected = RESOURCE_FREE;
    uint32_t idwaiter;
    yas_intern_task_t* waiter = NULL;
    y_assert(id < 0 || id >= glob_resources.nb_resources);
    
    y_sync_atomic_store(&(glob_resources.ressources[id].status), &expected);
    y_sync_atomic_fence();
    
    y_trace(TRACE_RESOURCE_RELEASE, id);
    
    y_sem_wait(&(glob_resources.ressources[id].mutex_waiter)); // wait max nbthread
    if(glob_resources.ressources[id].waiting_size > 0) {
        idwaiter = --glob_resources.ressources[id].waiting_size;
        waiter = glob_resources.ressources[id].waiter[idwaiter];
    }
    y_sem_post(&(glob_resources.ressources[id].mutex_waiter));
    if(waiter != NULL)
        waiter->_internal_thd_func(waiter);
}
ybool y_hwaccel_is_available(yas_hwaccel_id_t id) {
    enum coord_resource_status st;
    y_assert_return_val(id < 0 || id >= glob_resources.nb_resources, FALSE);
    
    y_sync_atomic_load(&(glob_resources.ressources[id].status), &st);
    return st == RESOURCE_FREE;
}

#ifdef MODE_TEST
size_t mt_get_nb_resources() {
    return glob_resources.nb_resources;
}
char * mt_get_resource_name(size_t i) {
    return glob_resources.ressources[i].name;
}
#endif

#endif /* YAS_HWACCEL_SIZE */
