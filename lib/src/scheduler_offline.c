/*
 * Copyright (C) 2020 Benjamin Rouxel <benjamin.rouxel@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * \file scheduler_offline.c
 * 
 * \short Implement the function for the on-line dispatcher for off-line schedules
 * 
 * Provide the implementation of the `yasmin_api.h' for an on-line dispatcher of
 * off-line schedules.
 * 
 * The static schedule is stored in a 2D array where for each virtual core there
 * is the list of ordered (by starting time) tasks to execute.
 * A special task is placed at the end of this 2D array to symbolise the
 * beginning of the next iteration of the static schedule.
 * 
 * After starting the thread pool, a single task that never ends is pushed into the
 * ready queue. This special task will start executing the user
 * tasks following the order of the 2D array. If the current task is not deemed to 
 * execute immediately (like idle time in the static schedule), then the required
 * amount of time is waited for the task to start at the time decided by the 
 * off-line scheduler.
 * 
 * \remark no user tasks are pushed into the ready queue of threads
 * 
 * Once all the tasks on a specific thread have been executed, the thread 
 * enters into a special function start_iteration that will wait for all other
 * threads. Once they all are in their respective `start_iteration', a new iteration
 * of the static schedule starts immediately.
 */
#include "yasmin_api.h"

#ifdef __INTERNAL_YAS_OFFLINE_SCHED

#include "yasmin_utils.h"
#include "worker_thread.h"
#include "job_pool.h"
#include "trace.h"
#include "memorymgnt.h"

#include "pp-loop.h"

#define __INTERNAL_YAS_STATIC_TASK_SIZE YAS_TASK_POOL_JOBS_SIZE+1 //+1 for the start iteration

/**
 * Describe the internal structure of the data required by the scheduler.
 */
typedef struct {
    volatile ybool running; //! TRUE if the schedule is running
    y_cond_t iteration; //! conditionnal barrier to wait on for the next iteration
    y_mutex_t iteration_mutex; //! mutex for the conditional barrier
    size_t nb_wait_iter; //! count how many are already waiting to start a new iteration
    int task_id_cnt; //! count how many tasks have been added to give a uniq ID
    
    yas_intern_task_t sched[YAS_THREAD_SIZE][__INTERNAL_YAS_STATIC_TASK_SIZE]; //! 2D array representing the schedule per thread
    size_t sched_size[YAS_THREAD_SIZE]; //! count how many tasks have been added per thread in the 2D array
    yas_time_t sched_origin[YAS_THREAD_SIZE]; //! stores the actual time per thread which corresponds the theoretical time 0
    
    yas_intern_task_t thread_tasks[YAS_THREAD_SIZE]; //! contains the tasks pushed into the ready queue
} y_task_manager_t;
static y_task_manager_t glob_static_tasks;

/**
 * Expanding this macro will result in a static_sched_start_iteration_CORE function
 * e.g. static_sched_start_iteration_1 for virtual core 1
 * 
 * @param func
 * @param mapping
 */
#define START_ITERATION_DECL(CORE, _) static void CAT(static_sched_start_iteration_,CORE)(void *unused){\
    DEBUG_MSG(("thread %lx mapped on %i wait start iteration", y_system_thread_self(), CORE));\
    y_mutex_wait(&glob_static_tasks.iteration_mutex); \
    if(!glob_static_tasks.running) {y_mutex_post(&glob_static_tasks.iteration_mutex); return; } \
    y_sync_atomic_incr(&glob_static_tasks.nb_wait_iter);\
    if(glob_static_tasks.nb_wait_iter < YAS_THREAD_SIZE) {\
        y_cond_wait(&glob_static_tasks.iteration, &glob_static_tasks.iteration_mutex); \
    } \
    else {\
        glob_static_tasks.nb_wait_iter = 0;\
        y_cond_broadcast(&glob_static_tasks.iteration);\
    }\
    y_mutex_post(&glob_static_tasks.iteration_mutex); \
    glob_static_tasks.sched_origin[CAT(CORE,)] = yas_utils_now(); \
    y_sync_atomic_fence();\
}

/**
 * Expaning this macro will result in the call to insert the special function
 *  static_sched_start_iteration_CORE into the scheduler structure
 * 
 * @param func
 * @param mapping
 */
#define START_ITERATION_CALL(X, _) y_insert_iteration_synchro(CAT(static_sched_start_iteration_,X), X);

/**
 * This expands at compile time into the declaration of as many static_sched_start_iteration_CORE
 * function than there are YAS_THREAD_SIZE in `yasmin_config.h'
 * 
 * E.g: 
 * in yasmin_config.h
 * #define YAS_THREAD_SIZE 3
 * 
 * in this file:
 * static void static_sched_start_iteration_1(...) { ... }
 * static void static_sched_start_iteration_2(...) { ... }
 * static void static_sched_start_iteration_3(...) { ... }
 * 
 * \remark the maximum number this loop can executed is bounded by the CMake
 * variable YAS_NB_ITER_MAX_FOR_PP_LOOP which by default is 1024.
 * 
 * @param func
 * @param mapping
 */
/* Generate all the different start iteration function */
EVAL(REPEAT(YAS_THREAD_SIZE, START_ITERATION_DECL, ~)) 

/**
 * Insert the function static_sched_start_iteration_CORE in the scheduler structure
 * 
 * @param func
 * @param mapping
 */
static void y_insert_iteration_synchro(yas_task_func_t func, int mapping);
/**
 * Remove the function static_sched_start_iteration_CORE from the scheduler structure
 */
static void y_rem_iteration_synchro();

/**
 * These functions are here for compatibility reason with the online scheduler
 */
yas_time_t yas_get_sched_clock() { return 0; }
void yas_task_activate(yas_task_id_t) {}
yas_hwaccel_id_t yas_hwaccel_decl(char *) { return -1; }
void yas_hwaccel_use(yas_task_id_t, yas_version_id_t, yas_hwaccel_id_t) {}
ybool yas_task_alter_period(yas_task_id_t id, yas_time_t period) {return TRUE; }
ybool yas_task_alter_deadline(yas_task_id_t id, yas_time_t deadline) {return TRUE; }
ybool yas_task_alter_priority(yas_task_id_t id, size_t priority) {return TRUE; }

/**
 * Task executed by each worker thread
 * 
 * It gets its mapping affinity (worker thread not physical core), and then
 * loop on its tasks from the 2D array representing the schedule.
 * 
 * @param ptdata
 */
static void thread_task_wrapper(void *ptdata) {
    yas_intern_task_t *tdata;
    yas_intern_task_t *task;
    int mapid;
    int sched_index = 0;
    int sched_size;
    yas_time_t *sched_origin;
    yas_intern_task_t *sched;
    yas_time_t now;
    yas_version_id_t vid = 0;
    yas_task_func_t func ;//= dummy_function; //avoid segfault if no variant selected, should I raise something?
    void* user_data;
    
    tdata = (yas_intern_task_t*) ptdata;
    y_assert(tdata == NULL);
    
    mapid = tdata->virt_code_id;
    sched_size = glob_static_tasks.sched_size[mapid];
    sched_origin = &(glob_static_tasks.sched_origin[mapid]);
    sched = &(glob_static_tasks.sched[mapid][0]);
    
    
    now = yas_utils_now();
    *sched_origin = now;
    
    do {
        y_trace(TRACE_BEGIN_SCHEDULING, mapid);
        task = &(sched[sched_index]);
        sched_index = (sched_index + 1) % sched_size;

        long long wait = *sched_origin + task->offset - now; // no unsigned here to catch inf to 0 value and therefore no wait
        if(wait > 0) {
            DEBUG_MSG(("thread %li mapped on %i wait %lu",
                        y_system_thread_self(), mapid, wait));
            yas_utils_nanosleep(wait);
        }
        else 
            y_trace_val(TRACE_EVENT_LATENCY, task->_internal_id, -1*wait);
        y_trace(TRACE_END_SCHEDULING, mapid);
        
        if(!glob_static_tasks.running) {break; }
        y_trace(TRACE_BEGIN_ACTIVATE_TASK, task->_internal_id);
        
#ifdef YAS_HOOK_PRE_TASK_CALL
        YAS_HOOK_PRE_TASK_CALL(task);
#endif
        
#ifdef YAS_TASK_VERSION_SIZE //the user wants multiple version
        vid = YAS_VERSION_SELECT_FUNCTION(task);
#endif
        
        func = task->versions[vid].func;
        user_data = task->versions[vid].fix_func_args;
        
        y_trace(TRACE_END_ACTIVATE_TASK, task->_internal_id);
        y_trace(TRACE_ACTIVATED_TASK_VARIANT, (size_t)func);

        func(user_data);
        
        y_trace(TRACE_BEGIN_FINALIZE_TASK, task->_internal_id);

#ifdef YAS_HOOK_POST_TASK_CALL
        YAS_HOOK_POST_TASK_CALL(task);
#endif
        y_trace(TRACE_END_FINALIZE_TASK, task->_internal_id);
        now = yas_utils_now();
    }
    while(glob_static_tasks.running);
}

/**
 * Used to compare offset in order to order the list of tasks
 * @param ta
 * @param tb
 * @return 
 */
static int y_compare_offset(const yas_intern_task_t *ta, const yas_intern_task_t *tb) {
    return yas_utils_cmp_time(ta->offset, tb->offset);
}

/**
 * Copy the necesary information from the one provided by the user to the
 * internal data structure.
 * @param dst
 * @param src
 */
static void y_task_copy_userdata(yas_intern_task_t *dst, const yas_task_data_t *src) {
    size_t i;
#ifdef __cplusplus
    dst->name = src->name;
#else
    for(i = 0 ; i < 256 && src->name[i] != '\0' ; ++i)
        dst->name[i] = src->name[i];
#endif
    dst->virt_code_id = src->virt_core_id;
    dst->offset = src->offset;
}

void yas_init() {
    
    y_system_lock_memory();
    
#ifdef YAS_MAIN_THREAD_AFFINITY
    y_system_thread_setaffinity(y_system_thread_self(), YAS_MAIN_THREAD_AFFINITY);
#endif
#ifdef YAS_MAIN_THREAD_PRIORITY
    y_system_thread_setpriority(y_system_thread_self(), YAS_MAIN_THREAD_PRIORITY);
#endif
    
    y_cond_init(&glob_static_tasks.iteration);
    y_mutex_init(&glob_static_tasks.iteration_mutex);
    glob_static_tasks.nb_wait_iter = 0;
    glob_static_tasks.task_id_cnt = 0;

    y_thread_pool_new();

    glob_static_tasks.running = FALSE;
    for(size_t i=0 ; i < YAS_THREAD_SIZE ; ++i)
        glob_static_tasks.sched_size[i] = 0;

#ifdef YAS_HWACCEL_SIZE
    y_hwaccel_init();
#endif
    
#ifdef YAS_TRACE
    y_trace_init();
#endif
}

void yas_cleanup() {
    y_assert(glob_static_tasks.running);
    y_thread_pool_join_all();
    
    y_thread_pool_free();
    y_mutex_destroy(&glob_static_tasks.iteration_mutex);
    y_cond_destroy(&glob_static_tasks.iteration);
#ifdef YAS_TRACE
    y_trace_print_and_clear();
#endif
    
    y_system_unlock_memory();
}

ybool yas_start() {
    size_t i, j;
    y_assert_return_val(glob_static_tasks.running, FALSE);
    
    y_thread_pool_lock_queue();
    for(i=0 ; i < YAS_THREAD_SIZE ; ++i) {
        y_task_sort_array(glob_static_tasks.sched[i], y_compare_offset, 0, glob_static_tasks.sched_size[i]);

        yas_intern_task_t *st = &(glob_static_tasks.thread_tasks[i]);
        st->_internal_id = 42;
        st->_internal_thd_func = thread_task_wrapper;
        char *tmp = "off-sched";
        for(j = 0 ; j < 9 ; ++j)
            st->name[j] = tmp[j];
        st->versions_size = 1;
        st->virt_code_id = i;

        y_thread_pool_push(st);
    }
    // We had the iteration task synchronisation at the end
    //   it must be removed by coord_stop()
    EVAL( REPEAT ( YAS_THREAD_SIZE, START_ITERATION_CALL, ~ ) )
    
    glob_static_tasks.running = TRUE;
    y_thread_pool_unlock_queue();
    
    return TRUE;
}

void yas_stop() {
    y_assert(!glob_static_tasks.running);
    
    glob_static_tasks.running = FALSE;
    
    y_mutex_wait(&glob_static_tasks.iteration_mutex);
    y_cond_broadcast(&glob_static_tasks.iteration);
    y_mutex_post(&glob_static_tasks.iteration_mutex);
    
    // Remove the iteration synchronisation task placed at the end
    y_rem_iteration_synchro();
}

void y_insert_iteration_synchro(yas_task_func_t func, int mapping) {
    size_t j;
    yas_intern_task_t *tdata;
    
    for(j = glob_static_tasks.sched_size[mapping] ; j > 0 ; --j) {
        glob_static_tasks.sched[mapping][j] = glob_static_tasks.sched[mapping][j-1];
    }
    glob_static_tasks.sched_size[mapping]++;
    tdata = &(glob_static_tasks.sched[mapping][0]);
    tdata->_internal_id = glob_static_tasks.task_id_cnt++;
#ifdef __cplusplus
    tdata->name = (char*)"start_iteration";
#else
    char *tmp = "start_iteration";
    for(j = 0 ; j < 16 ; ++j)
        tdata->name[j] = tmp[j];
#endif
    tdata->virt_code_id = mapping;
    tdata->offset = 0;
    
    tdata->versions[0].fix_func_args = NULL;
    tdata->versions[0].func = func;
    tdata->versions_size = 1;
}
void y_rem_iteration_synchro() {
    size_t i, j;
    
    for(i=0 ; i < YAS_THREAD_SIZE ; ++i) {
        for(j = 0 ; j < glob_static_tasks.sched_size[i]-1 ; ++j) {
            glob_static_tasks.sched[i][j] = glob_static_tasks.sched[i][j+1];
        }
        glob_static_tasks.sched_size[i]--;
    }
}

yas_task_id_t yas_task_decl(const yas_task_data_t *t, yas_task_func_t func, void *fixed_args) {
    yas_intern_task_t *tdata;
    y_assert_return_val(glob_static_tasks.running, -1);
    y_assert_return_val(t->virt_core_id < 0 || t->virt_core_id > YAS_THREAD_SIZE, -1);
    
    tdata = &(glob_static_tasks.sched[t->virt_core_id][glob_static_tasks.sched_size[t->virt_core_id]++]);
    y_task_init(tdata);
    y_task_copy_userdata(tdata, t);
    
    tdata->_internal_id = glob_static_tasks.task_id_cnt++;

    if(func != NULL) {
        tdata->versions[0].func = func;
        tdata->versions[0].fix_func_args = fixed_args;
        tdata->versions_size = 1;
    }
    
    return tdata->_internal_id;
}

/**
 * Retrieve a task from the 2D array according to its id.
 * 
 * It returns the task, the mapping (virtual core id), and the index in the array
 * 
 * @param id
 * @param task
 * @param mapping
 * @param index
 */
static void y_task_get_task_by_id(yas_task_id_t id, yas_intern_task_t **task, size_t *mapping, size_t *index) {
    size_t task_index;
    size_t map;
    
    y_assert(id < 0 || id >= glob_static_tasks.task_id_cnt);
    
    for(map = 0; map < YAS_THREAD_SIZE ; ++map) {
        for(task_index = 0 ; task_index < glob_static_tasks.sched_size[map] ; ++task_index) {
            if(glob_static_tasks.sched[map][task_index]._internal_id == id) {
                *index = task_index;
                *mapping = map;
                *task = &(glob_static_tasks.sched[map][task_index]);
                break;
            }
        }
    }
}

ybool yas_task_rem(yas_task_id_t id) {
    y_assert_return_val(glob_static_tasks.running, FALSE);
    
    yas_intern_task_t *task = NULL;
    size_t task_index = 0;
    size_t mapping = 0;
    y_task_get_task_by_id(id, &task, &mapping, &task_index);
    y_assert_return_val(task == NULL, FALSE);
    
    for(size_t i=task_index ; i < glob_static_tasks.sched_size[mapping]-1 ; ++i) {
        yas_intern_task_t *dst = &(glob_static_tasks.sched[mapping][i]);
        yas_intern_task_t *src = &(glob_static_tasks.sched[mapping][i+1]);
        y_task_copy(dst, src);
    }
    --glob_static_tasks.sched_size[mapping];
    return TRUE;
}

ybool yas_task_alter_offset(yas_task_id_t id, yas_time_t offset) {
    y_assert_return_val(glob_static_tasks.running, FALSE);
    
    yas_intern_task_t *task = NULL;
    size_t task_index = 0;
    size_t mapping = 0;
    y_task_get_task_by_id(id, &task, &mapping, &task_index);
    y_assert_return_val(task == NULL, FALSE);
    task->offset = offset;
    return TRUE;
}
ybool yas_task_alter_mapping(yas_task_id_t id, int mapping) {
    y_assert_return_val(glob_static_tasks.running, FALSE);
    y_assert_return_val(mapping < 0 || mapping >= YAS_THREAD_SIZE, FALSE);
    
    yas_intern_task_t *task = NULL;
    size_t task_index = 0;
    size_t m = 0;
    y_task_get_task_by_id(id, &task, &m, &task_index);
    y_assert_return_val(task == NULL, FALSE);
    task->virt_code_id = mapping;
    return TRUE;
}

yas_version_id_t yas_version_decl(yas_task_id_t id, yas_task_func_t altfunc, void *static_argument, yas_version_select_t user_properties) {
#ifdef YAS_TASK_VERSION_SIZE //the user wants multiple version
    yas_intern_task_t *task = NULL;
    y_assert_return_val(glob_static_tasks.running, -1);
    
    size_t task_index = 0;
    size_t mapping = 0;
    y_task_get_task_by_id(id, &task, &mapping, &task_index);
    y_assert_return_val(task == NULL, -1);
    y_assert_return_val(task->versions_size >= __INTERNAL_TASK_VERSION_SIZE, -1);
    
    task->versions[task->versions_size].select_properties = user_properties;
    task->versions[task->versions_size].func = altfunc;
    task->versions[task->versions_size].fix_func_args = static_argument;
    return task->versions_size++;
#else
    return -1;
#endif
}

#ifdef YAS_CHANNEL_SIZE
void y_channel_connect(yas_task_id_t srcid, yas_task_id_t sinkid, base_channel_t *channel) {
    yas_intern_task_t *src=NULL, *sink=NULL;
    
    size_t task_index = 0;
    size_t mapping = 0;
    y_task_get_task_by_id(srcid, &src, &mapping, &task_index);
    y_assert_msg(src == NULL, ("Can't find src with id %d\n", srcid));
    y_task_get_task_by_id(sinkid, &sink, &mapping, &task_index);
    y_assert_msg(sink == NULL, ("Can't find sink with id %d\n", sinkid));
    
    channel->src = src;
    channel->sink = sink;
    src->output_channels[src->output_channels_size++] = channel;
    sink->input_channels[sink->input_channels_size++] = channel;
}
#endif /* YAS_CHANNEL_SIZE */

#ifdef MODE_TEST
yas_time_t mt_online_get_sched_period() {
    return 0;
}
y_task_sort_func_t mt_online_get_sort_func() {
    return NULL;
}
ybool mt_online_is_running() {
    return glob_static_tasks.running;
}
yas_intern_task_t* mt_get_recurring_tasks() {
    return NULL;
}
size_t mt_get_recurring_task_size() {
    return 0;
}
yas_intern_task_t* mt_get_nonrecurring_tasks() {
    return NULL;
}
size_t mt_get_nonrecurring_task_size() {
    return 0;
}
#endif /*MODE_TEST*/

#endif //__INTERNAL_YAS_OFFLINE_SCHED
