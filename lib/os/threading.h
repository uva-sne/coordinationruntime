/*
 * Copyright (C) 2020 Benjamin Rouxel <benjamin.rouxel@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * \file threading.h
 * 
 * \short Wrapper function to deal with system threads
 * 
 * Wrapper for POSIX thread (pthread)
 */
#ifndef THREADING_H
#define THREADING_H
    
#include "yasmin_config.h"
#include "check_config.h"
#include "yasmin_utils.h"

#include <pthread.h>
#include <signal.h>
typedef pthread_t y_system_thread;

typedef void*   (*y_system_thread_func)          (volatile void *);

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Create a new thread
 * 
 * @param newthread
 * @param 
 * @param data
 * @param affinity
 * @param priority
 * @return 
 */
ybool y_system_thread_create(volatile y_system_thread *newthread, y_system_thread_func, volatile void *data, int affinity, int priority);
/**
 * Destroy a thread
 * @param newthread
 */
void y_system_thread_destroy(y_system_thread *newthread);
/**
 * Join a thread
 * @param newthread
 * @param ret
 */
void y_system_thread_join(const volatile y_system_thread *newthread, void **ret);
/**
 * Kill a thread
 * @param thid
 * @param signum
 */
void y_system_thread_kill(y_system_thread thid, int signum);
/**
 * Set the mapping of a thread to a physical core
 * @param thrd
 * @param coreid
 */
void y_system_thread_setaffinity(y_system_thread thrd, int coreid);
/**
 * Set the system priority of a thread
 * @param thrd
 * @param prio
 */
void y_system_thread_setpriority(y_system_thread thrd, int prio);

/**
 * Return the thread itself
 * @return 
 */
y_system_thread y_system_thread_self(void);

#ifdef __cplusplus
}
#endif
#endif /* THREADING_H */

