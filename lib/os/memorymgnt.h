/*
 * Copyright (C) 2020 Benjamin Rouxel <benjamin.rouxel@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * \file memorymgnt.h
 * 
 * \short Contains some wrapper for system memory management
 * 
 * As of now it is only to do a lock all pages in memory to avoid swapping
 */
#ifndef MEMORY_H
#define MEMORY_H

#ifdef __cplusplus
extern "C" {
#endif

#include <sys/mman.h>
    
#include "yasmin_config.h"
#include "check_config.h"
#include "yasmin_utils.h"

/**
 * Loge pages in memory to avoid swapping
 */
void y_system_lock_memory(void);
/**
 * Unlock the pages
 */
void y_system_unlock_memory(void);

#ifdef __cplusplus
}
#endif

#endif /* MEMORY_H */

