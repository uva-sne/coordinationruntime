/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include "memorymgnt.h"

void y_system_lock_memory(void) {
    int rc = mlockall(MCL_CURRENT | MCL_FUTURE);
    if (rc != 0)
        posix_check_err(rc, "mlockall");
}

void y_system_unlock_memory(void) {
    int rc = munlockall();
    if (rc != 0)
        posix_check_err(rc, "munlockall");
}
