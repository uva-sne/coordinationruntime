/*
 * Copyright (C) 2020 Benjamin Rouxel <benjamin.rouxel@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "threading.h"

ybool y_system_thread_create(volatile y_system_thread *t, y_system_thread_func func, volatile void *data, int affinity, int priority) {
    pthread_attr_t attr;
    int ret;
    struct sched_param param;

    ret = pthread_attr_init(&attr);
    posix_check_err(ret, "pthread_attr_init(&attr)");

    ret = pthread_attr_setinheritsched(&attr, PTHREAD_EXPLICIT_SCHED);
    posix_check_err(ret, "pthread_attr_setinheritsched");
    ret = pthread_attr_setschedpolicy(&attr, YAS_THREAD_SCHED_POLICY);
    posix_check_err(ret, "pthread_attr_setschedpolicy");
    
    if (priority > 0) {
        param.sched_priority = priority;
        ret = pthread_attr_setschedparam(&attr, &param);
        posix_check_err(ret, "pthread_attr_setschedparam");
    }
    
    if(affinity > -1) {
        cpu_set_t cpuset;
        CPU_ZERO(&cpuset);
        CPU_SET(affinity, &cpuset);
        ret = pthread_attr_setaffinity_np(&attr, sizeof (cpu_set_t), &cpuset);
        posix_check_err(ret, "pthread_attr_setaffinity_np");
    }
    
    ret = pthread_create((y_system_thread*)t, &attr, (void * (*)(void *))func, (void *)data);
    
    if(ret) { //-- If ret  == 22, either put YAS_THREAD_SCHED_POLICY to SCHED_OTHER, or make sure that a priority is given
        posix_check_err(ret, "Error creating thread: %s");
        ret = pthread_attr_destroy(&attr);
        posix_check_err(ret, "pthread_attr_destroy(&attr)");
        return FALSE;
    }
    ret = pthread_attr_destroy(&attr);
    posix_check_err(ret, "pthread_attr_destroy(&attr)");

    ret = pthread_setname_np(*((y_system_thread*)t), YAS_THREAD_NAME);
    posix_check_err(ret, "pthread_setname_np");

    DEBUG_MSG(("Thread %li created", *t));
    return TRUE;
}

void y_system_thread_join(const volatile y_system_thread *t, void **ret) {
    int err = pthread_join(*((y_system_thread*)t), ret);
    posix_check_err(err, "pthread_join");
}

y_system_thread y_system_thread_self(void) {
    return pthread_self();
}

void y_system_thread_setaffinity(y_system_thread thrd, int coreid) {
    int ret;
    cpu_set_t cpuset;
    CPU_ZERO(&cpuset);
    CPU_SET(coreid, &cpuset);
    ret = pthread_setaffinity_np(thrd, sizeof (cpu_set_t), &cpuset);
    posix_check_err(ret, "pthread_attr_setaffinity_np");
}
//
//void c_system_thread_setscheduler(c_system_thread thrd, int schedid) {
//    struct sched_param param;
//    posix_check_err(
//            pthread_setschedparam(thrd, schedid, &param),
//            "pthread_setsched_param"
//        );
//}
//
void y_system_thread_setpriority(y_system_thread thrd, int prio) {
    struct sched_param param;
    int ret;
    param.sched_priority = prio;
    ret = pthread_setschedparam(thrd, YAS_THREAD_SCHED_POLICY, &param);
    posix_check_err(ret, "pthread_setschedprio");
}

void y_system_thread_kill(y_system_thread thid, int signum) {
    int ret = pthread_kill(thid, signum);
    posix_check_err(ret, "pthread_kill");
}
