/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include <check.h>
#include "yasmin_api.h"
#include "trace.h"
#include "../blockcalls.h"
#include "../mode_test_decls.h"

#define MS2NS(X) (X*1000000)
#define S2NS(X)  (X*1000000000l)

yas_time_t exec_time = S2NS(5);

int glob_a;
int limit_a = 1000000;
int limit_b = 10000;
yas_time_t period_a = MS2NS(500);
yas_time_t period_b = MS2NS(250);


void ftask(void *arg) {
    int limit = *((int*)arg);
    int i;
    glob_a = 0;
    for(i=0 ; i < limit ; ++i)
        glob_a += glob_a * i;
}

START_TEST (onePeriodicTask) {
    yas_task_data_t taskA;
    yas_task_id_t aid;
    int i;
    int cnt_begin_sched = 0, cnt_end_sched = 0, 
        cnt_start_task = 0, cnt_end_task = 0, 
        cnt_start_preempt = 0, cnt_end_preempt = 0,
        cnt_req_preempt = 0, 
        cnt_other = 0;
    float num_sched_act, num_task_act;
    yas_time_t last_sched = 0;
    yas_time_t time_start_sched = 0, time_stop_sched = 0, drift_acc = 0;
    
    strncpy(taskA.name, "TA", 256);
    taskA.period = period_a;
    
    yas_init();
    aid = yas_task_decl(&taskA,ftask, (void*)&limit_a);

    ck_assert(!mt_online_is_running());
    ck_assert_int_eq(mt_get_recurring_task_size(), 1);
    ck_assert(yas_start());
    ck_assert(mt_online_is_running());
    ck_assert(mt_online_get_sched_period() == period_a);
    
    yas_utils_nanosleep(exec_time);
    
    yas_stop();
    ck_assert(!mt_online_is_running());
    while(!mt_are_ready_queues_empty()) yas_utils_nanosleep(1000);
    
    for(i=0 ; i < trace_dyn_index ; ++i) {
        if(trace_dyn_buf[i].trid == TRACE_BEGIN_SCHEDULING) {
            ++cnt_begin_sched;
            if(last_sched != 0) {
                //mt_online_get_sched_freq()-1%  <= trace_dyn_buf[i].time-last_sched <= mt_online_get_sched_freq()+1%
                ck_assert_int_ge(trace_dyn_buf[i].time-last_sched, mt_online_get_sched_period()*0.99);
                ck_assert_int_le(trace_dyn_buf[i].time-last_sched, mt_online_get_sched_period()*1.01);
            }
            last_sched = trace_dyn_buf[i].time;
            if(time_start_sched > 0)
                drift_acc += (trace_dyn_buf[i].time-time_start_sched) % mt_online_get_sched_period();
            time_start_sched = trace_dyn_buf[i].time;
        }
        else if(trace_dyn_buf[i].trid == TRACE_END_SCHEDULING) {
            ++cnt_end_sched;
            time_stop_sched = trace_dyn_buf[i].time;
            ck_assert_uint_le(time_stop_sched-time_start_sched, mt_online_get_sched_period());
        }
        else if(trace_dyn_buf[i].trid == TRACE_BEGIN_ACTIVATE_TASK)
            ++cnt_start_task;
        else if(trace_dyn_buf[i].trid == TRACE_END_FINALIZE_TASK)
            ++cnt_end_task;
        else if(trace_dyn_buf[i].trid == TRACE_BEGIN_PREEMPTION)
            ++cnt_start_preempt;
        else if(trace_dyn_buf[i].trid == TRACE_END_PREEMPTION)
            ++cnt_end_preempt;
        else if(trace_dyn_buf[i].trid == TRACE_SEND_PREEMPTION_SIGNAL)
            ++cnt_req_preempt;
        else
            cnt_other = 0;
    }
    
    mt_stdout_2_devnull();
    yas_cleanup(); // free trace_dyn_buf
    mt_stdout_2_console();
    
    printf("Scheduling drift %lu - %f\n", drift_acc, drift_acc / (float)mt_online_get_sched_period());
    
    ck_assert_int_eq(cnt_begin_sched, cnt_end_sched);
    num_sched_act = exec_time / (float)mt_online_get_sched_period();
    // num_sched_act-1 <= cnt_begin_sched <= num_sched_act+1 -> due to clock drift and precision, we allow +/- 1 activation
    ck_assert_float_ge(cnt_begin_sched, num_sched_act-1);
    ck_assert_float_le(cnt_begin_sched, num_sched_act+1);
    
    ck_assert_int_eq(cnt_start_preempt, cnt_end_preempt);
    ck_assert_int_eq(cnt_req_preempt, cnt_start_preempt);
    
    ck_assert_int_eq(cnt_start_task, cnt_end_task);
    num_task_act = exec_time / (float)period_a;
    ck_assert(num_task_act >= cnt_start_task-1 && num_task_act <= cnt_start_task+1);
    
} END_TEST


yas_time_t period_short = MS2NS(250);
yas_time_t period_long = MS2NS(750);
void task_short(void* data) {
    printf("Start SHORT\n");
    yas_utils_nanosleep(MS2NS(25));
    printf("Stop SHORT\n");
}

void task_long(void* data) {
    printf("Start LONG\n");
    yas_utils_nanosleep(MS2NS(500));
    printf("Stop LONG\n");
}

START_TEST (twoTasksWithPreemption) {
    yas_task_data_t taskS, taskL;
    yas_task_id_t sid, lid;
    int i;
    int cnt_begin_sched = 0, cnt_end_sched = 0, 
        cnt_start_task_s = 0, cnt_end_task_s = 0,
        cnt_start_task_l = 0, cnt_end_task_l = 0,
        cnt_other = 0;
    float num_sched_act, num_task_act_s, num_task_act_l;
    yas_time_t time_start_sched = 0, time_stop_sched = 0, drift_acc = 0;
    
    strncpy(taskS.name, "short", 256);
    taskS.period = period_short;
    
    strncpy(taskL.name, "long", 256);
    taskL.period = period_long;
    
    yas_init();
    sid = yas_task_decl(&taskS,task_short, NULL);
    lid = yas_task_decl(&taskL,task_long, NULL);
    
    ck_assert_int_eq(mt_get_recurring_task_size(), 2);
    ck_assert_int_eq(mt_get_recurring_tasks()[0]._internal_id, sid);
    ck_assert_int_eq(mt_get_recurring_tasks()[1]._internal_id, lid);

    mt_stdout_2_devnull();
    ck_assert(yas_start());
    ck_assert(mt_online_get_sched_period() == period_short);
    
    ck_assert_int_eq(mt_get_recurring_tasks()[0]._internal_id, sid);
    ck_assert_int_eq(mt_get_recurring_tasks()[1]._internal_id, lid);
    
    yas_utils_nanosleep(exec_time);
    
    yas_stop();
    while(!mt_are_ready_queues_empty()) yas_utils_nanosleep(1000);
    y_sync_atomic_fence();
    mt_stdout_2_console();
    
    for(i=0 ; i < trace_dyn_index ; ++i) {
        if(trace_dyn_buf[i].trid == TRACE_BEGIN_SCHEDULING) {
            ++cnt_begin_sched;
            if(time_start_sched > 0)
                drift_acc += (trace_dyn_buf[i].time-time_start_sched) % mt_online_get_sched_period();
            time_start_sched = trace_dyn_buf[i].time;
        }
        else if(trace_dyn_buf[i].trid == TRACE_END_SCHEDULING) {
            ++cnt_end_sched;
            time_stop_sched = trace_dyn_buf[i].time;
            ck_assert_uint_le(time_stop_sched-time_start_sched, mt_online_get_sched_period());
        }
        else if(trace_dyn_buf[i].trid == TRACE_BEGIN_ACTIVATE_TASK && trace_dyn_buf[i].elid == sid)
            ++cnt_start_task_s;
        else if(trace_dyn_buf[i].trid == TRACE_END_FINALIZE_TASK && trace_dyn_buf[i].elid == sid)
            ++cnt_end_task_s;
        else if(trace_dyn_buf[i].trid == TRACE_BEGIN_ACTIVATE_TASK && trace_dyn_buf[i].elid == lid)
            ++cnt_start_task_l;
        else if(trace_dyn_buf[i].trid == TRACE_END_FINALIZE_TASK && trace_dyn_buf[i].elid == lid)
            ++cnt_end_task_l;
        else
            cnt_other = 0;
    }
    
    mt_stdout_2_devnull();
    yas_cleanup(); // free trace_dyn_buf
    mt_stdout_2_console();
    
    printf("Scheduling drift %lu - %f\n", drift_acc, drift_acc / (float)mt_online_get_sched_period());
    
    ck_assert_int_eq(cnt_begin_sched, cnt_end_sched);
    num_sched_act = exec_time / (float)mt_online_get_sched_period();
    
    // cnt_begin_sched-1 <= num_sched_act <= cnt_begin_sched+1 -> due to clock drift and precision, we allow +/- 1 activation
    ck_assert_float_ge(num_sched_act, cnt_begin_sched-1);
    ck_assert_float_le(num_sched_act, cnt_begin_sched+1);
    
    ck_assert_int_eq(cnt_start_task_s, cnt_end_task_s);
    num_task_act_s = exec_time / (float)period_short;
    ck_assert_float_ge(num_task_act_s, cnt_start_task_s-1);
    ck_assert_float_le(num_task_act_s, cnt_start_task_s+1);
    
    ck_assert_int_eq(cnt_start_task_l, cnt_end_task_l);
    num_task_act_l = exec_time / (float)period_long;
    ck_assert_int_ge(num_task_act_l, cnt_start_task_l-1);
    ck_assert_int_le(num_task_act_l, cnt_start_task_l+1);
    
} END_TEST

yas_time_t period_middle = MS2NS(480);
void task_middle(void* data) {
    printf("Start MIDDLE\n");
    yas_utils_nanosleep(MS2NS(300));
    printf("Stop MIDDLE\n");
}

START_TEST(threeTasksWithPreemption) {
    yas_task_data_t taskS, taskM, taskL;
    yas_task_id_t sid, mid, lid;
    int i;
    int cnt_begin_sched = 0, cnt_end_sched = 0, 
        cnt_start_task_s = 0, cnt_end_task_s = 0,
        cnt_start_task_l = 0, cnt_end_task_l = 0,
        cnt_start_task_m = 0, cnt_end_task_m = 0,
        cnt_other = 0,
        missed_sched_dueto_drift;
    float num_sched_act, num_task_act_s, num_task_act_l, num_task_act_m;
    yas_time_t time_start_sched = 0, time_stop_sched = 0, drift_acc = 0;
    
    strncpy(taskS.name, "short", 256);
    taskS.period = period_short;
    
    strncpy(taskL.name, "long", 256);
    taskL.period = period_long;
    
    strncpy(taskM.name, "middle", 256);
    taskM.period = period_middle;
    
    yas_init();
    sid = yas_task_decl(&taskS,task_short, NULL);
    lid = yas_task_decl(&taskL,task_long, NULL);
    mid = yas_task_decl(&taskM,task_middle, NULL);
    
    ck_assert_int_eq(mt_get_recurring_task_size(), 3);
    ck_assert_int_eq(mt_get_recurring_tasks()[0]._internal_id, sid);
    ck_assert_int_eq(mt_get_recurring_tasks()[1]._internal_id, lid);
    ck_assert_int_eq(mt_get_recurring_tasks()[2]._internal_id, mid);

    mt_stdout_2_devnull();
    ck_assert(yas_start());
//    ck_assert(mt_online_get_sched_freq() == period_short);
    
    ck_assert_int_eq(mt_get_recurring_tasks()[0]._internal_id, sid);
    ck_assert_int_eq(mt_get_recurring_tasks()[1]._internal_id, mid);
    ck_assert_int_eq(mt_get_recurring_tasks()[2]._internal_id, lid);
    
    yas_utils_nanosleep(exec_time);
    
    yas_stop();
    while(!mt_are_ready_queues_empty()) yas_utils_nanosleep(1000);
    y_sync_atomic_fence();
    mt_stdout_2_console();
    
    for(i=0 ; i < trace_dyn_index ; ++i) {
        if(trace_dyn_buf[i].trid == TRACE_BEGIN_SCHEDULING) {
            ++cnt_begin_sched;
            if(time_start_sched > 0)
                drift_acc += (trace_dyn_buf[i].time-time_start_sched) % mt_online_get_sched_period();
            time_start_sched = trace_dyn_buf[i].time;
        }
        else if(trace_dyn_buf[i].trid == TRACE_END_SCHEDULING) {
            ++cnt_end_sched;
            time_stop_sched = trace_dyn_buf[i].time;
            ck_assert_uint_le(time_stop_sched-time_start_sched, mt_online_get_sched_period());
        }
        else if(trace_dyn_buf[i].trid == TRACE_BEGIN_ACTIVATE_TASK && trace_dyn_buf[i].elid == sid)
            ++cnt_start_task_s;
        else if(trace_dyn_buf[i].trid == TRACE_END_FINALIZE_TASK && trace_dyn_buf[i].elid == sid)
            ++cnt_end_task_s;
        else if(trace_dyn_buf[i].trid == TRACE_BEGIN_ACTIVATE_TASK && trace_dyn_buf[i].elid == mid)
            ++cnt_start_task_m;
        else if(trace_dyn_buf[i].trid == TRACE_END_FINALIZE_TASK && trace_dyn_buf[i].elid == mid)
            ++cnt_end_task_m;
        else if(trace_dyn_buf[i].trid == TRACE_BEGIN_ACTIVATE_TASK && trace_dyn_buf[i].elid == lid)
            ++cnt_start_task_l;
        else if(trace_dyn_buf[i].trid == TRACE_END_FINALIZE_TASK && trace_dyn_buf[i].elid == lid)
            ++cnt_end_task_l;
        else
            cnt_other = 0;
    }
    
    mt_stdout_2_devnull();
    yas_cleanup(); // free trace_dyn_buf
    mt_stdout_2_console();
    
    missed_sched_dueto_drift = (int)(drift_acc / mt_online_get_sched_period());
    printf("Scheduling drift %lu ns -- missed sched activation %f\n", drift_acc, drift_acc / (float)mt_online_get_sched_period());
    
    
    ck_assert_int_eq(cnt_begin_sched, cnt_end_sched);
    
    num_sched_act = exec_time / (float)mt_online_get_sched_period();
    
    // num_sched_act-1 <= cnt_begin_sched <= num_sched_act+1 -> due to clock drift and precision, we allow +/- 1 activation
//    ck_assert_float_ge(cnt_begin_sched+missed_sched_dueto_drift, num_sched_act-1);
//    ck_assert_float_le(cnt_begin_sched+missed_sched_dueto_drift, num_sched_act+1);
    
    ck_assert_int_eq(cnt_start_task_s, cnt_end_task_s);
    num_task_act_s = exec_time / (float)period_short;
    ck_assert_float_ge(num_task_act_s, cnt_start_task_s-1);
    ck_assert_float_le(num_task_act_s, cnt_start_task_s+1);
    
    ck_assert_int_eq(cnt_start_task_m, cnt_end_task_m);
    num_task_act_m = exec_time / (float)period_middle;
    ck_assert_float_ge(num_task_act_m, cnt_start_task_m-1);
    ck_assert_float_le(num_task_act_m, cnt_start_task_m+1);
    
    ck_assert_int_eq(cnt_start_task_l, cnt_end_task_l);
    num_task_act_l = exec_time / (float)period_long;
    ck_assert_int_ge(num_task_act_l, cnt_start_task_l-1);
    ck_assert_int_le(num_task_act_l, cnt_start_task_l+1);
    
} END_TEST

Suite *test_suite() {
    Suite *s;
    TCase *tc_core;
    s = suite_create("Single Core RM Implicit Preempt");
    tc_core = tcase_create("Core");

    tcase_add_test(tc_core, onePeriodicTask);
    tcase_add_test(tc_core, twoTasksWithPreemption);
    tcase_add_test(tc_core, threeTasksWithPreemption);
    
    suite_add_tcase(s, tc_core);
    return s;
}

int main(void) {
    int number_failed;
    Suite *s;
    SRunner *sr;

    s = test_suite();
    sr = srunner_create(s);
    
    srunner_set_fork_status(sr, CK_NOFORK);

    srunner_run_all(sr, CK_NORMAL);
    number_failed = srunner_ntests_failed(sr);
    srunner_free(sr);
    return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}

