/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include <check.h>
#include "yasmin_api.h"
#include "trace.h"
#include "../blockcalls.h"
#include "../mode_test_decls.h"

#define MS2NS(X) (X*1000000)
#define S2NS(X)  (X*1000000000l)

yas_time_t exec_time = S2NS(5);

void task_short(void* data) {
//    printf("Start SHORT\n");
    yas_utils_nanosleep(MS2NS(25));
//    printf("Stop SHORT\n");
}

void task_long(void* data) {
//    printf("Start LONG\n");
    yas_utils_nanosleep(MS2NS(200));
//    printf("Stop LONG\n");
}

void task_middle(void* data) {
//    printf("Start MIDDLE\n");
    yas_utils_nanosleep(MS2NS(200));
//    printf("Stop MIDDLE\n");
}

START_TEST(checkDrift) {
    yas_task_data_t taskS, taskM, taskL, taskO;
    yas_task_id_t sid, mid, lid, last_id;
    int i;
    int cnt_begin_sched = 0, cnt_end_sched = 0, 
        cnt_start_task_s = 0, cnt_end_task_s = 0,
        cnt_start_task_l = 0, cnt_end_task_l = 0,
        cnt_start_task_m = 0, cnt_end_task_m = 0,
        cnt_start_task_o = 0, cnt_end_task_o = 0,
        missed_sched_dueto_drift;
    int exp_act = exec_time / MS2NS(500);
    
    strncpy(taskS.name, "short\0", 6);
    taskS.offset = 1;
    taskS.virt_core_id = 0;
    
    strncpy(taskL.name, "long\0", 5);
    taskL.offset = MS2NS(50);
    taskL.virt_core_id = 0;
    
    strncpy(taskM.name, "middle\0", 7);
    taskM.offset = MS2NS(300);
    taskM.virt_core_id = 0;
    
    yas_init();
    sid = yas_task_decl(&taskS,task_short, NULL);
    lid = yas_task_decl(&taskL,task_long, NULL);
    mid = yas_task_decl(&taskM,task_middle, NULL);
    
    ck_assert(yas_start());
    
//    mt_stdout_2_devnull();
    yas_utils_nanosleep(exec_time);
    
    yas_stop();
    while(!mt_are_ready_queues_empty()) yas_utils_nanosleep(1000); 
    y_sync_atomic_fence();
//    mt_stdout_2_console();
    
    last_id = -1;
    for(i=0 ; i < trace_dyn_index ; ++i) {
        if(trace_dyn_buf[i].elid == 42) {
            continue;
        }
        if(trace_dyn_buf[i].trid == TRACE_BEGIN_ACTIVATE_TASK && trace_dyn_buf[i].elid == sid) {
            ++cnt_start_task_s;
            ck_assert_int_eq(last_id, 3); //start iteration is 3
        }
        else if(trace_dyn_buf[i].trid == TRACE_END_FINALIZE_TASK && trace_dyn_buf[i].elid == sid) {
            ++cnt_end_task_s;
            ck_assert_int_eq(last_id, sid);
        }
        else if(trace_dyn_buf[i].trid == TRACE_BEGIN_ACTIVATE_TASK && trace_dyn_buf[i].elid == mid) {
            ++cnt_start_task_m;
            ck_assert_int_eq(last_id, lid);
        }
        else if(trace_dyn_buf[i].trid == TRACE_END_FINALIZE_TASK && trace_dyn_buf[i].elid == mid) {
            ++cnt_end_task_m;
            ck_assert_int_eq(last_id, mid);
        }
        else if(trace_dyn_buf[i].trid == TRACE_BEGIN_ACTIVATE_TASK && trace_dyn_buf[i].elid == lid) {
            ++cnt_start_task_l;;
            ck_assert_int_eq(last_id, sid);
        }
        else if(trace_dyn_buf[i].trid == TRACE_END_FINALIZE_TASK && trace_dyn_buf[i].elid == lid) {
            ++cnt_end_task_l;
            ck_assert_int_eq(last_id, lid);
        }
        
        if(trace_dyn_buf[i].trid == TRACE_BEGIN_ACTIVATE_TASK)
            last_id = trace_dyn_buf[i].elid;
    }
    
    mt_stdout_2_devnull();
    yas_cleanup(); // free trace_dyn_buf
    mt_stdout_2_console();
    
    ck_assert_int_eq(cnt_start_task_s, cnt_end_task_s);
    ck_assert_int_eq(cnt_start_task_m, cnt_end_task_m);
    ck_assert_int_eq(cnt_start_task_l, cnt_end_task_l);
    
    
    ck_assert_int_ge(cnt_start_task_s, exp_act-1);
    ck_assert_int_le(cnt_start_task_s, exp_act+1);
    ck_assert_int_ge(cnt_start_task_l, exp_act-1);
    ck_assert_int_le(cnt_start_task_l, exp_act+1);
    ck_assert_int_ge(cnt_start_task_m, exp_act-1);
    ck_assert_int_le(cnt_start_task_m, exp_act+1);
    
} END_TEST

Suite *test_suite() {
    Suite *s;
    TCase *tc_core;
    s = suite_create("Offline Scheduler");
    tc_core = tcase_create("Core");

    tcase_add_test(tc_core, checkDrift);
    
    suite_add_tcase(s, tc_core);
    return s;
}

int main(void) {
    int number_failed;
    Suite *s;
    SRunner *sr;

    s = test_suite();
    sr = srunner_create(s);
    
    srunner_set_fork_status(sr, CK_NOFORK);

    srunner_run_all(sr, CK_NORMAL);
    number_failed = srunner_ntests_failed(sr);
    srunner_free(sr);
    return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}

