/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include <stdlib.h>
#include <check.h>
#include "yasmin_api.h"
#include "trace.h"
#include "../blockcalls.h"
#include "../mode_test_decls.h"

#define MS2NS(X) (X*1000000)
#define S2NS(X)  (X*1000000000l)

yas_time_t exec_time = S2NS(5);

int glob_a;
int limit_a = 1000000;
int limit_b = 10000;
yas_time_t period_a = MS2NS(500);
yas_time_t period_b = MS2NS(250);


void ftask(void *arg) {
    int limit = *((int*)arg);
    int i;
    glob_a = 0;
    for(i=0 ; i < limit ; ++i)
        glob_a += glob_a * i;
}

START_TEST (onePeriodicTask) {
    yas_task_data_t taskA;
    yas_task_id_t aid;
    int i;
    int cnt_begin_sched = 0, cnt_end_sched = 0, 
        cnt_start_task = 0, cnt_end_task = 0, 
        cnt_push_task = 0;
    float num_sched_act, num_task_act;
    yas_time_t last_sched = 0;
    yas_time_t time_start_sched = 0, time_stop_sched = 0, drift_acc = 0;
    
    strncpy(taskA.name, "TA", 256);
    taskA.period = period_a;
    
    yas_init();
    aid = yas_task_decl(&taskA, ftask, (void*)&limit_a);

    ck_assert(!mt_online_is_running());
    ck_assert_int_eq(mt_get_recurring_task_size(), 1);
    ck_assert(yas_start());
    ck_assert(mt_online_is_running());
    ck_assert(mt_online_get_sched_period() == period_a);
    
    yas_utils_nanosleep(exec_time);
    
    yas_stop();
    ck_assert(!mt_online_is_running());
    
    while(!mt_are_ready_queues_empty()) yas_utils_nanosleep(1000);
    
    for(i=0 ; i < trace_dyn_index ; ++i) {
        if(trace_dyn_buf[i].trid == TRACE_BEGIN_SCHEDULING) {
            ++cnt_begin_sched;
            if(last_sched != 0) {
                //mt_online_get_sched_freq()-1%  <= trace_dyn_buf[i].time-last_sched <= mt_online_get_sched_freq()+1%
                ck_assert_int_ge(trace_dyn_buf[i].time-last_sched, mt_online_get_sched_period()*0.99);
                ck_assert_int_le(trace_dyn_buf[i].time-last_sched, mt_online_get_sched_period()*1.01);
            }
            last_sched = trace_dyn_buf[i].time;
            if(time_start_sched > 0)
                drift_acc += (trace_dyn_buf[i].time-time_start_sched) % mt_online_get_sched_period();
            time_start_sched = trace_dyn_buf[i].time;
        }
        else if(trace_dyn_buf[i].trid == TRACE_END_SCHEDULING) {
            ++cnt_end_sched;
            time_stop_sched = trace_dyn_buf[i].time;
            ck_assert_uint_le(time_stop_sched-time_start_sched, mt_online_get_sched_period());
        }
        else if(trace_dyn_buf[i].trid == TRACE_BEGIN_ACTIVATE_TASK)
            ++cnt_start_task;
        else if(trace_dyn_buf[i].trid == TRACE_END_FINALIZE_TASK) {
            ++cnt_end_task;
        }
        else {
        }
    }
    
    mt_stdout_2_devnull();
    yas_cleanup(); // free trace_dyn_buf
    mt_stdout_2_console();
    
    printf("Scheduling drift %lu - %f\n", drift_acc, drift_acc / (float)mt_online_get_sched_period());
    
    ck_assert_int_eq(cnt_begin_sched, cnt_end_sched);
    num_sched_act = exec_time / (float)mt_online_get_sched_period();
    // num_sched_act-1 <= cnt_begin_sched <= num_sched_act+1 -> due to clock drift and precision, we allow +/- 1 activation
    ck_assert_float_ge(cnt_begin_sched, num_sched_act-1);
    ck_assert_float_le(cnt_begin_sched, num_sched_act+1);
    
    ck_assert_int_eq(cnt_start_task, cnt_end_task);
    num_task_act = exec_time / (float)period_a;
    ck_assert(num_task_act >= cnt_start_task-1 && num_task_act <= cnt_start_task+1);
    
} END_TEST

START_TEST (twoPeriodicTasks) {
    yas_task_data_t taskA, taskB;
    yas_task_id_t aid, bid;
    int i;
    int cnt_begin_sched = 0, cnt_end_sched = 0, 
        cnt_start_task_a = 0, cnt_end_task_a = 0,
        cnt_start_task_b = 0, cnt_end_task_b = 0,
        cnt_other = 0;
    float num_sched_act, num_task_act_a, num_task_act_b;
    yas_time_t last_start_a = 0, last_end_a = 0, last_start_b = 0, last_end_b = 0;
    yas_time_t time_start_sched = 0, time_stop_sched = 0, drift_acc = 0;
    
    strncpy(taskA.name, "TA", 256);
    taskA.period = period_a;
    
    strncpy(taskB.name, "TB", 256);
    taskB.period = period_b;
    
    yas_init();
    aid = yas_task_decl(&taskA, ftask, (void*)&limit_a);
    bid = yas_task_decl(&taskB, ftask, (void*)&limit_b);
    
    ck_assert_int_eq(aid, 0);
    ck_assert_int_eq(bid, 1);
    ck_assert_int_eq(mt_get_recurring_tasks()[0]._internal_id, aid);
    ck_assert_int_eq(mt_get_recurring_tasks()[1]._internal_id, bid);

    ck_assert_int_eq(mt_get_recurring_task_size(), 2);
    ck_assert(yas_start());
    ck_assert(mt_online_get_sched_period() == period_b);
    
    ck_assert_int_eq(mt_get_recurring_tasks()[0]._internal_id, bid);
    ck_assert_int_eq(mt_get_recurring_tasks()[1]._internal_id, aid);// b has a higher priority, so with RM assignment it must have been moved
    
    yas_utils_nanosleep(exec_time);
    
    yas_stop();
    ck_assert(!mt_online_is_running());
    while(!mt_are_ready_queues_empty()) yas_utils_nanosleep(1000);
    
    for(i=0 ; i < trace_dyn_index ; ++i) {
        if(trace_dyn_buf[i].trid == TRACE_BEGIN_SCHEDULING) {
            ++cnt_begin_sched;
            if(time_start_sched > 0)
                drift_acc += (trace_dyn_buf[i].time-time_start_sched) % mt_online_get_sched_period();
            time_start_sched = trace_dyn_buf[i].time;
        }
        else if(trace_dyn_buf[i].trid == TRACE_END_SCHEDULING) {
            ++cnt_end_sched;
            time_stop_sched = trace_dyn_buf[i].time;
            ck_assert_uint_le(time_stop_sched-time_start_sched, mt_online_get_sched_period());
        }
        else if(trace_dyn_buf[i].trid == TRACE_BEGIN_ACTIVATE_TASK && trace_dyn_buf[i].elid == aid) {
            ++cnt_start_task_a;
            
            last_start_a = trace_dyn_buf[i].time;
            ck_assert_uint_lt(last_start_b, last_end_b);
            ck_assert_uint_lt(last_end_b, last_start_a); //new job of a must be after b
        }
        else if(trace_dyn_buf[i].trid == TRACE_END_FINALIZE_TASK && trace_dyn_buf[i].elid == aid) {
            ++cnt_end_task_a;
            ck_assert_int_eq(cnt_start_task_a, cnt_end_task_a);
            
            last_end_a = trace_dyn_buf[i].time;
            ck_assert_uint_lt(last_start_a, last_end_a);
        }
        else if(trace_dyn_buf[i].trid == TRACE_BEGIN_ACTIVATE_TASK && trace_dyn_buf[i].elid == bid) {
            ++cnt_start_task_b;
            
            last_start_b = trace_dyn_buf[i].time;
            if(last_start_a > 0 || last_end_a > 0)
                ck_assert_uint_lt(last_start_a, last_end_a);//make sure that last job of a is over
            ck_assert_uint_lt(last_end_a, last_start_b); //new job of b must be after a
        }
        else if(trace_dyn_buf[i].trid == TRACE_END_FINALIZE_TASK && trace_dyn_buf[i].elid == bid) {
            ++cnt_end_task_b;
            ck_assert_int_eq(cnt_start_task_b, cnt_end_task_b);
            
            last_end_b = trace_dyn_buf[i].time;
            ck_assert_uint_lt(last_start_b, last_end_b);
        }
        else {
            cnt_other = 0;
            ck_assert_int_ne(trace_dyn_buf[i].trid, TRACE_BEGIN_PREEMPTION);
        }
    }
    
    mt_stdout_2_devnull();
    yas_cleanup(); // free trace_dyn_buf
    mt_stdout_2_console();
    
    printf("Scheduling drift %lu - %f\n", drift_acc, drift_acc / (float)mt_online_get_sched_period());
    
    ck_assert_int_eq(cnt_begin_sched, cnt_end_sched);
//    num_sched_act = exec_time / (float)mt_online_get_sched_freq();
    
    // num_sched_act-1 <= cnt_begin_sched <= num_sched_act+1 -> due to clock drift and precision, we allow +/- 1 activation
//    ck_assert_float_ge(cnt_begin_sched, num_sched_act-1);
//    ck_assert_float_le(cnt_begin_sched, num_sched_act+1);
    
    ck_assert_int_eq(cnt_start_task_a, cnt_end_task_a);
    num_task_act_a = exec_time / (float)period_a;
    ck_assert_float_ge(num_task_act_a, cnt_start_task_a-1);
    ck_assert_float_le(num_task_act_a, cnt_start_task_a+1);
    
    ck_assert_int_eq(cnt_start_task_b, cnt_end_task_b);
    num_task_act_b = exec_time / (float)period_b;
    ck_assert_int_ge(num_task_act_b, cnt_start_task_b-1);
    ck_assert_int_le(num_task_act_b, cnt_start_task_b+1);
    
} END_TEST

Suite *test_suite() {
    Suite *s;
    TCase *tc_core;
    s = suite_create("Single Core RM Implicit NoPreempt");
    tc_core = tcase_create("Core");

    tcase_add_test(tc_core, onePeriodicTask);
    tcase_add_test(tc_core, twoPeriodicTasks);
    
    suite_add_tcase(s, tc_core);
    return s;
}

int main(void) {
    int number_failed;
    Suite *s;
    SRunner *sr;

    s = test_suite();
    sr = srunner_create(s);
    
    srunner_set_fork_status(sr, CK_NOFORK);

    srunner_run_all(sr, CK_NORMAL);
    number_failed = srunner_ntests_failed(sr);
    srunner_free(sr);
    return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
