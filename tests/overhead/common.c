#include "common.h"
#include "../blockcalls.h"

#define MS2NS(X) (X*1000000)
#define S2NS(X)  (X*1000000000l)

yas_time_t exec_time = S2NS(10);

yas_time_t period_short = MS2NS(250);
void task_short(void* data) {
//    printf("Start SHORT\n");
    yas_utils_nanosleep(MS2NS(25));
//    printf("Stop SHORT\n");
}

yas_time_t period_long = MS2NS(750);
void task_long(void* data) {
//    printf("Start LONG\n");
    yas_utils_nanosleep(MS2NS(200));
//    printf("Stop LONG\n");
}

yas_time_t period_middle = MS2NS(480);
void task_middle(void* data) {
//    printf("Start MIDDLE\n");
    yas_utils_nanosleep(MS2NS(200));
//    printf("Stop MIDDLE\n");
}

yas_time_t period_another = MS2NS(512);
void task_other(void* data) {
//    printf("Start OTHER\n");
    yas_utils_nanosleep(MS2NS(100));
//    printf("Stop OTHER\n");
}

START_TEST(checkDrift) {
    yas_task_data_t taskS, taskM, taskL, taskO;
    yas_task_id_t sid, mid, lid, oid;
    int i;
    int cnt_begin_sched = 0, cnt_end_sched = 0, 
        missed_sched_dueto_drift;
    yas_time_t time_start_sched = 0, time_stop_sched = 0, drift_acc = 0, drift;
    
    yas_build_task_name(taskS.name, "short");
    taskS.period = period_short;
    
    yas_build_task_name(taskL.name, "long");
    taskL.period = period_long;
    
    yas_build_task_name(taskM.name, "middle");
    taskM.period = period_middle;
    
    yas_build_task_name(taskO.name, "other");
    taskO.period = period_another;
    
    yas_init();
    sid = yas_task_decl(&taskS,task_short, NULL);
    lid = yas_task_decl(&taskL,task_long, NULL);
    mid = yas_task_decl(&taskM,task_middle, NULL);
    oid = yas_task_decl(&taskO,task_other, NULL);
    
    ck_assert(yas_start());
    ck_assert_int_eq(mt_online_get_sched_period(), MS2NS(2));
    mt_stdout_2_devnull();
    
    yas_utils_nanosleep(exec_time);
    
    yas_stop();
    while(!mt_are_ready_queues_empty()) yas_utils_nanosleep(1000); 
    y_sync_atomic_fence();
    mt_stdout_2_console();
    
    printf("Scheduler frequency: %llu ns\n", mt_online_get_sched_period());
    for(i=0 ; i < trace_dyn_index ; ++i) {
        if(trace_dyn_buf[i].trid == TRACE_BEGIN_SCHEDULING) {
            ++cnt_begin_sched;
            if(time_start_sched > 0) {
                drift_acc += drift = (trace_dyn_buf[i].time-time_start_sched) % mt_online_get_sched_period();
//                printf("Scheduler drift %llu ns\n", drift);
            }
            time_start_sched = trace_dyn_buf[i].time;
        }
        else if(trace_dyn_buf[i].trid == TRACE_END_SCHEDULING) {
            ++cnt_end_sched;
            time_stop_sched = trace_dyn_buf[i].time;
            ck_assert_uint_le(time_stop_sched-time_start_sched, mt_online_get_sched_period());
        }
    }
    
    mt_stdout_2_devnull();
    yas_cleanup(); // free trace_dyn_buf
    mt_stdout_2_console();
    
    ck_assert_int_eq(cnt_begin_sched, cnt_end_sched);
    
    missed_sched_dueto_drift = (int)(drift_acc / mt_online_get_sched_period());
    printf("Total scheduling drift %llu ns -- potential missed sched activation %i\n", drift_acc, missed_sched_dueto_drift);
} END_TEST

void print_overhead(char tname[256], yas_task_id_t id, unsigned int size) {
    int i, oh_act_s_i=0, oh_fin_s_i=0;
    yas_time_t s_act_s, e_act_s, s_fin_s, e_fin_s, min_oh_s, max_oh_s, tmp_oh_s_acc;
    yas_time_t *overhead_s;
    
    overhead_s = (yas_time_t*)malloc(sizeof(yas_time_t)*size);
    for(i=0 ; i < trace_dyn_index ; ++i) {
        if(trace_dyn_buf[i].elid == id && trace_dyn_buf[i].trid == TRACE_BEGIN_ACTIVATE_TASK) {
            s_act_s = trace_dyn_buf[i].time;
        }
        else if(trace_dyn_buf[i].elid == id && trace_dyn_buf[i].trid == TRACE_END_ACTIVATE_TASK) {
            e_act_s = trace_dyn_buf[i].time;
            overhead_s[oh_act_s_i++] = e_act_s - s_act_s;
        }
        else if(trace_dyn_buf[i].elid == id && trace_dyn_buf[i].trid == TRACE_BEGIN_FINALIZE_TASK) {
            s_fin_s = trace_dyn_buf[i].time;
        }
        else if(trace_dyn_buf[i].elid == id && trace_dyn_buf[i].trid == TRACE_END_FINALIZE_TASK) {
            e_fin_s = trace_dyn_buf[i].time;
            overhead_s[oh_fin_s_i++] += e_fin_s - s_fin_s;
        }
    }
    
    tmp_oh_s_acc = 0;
    min_oh_s = -1;
    max_oh_s = 0;
    for(i = 0 ; i < oh_fin_s_i ; ++i) {
        if(overhead_s[i] < min_oh_s)
            min_oh_s = overhead_s[i];
        if(overhead_s[i] > max_oh_s)
            max_oh_s = overhead_s[i];
        tmp_oh_s_acc += overhead_s[i];
    }
    printf(">>> Overhead task \"%s\": min %llu ns - max %llu ns - avg %f ns\n", tname, min_oh_s, max_oh_s, tmp_oh_s_acc/(double)oh_fin_s_i);
}

START_TEST(TaskOverhead) {
    yas_task_data_t taskS, taskM, taskL, taskO;
    yas_task_id_t sid, mid, lid, oid;
    unsigned int oh_s_size, oh_l_size, oh_m_size, oh_o_size;
    
    int i, oh_s_i=0, oh_g_i=0;
    yas_time_t s_sched, e_sched, s_get, e_get, min_oh_s, max_oh_s, tmp_oh_s_acc, min_oh_g, max_oh_g, tmp_oh_g_acc;
    yas_time_t *overhead_sched, *overhead_get;
    unsigned int size_oh = (exec_time / mt_online_get_sched_period());
    size_t unused_trace = 0;
    
    yas_build_task_name(taskS.name, "short");
    taskS.period = period_short;
    oh_s_size = (exec_time/period_short)+1;
    
    
    yas_build_task_name(taskL.name, "long");
    taskL.period = period_long;
    oh_l_size = (exec_time/period_long)+1;
    
    yas_build_task_name(taskM.name, "middle");
    taskM.period = period_middle;
    oh_m_size = (exec_time/period_middle)+1;
    
    yas_build_task_name(taskO.name, "other");
    taskO.period = period_another;
    oh_o_size = (exec_time/period_another)+1;
    
    yas_init();
    sid = yas_task_decl(&taskS,task_short, NULL);
    lid = yas_task_decl(&taskL,task_long, NULL);
    mid = yas_task_decl(&taskM,task_middle, NULL);
    oid = yas_task_decl(&taskO,task_other, NULL);
    
    ck_assert(yas_start());
    ck_assert_int_eq(mt_online_get_sched_period(), MS2NS(2));
    mt_stdout_2_devnull();
    
    yas_utils_nanosleep(exec_time);
    
    yas_stop();
    while(!mt_are_ready_queues_empty()) yas_utils_nanosleep(1000); 
    y_sync_atomic_fence();
    mt_stdout_2_console();
    
    print_overhead(taskS.name, sid, oh_s_size);
    print_overhead(taskL.name, lid, oh_l_size);
    print_overhead(taskM.name, mid, oh_m_size);
    print_overhead(taskO.name, oid, oh_o_size);
    
    overhead_sched = (yas_time_t*)malloc(sizeof(yas_time_t)*size_oh);
    overhead_get = (yas_time_t*)malloc(sizeof(yas_time_t)*size_oh);
    for(i=0 ; i < trace_dyn_index ; ++i) {
        if(trace_dyn_buf[i].trid == TRACE_BEGIN_SCHEDULING) {
            s_sched = trace_dyn_buf[i].time;
        }
        else if(trace_dyn_buf[i].trid == TRACE_END_SCHEDULING) {
            e_sched = trace_dyn_buf[i].time;
            overhead_sched[oh_s_i++] = e_sched - s_sched;
        }
        if(trace_dyn_buf[i].trid == TRACE_BEGIN_GET_TASK) {
            s_get = trace_dyn_buf[i].time;
        }
        else if(trace_dyn_buf[i].trid == TRACE_END_GET_TASK) {
            e_get = trace_dyn_buf[i].time;
            if(oh_g_i < size_oh)
                overhead_get[oh_g_i++] = e_get - s_get;
            else
                printf("Not enough space in overhead_get\n");
        }
    }
    
    for(i=0 ; i < trace_dyn_index ; ++i) {
        if(trace_dyn_buf[i].trid != TRACE_BEGIN_SCHEDULING && 
           trace_dyn_buf[i].trid != TRACE_END_SCHEDULING && 
           trace_dyn_buf[i].trid != TRACE_BEGIN_GET_TASK && 
           trace_dyn_buf[i].trid != TRACE_END_GET_TASK && 
           trace_dyn_buf[i].trid != TRACE_BEGIN_SCHEDULING && 
           trace_dyn_buf[i].trid != TRACE_BEGIN_ACTIVATE_TASK && 
           trace_dyn_buf[i].trid != TRACE_END_ACTIVATE_TASK && 
           trace_dyn_buf[i].trid != TRACE_BEGIN_FINALIZE_TASK && 
           trace_dyn_buf[i].trid != TRACE_END_FINALIZE_TASK) {
            ++unused_trace;
        }
    }
    
    tmp_oh_s_acc = 0;
    min_oh_s = -1;
    max_oh_s = 0;
    for(i = 0 ; i < oh_s_i ; ++i) {
        if(overhead_sched[i] < min_oh_s)
            min_oh_s = overhead_sched[i];
        if(overhead_sched[i] > max_oh_s)
            max_oh_s = overhead_sched[i];
        tmp_oh_s_acc += overhead_sched[i];
    }
    printf("Overhead scheduling: min %llu ns - max %llu ns - avg %f ns\n", min_oh_s, max_oh_s, tmp_oh_s_acc/(double)oh_s_i);
    
    tmp_oh_g_acc = 0;
    min_oh_g = -1;
    max_oh_g = 0;
    for(i = 0 ; i < oh_g_i ; ++i) {
        if(overhead_get[i] < min_oh_g)
            min_oh_g = overhead_get[i];
        if(overhead_get[i] > max_oh_g)
            max_oh_g = overhead_get[i];
        tmp_oh_g_acc += overhead_get[i];
    }
    printf("Overhead get task: min %llu ns - max %llu ns - avg %f ns\n", min_oh_g, max_oh_g, tmp_oh_g_acc/(double)oh_g_i);
    
    printf("Unused trace: %lu / %lu\n", unused_trace, trace_dyn_index);
    
    mt_stdout_2_devnull();
    yas_cleanup(); // free trace_dyn_buf
    mt_stdout_2_console();
} END_TEST

void setup_testsuite(TCase *tc_core) {
    tcase_add_test(tc_core, checkDrift);
    tcase_add_test(tc_core, TaskOverhead);
}