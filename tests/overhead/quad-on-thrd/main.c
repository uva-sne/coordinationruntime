#include "../common.h"

Suite *test_suite() {
    Suite *s;
    TCase *tc_core;
    s = suite_create("Quadcore Online Sched Thread");
    tc_core = tcase_create("Core");

    setup_testsuite(tc_core);
    
    suite_add_tcase(s, tc_core);
    return s;
}

int main(void) {
    int number_failed;
    Suite *s;
    SRunner *sr;

    s = test_suite();
    sr = srunner_create(s);
    
    srunner_set_fork_status(sr, CK_NOFORK);

    srunner_run_all(sr, CK_NORMAL);
    number_failed = srunner_ntests_failed(sr);
    srunner_free(sr);
    return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}

