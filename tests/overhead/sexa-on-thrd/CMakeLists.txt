message("-- Building test \"DualCore Online scheduler using a thread for the scheduler\"")

set(YAS_CUSTOM_LIB_NAME libruntime_sexaon_oh_thrd)
set(CMAKE_TARGET_NAME testsexaonohthrdsched)
set(YAS_RUNTIME_APP_PATH "${CMAKE_CURRENT_LIST_DIR}")

find_package(yasmin_runtime_core REQUIRED)

add_executable(${CMAKE_TARGET_NAME} ${YAS_RUNTIME_APP_PATH}/main.c ${YAS_RUNTIME_APP_PATH}/../common.c ${MT_GLOBAL_C_FILES})
target_link_libraries(${CMAKE_TARGET_NAME} PUBLIC ${YAS_CUSTOM_LIB_NAME})
target_link_libraries(${CMAKE_TARGET_NAME} PUBLIC check)
target_include_directories(${CMAKE_TARGET_NAME} PUBLIC ${YAS_RUNTIME_APP_PATH}/../)

add_custom_target(run_${CMAKE_TARGET_NAME}
    DEPENDS ${CMAKE_TARGET_NAME}
    COMMAND ${CMAKE_CURRENT_BINARY_DIR}/${CMAKE_TARGET_NAME}
)

get_property(RUN_DEP GLOBAL PROPERTY RUN_ALL_DEPENDS)
list(APPEND RUN_DEP "run_${CMAKE_TARGET_NAME}")
set_property(GLOBAL PROPERTY RUN_ALL_DEPENDS ${RUN_DEP})