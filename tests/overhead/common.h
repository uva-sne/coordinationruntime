/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   common.h
 * Author: brouxel
 *
 * Created on 19 mars 2021, 14:09
 */

#ifndef COMMON_H
#define COMMON_H

#ifdef __cplusplus
extern "C" {
#endif
    
#include <stdlib.h>
#include <check.h>
#include "yasmin_api.h"
#include "trace.h"
#include "../mode_test_decls.h"

void setup_testsuite(TCase *tc_core);


#ifdef __cplusplus
}
#endif

#endif /* COMMON_H */

