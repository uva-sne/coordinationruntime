/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   config.h
 * Author: brouxel
 *
 * Created on 7 décembre 2020, 09:32
 */

#ifndef CONFIG_H
#define CONFIG_H

#include <time.h>
#include <pthread.h>

#include "constants.h"

#define YAS_PERIODIC_TASK_SIZE 5
#define YAS_TASK_POOL_JOBS_SIZE 4

#define YAS_MGNT_SCHEDULER_TASK YAS_SCHED_THR
#define YAS_TASK_MAPPING YAS_MAPPING_GLOBAL_SCHEME
#define YAS_PRIORITY_ASSIGNMENT YAS_TASK_PRIORITY_EDF

#define YAS_SYNCHRO_FORCE_POSIX

#define YAS_TRACE 1

#define YAS_PREEMPTION_MODEL YAS_SCHED_NONPREEMPTIVE

#endif /* CONFIG_H */

