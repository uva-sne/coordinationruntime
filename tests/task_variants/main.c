/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include "shared.h"

START_TEST(CRUD_variants) {
    yas_task_data_t tA;
    yas_task_id_t aid;
    yas_version_id_t vid1, vid2, vid3;
    
    yas_init();
    
    strncpy(tA.name, "TA", 256);
    tA.period = period;
    aid = yas_task_decl(&tA, NULL, NULL);
    ck_assert_int_eq(mt_get_recurring_task_size(), 1);
    
    vid1 = yas_version_decl(aid, &task, NULL, 0);
    ck_assert_int_eq(mt_get_recurring_tasks()[0].versions_size, 1);
    
    vid2 = yas_version_decl(aid, &task_alt1, NULL, 0);
    ck_assert_int_eq(mt_get_recurring_tasks()[0].versions_size, 2);
    
    vid3 = yas_version_decl(aid, &task_alt2, NULL, 0);
    ck_assert_int_eq(mt_get_recurring_tasks()[0].versions_size, 3);
    
    ck_assert_int_eq(mt_get_recurring_task_size(), 1);
    
    ck_assert_ptr_eq(mt_get_recurring_tasks()[0].versions[0].func, &task);
    ck_assert_ptr_eq(mt_get_recurring_tasks()[0].versions[1].func, &task_alt1);
    ck_assert_ptr_eq(mt_get_recurring_tasks()[0].versions[2].func, &task_alt2);
    
//    ck_assert_int_eq(coord_rem_version(aid, vid2), TRUE);
//    ck_assert_int_eq(mt_get_recurring_tasks()[0].nbvariants, 2);
//    ck_assert_ptr_eq(mt_get_recurring_tasks()[0].variants[0].func, &task);
//    ck_assert_ptr_eq(mt_get_recurring_tasks()[0].variants[1].func, &task_alt2);
//    
//    ck_assert_int_eq(coord_rem_version(aid, vid2), FALSE);
//    
//    ck_assert_int_eq(coord_rem_version(aid, vid3), TRUE);
//    ck_assert_int_eq(mt_get_recurring_tasks()[0].nbvariants, 1);
//    ck_assert_ptr_eq(mt_get_recurring_tasks()[0].variants[0].func, &task);
//    
//    ck_assert_int_eq(coord_rem_version(aid, vid3), FALSE);
    
    mt_stdout_2_devnull();
    yas_cleanup(); // free trace_dyn_buf
    mt_stdout_2_console();
    
} END_TEST

START_TEST(variant_select_nothingselected) {
    yas_task_data_t tA;
    yas_task_id_t aid;
    int i;
    yas_time_t time_start_sched = 0, time_stop_sched = 0, drift_acc = 0;
    
    yas_init();
    
    strncpy(tA.name, "TA", 256);
    tA.period = period;
    aid = yas_task_decl(&tA, NULL, NULL);
    
    yas_version_decl(aid, &task, NULL, 0);
    yas_version_decl(aid, &task_alt1, NULL, 0);
    yas_version_decl(aid, &task_alt2, NULL, 0);
    
    yas_start();
    yas_utils_nanosleep(S2NS(1));
    yas_stop();
    ck_assert(!mt_online_is_running());
    
    for(i=0 ; i < trace_dyn_index ; ++i) {
        if(trace_dyn_buf[i].trid == TRACE_BEGIN_SCHEDULING) {
            if(time_start_sched > 0)
                drift_acc += (trace_dyn_buf[i].time-time_start_sched) % mt_online_get_sched_period();
            time_start_sched = trace_dyn_buf[i].time;
        }
        else if(trace_dyn_buf[i].trid == TRACE_END_SCHEDULING) {
            time_stop_sched = trace_dyn_buf[i].time;
        }
        else if(trace_dyn_buf[i].trid == TRACE_ACTIVATED_TASK_VARIANT) {
            ck_assert_ptr_eq((void*)trace_dyn_buf[i].elid, &task);
        }
    }
    
    mt_stdout_2_devnull();
    yas_cleanup(); // free trace_dyn_buf
    mt_stdout_2_console();
    
    printf("Scheduling drift %lu - %f\n", drift_acc, drift_acc / (float)mt_online_get_sched_period());
    
    yas_cleanup();
} END_TEST

Suite *test_suite() {
    Suite *s;
    TCase *tc_core;
    s = suite_create("Task Variant Base");
    tc_core = tcase_create("Core");

    tcase_add_test(tc_core, CRUD_variants);
    tcase_add_test(tc_core, variant_select_nothingselected);
    
    suite_add_tcase(s, tc_core);
    return s;
}

int main(void) {
    int number_failed;
    Suite *s;
    SRunner *sr;

    s = test_suite();
    sr = srunner_create(s);
    
    srunner_set_fork_status(sr, CK_NOFORK);

    srunner_run_all(sr, CK_NORMAL);
    number_failed = srunner_ntests_failed(sr);
    srunner_free(sr);
    return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
