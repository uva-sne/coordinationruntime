/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include "../shared.h"

START_TEST(variant_select_exec_mode) {
    yas_task_data_t tA;
    yas_task_id_t aid;
    int i;
    yas_time_t time_start_sched = 0, time_stop_sched = 0, drift_acc = 0;
    
    exec_mode = 0;
    yas_init();
    
    strncpy(tA.name, "TA", 256);
    tA.period = period;
    aid = yas_task_decl(&tA, NULL, NULL);
    
    yas_version_select_t tAv1 = {0,&get_exec_mode};
    yas_version_select_t tAv2 = {1,&get_exec_mode};
    yas_version_select_t tAv3 = {2,&get_exec_mode};
    
    yas_version_decl(aid, &task, NULL, tAv1);
    yas_version_decl(aid, &task_alt1, NULL, tAv2);
    yas_version_decl(aid, &task_alt2, NULL, tAv3);
    
    mt_stdout_2_devnull();
    yas_start();
    yas_utils_nanosleep(S2NS(1));
    yas_stop();
    mt_stdout_2_console();
    ck_assert(!mt_online_is_running());
    
    yas_task_func_t last = &task_alt2;
    for(i=0 ; i < trace_dyn_index ; ++i) {
        if(trace_dyn_buf[i].trid == TRACE_BEGIN_SCHEDULING) {
            if(time_start_sched > 0)
                drift_acc += (trace_dyn_buf[i].time-time_start_sched) % mt_online_get_sched_period();
            time_start_sched = trace_dyn_buf[i].time;
        }
        else if(trace_dyn_buf[i].trid == TRACE_END_SCHEDULING) {
            time_stop_sched = trace_dyn_buf[i].time;
        }
        else if(trace_dyn_buf[i].trid == TRACE_ACTIVATED_TASK_VARIANT) {
            if(last == &task_alt2) {
                ck_assert_ptr_eq((void*)trace_dyn_buf[i].elid, &task);
                last = &task;
            }
            else if(last == &task) {
                ck_assert_ptr_eq((void*)trace_dyn_buf[i].elid, &task_alt1);
                last = &task_alt1;
            }
            else if(last == &task_alt1) {
                ck_assert_ptr_eq((void*)trace_dyn_buf[i].elid, &task_alt2);
                last = &task_alt2;
            }
        }
    }
    
    mt_stdout_2_devnull();
    yas_cleanup(); // free trace_dyn_buf
    mt_stdout_2_console();
    
    printf("Scheduling drift %lu - %f\n", drift_acc, drift_acc / (float)mt_online_get_sched_period());
    
    yas_cleanup();
} END_TEST

Suite *test_suite() {
    Suite *s;
    TCase *tc_core;
    s = suite_create("Task Variant Exec Mode");
    tc_core = tcase_create("Core");

    tcase_add_test(tc_core, variant_select_exec_mode);
    
    suite_add_tcase(s, tc_core);
    return s;
}

int main(void) {
    int number_failed;
    Suite *s;
    SRunner *sr;

    s = test_suite();
    sr = srunner_create(s);
    
    srunner_set_fork_status(sr, CK_NOFORK);

    srunner_run_all(sr, CK_NORMAL);
    number_failed = srunner_ntests_failed(sr);
    srunner_free(sr);
    return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
