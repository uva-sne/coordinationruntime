/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   shared.h
 * Author: brouxel
 *
 * Created on 22 janvier 2021, 16:28
 */

#ifndef SHARED_H
#define SHARED_H

#ifdef __cplusplus
extern "C" {
#endif
    
#include <stdlib.h>
#include <stdio.h>
#include <check.h>
#include "../mode_test_decls.h"
#include "yasmin_api.h"
#include "../blockcalls.h"
#include "trace.h"

#define MS2NS(X) (X*1000000)
#define S2NS(X)  (X*1000000000l)

yas_time_t exec_time = S2NS(5);
static int32_t exec_mode = 0;

yas_time_t period = MS2NS(250);
void task(void* data) {
//    printf("Start t\n");
    yas_utils_nanosleep(MS2NS(25));
//    printf("Stop t\n");
    exec_mode = 1;
}

void task_alt1(void *data) {
//    printf("Start alt1\n");
    yas_utils_nanosleep(MS2NS(10));
//    printf("Stop alt1\n");
    exec_mode = 2;
}

void task_alt2(void *data) {
//    printf("Start alt2\n");
    yas_utils_nanosleep(MS2NS(100));
//    printf("Stop alt2\n");
    exec_mode = 0;
}

uint64_t get_battery_status() {
    return 100;
}

int32_t get_exec_mode() {
    return exec_mode;
}

#ifdef __cplusplus
}
#endif

#endif /* SHARED_H */

