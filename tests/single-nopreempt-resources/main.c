/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include <check.h>
#include "yasmin_api.h"
#include "trace.h"
#include "../blockcalls.h"
#include "../mode_test_decls.h"

#define US2NS(X) (X*1000)
#define MS2NS(X) (X*1000000)
#define S2NS(X)  (X*1000000000l)

yas_time_t exec_time = S2NS(5);

yas_time_t period_a = MS2NS(1);
yas_time_t period_b = US2NS(1500);

void myfork(void *d) {
    yas_utils_nanosleep(US2NS(600));
}
void join(void *d) {
    yas_utils_nanosleep(US2NS(100));
}

START_TEST(SeparateResources) {
    yas_task_id_t fid, jid;
    yas_task_data_t f, j;
    yas_hwaccel_id_t resid;
    yas_hwaccel_id_t resid2;
    int i;
    uint32_t cnt_start_task_f=0, cnt_end_task_f=0, 
            cnt_start_task_j=0, cnt_end_task_j=0,
            cnt_acq_res1 = 0, cnt_rel_res1 = 0,
            cnt_acq_res2 = 0, cnt_rel_res2 = 0;
    yas_time_t last_start_f=0, last_end_f=0, 
                last_start_j=0, last_end_j=0;
    
    yas_init();
    
    resid = yas_hwaccel_decl("gpu");
    resid2 = yas_hwaccel_decl("nvdla");
    
    strncpy(f.name, "Fork", 256);
    f.period = period_a;
    
    strncpy(j.name, "Join", 256);
    j.period = period_a;
    
    fid = yas_task_decl(&f, &myfork, NULL);
    jid = yas_task_decl(&j, &join, NULL);
    
    yas_hwaccel_use(fid, 0, resid);
    yas_hwaccel_use(jid, 0, resid2);
    
    mt_stdout_2_devnull();
    ck_assert(yas_start());
    
    yas_utils_nanosleep(exec_time);
    
    yas_stop();
    mt_stdout_2_console();
    while(!mt_are_ready_queues_empty()) yas_utils_nanosleep(1000);
    
    for(i=0 ; i < trace_dyn_index ; ++i) {
        if(trace_dyn_buf[i].trid == TRACE_BEGIN_ACTIVATE_TASK && trace_dyn_buf[i].elid == fid) {
            ++cnt_start_task_f;
            
            last_start_f = trace_dyn_buf[i].time;
            ck_assert_uint_lt(last_end_f, last_start_f);
        }
        else if(trace_dyn_buf[i].trid == TRACE_END_FINALIZE_TASK && trace_dyn_buf[i].elid == fid) {
            ++cnt_end_task_f;
            ck_assert_int_eq(cnt_start_task_f, cnt_end_task_f);
            
            last_end_f = trace_dyn_buf[i].time;
            ck_assert_uint_lt(last_start_f, last_end_f);
        }
        
        else if(trace_dyn_buf[i].trid == TRACE_BEGIN_ACTIVATE_TASK && trace_dyn_buf[i].elid == jid) {
            ++cnt_start_task_j;
            
            last_start_j = trace_dyn_buf[i].time;
            ck_assert_uint_lt(last_end_j, last_start_j);
            
            ck_assert_uint_lt(last_end_f, last_start_j);
        }
        else if(trace_dyn_buf[i].trid == TRACE_END_FINALIZE_TASK && trace_dyn_buf[i].elid == jid) {
            ++cnt_end_task_j;
            ck_assert_int_eq(cnt_start_task_j, cnt_end_task_j);
            
            last_end_j = trace_dyn_buf[i].time;
            ck_assert_uint_lt(last_start_j, last_end_j);
        }
        
        else if(trace_dyn_buf[i].trid == TRACE_RESOURCE_ACQUIRE && trace_dyn_buf[i].elid == resid) {
            ++cnt_acq_res1;
            ck_assert_uint_eq(cnt_acq_res1, cnt_rel_res1+1);
        }
        else if(trace_dyn_buf[i].trid == TRACE_RESOURCE_RELEASE && trace_dyn_buf[i].elid == resid) {
            ++cnt_rel_res1;
            ck_assert_uint_eq(cnt_acq_res1, cnt_rel_res1);
        }
        
        else if(trace_dyn_buf[i].trid == TRACE_RESOURCE_ACQUIRE && trace_dyn_buf[i].elid == resid2) {
            ++cnt_acq_res2;
            ck_assert_uint_eq(cnt_acq_res2, cnt_rel_res2+1);
        }
        else if(trace_dyn_buf[i].trid == TRACE_RESOURCE_RELEASE && trace_dyn_buf[i].elid == resid2) {
            ++cnt_rel_res2;
            ck_assert_uint_eq(cnt_acq_res2, cnt_rel_res2);
        }
    }
    
    mt_stdout_2_devnull();
    yas_cleanup(); // free trace_dyn_buf
    mt_stdout_2_console();
    
    ck_assert_int_eq(cnt_start_task_f, cnt_end_task_f);
    ck_assert_int_eq(cnt_start_task_f, cnt_acq_res1);
    
    ck_assert_int_eq(cnt_start_task_j, cnt_end_task_j);
    ck_assert_int_eq(cnt_start_task_j, cnt_acq_res2);
    
} END_TEST

START_TEST(SingleSharedResource) {
    yas_task_id_t fid, jid;
    yas_task_data_t f, j;
    yas_hwaccel_id_t resid;
    yas_hwaccel_id_t resid2;
    int i;
    uint32_t cnt_start_task_f=0, cnt_end_task_f=0, 
            cnt_start_task_j=0, cnt_end_task_j=0,
            cnt_acq_res = 0, cnt_rel_res = 0;
    yas_time_t last_start_f=0, last_end_f=0, 
                last_start_j=0, last_end_j=0;
    
    yas_init();
    
    resid = yas_hwaccel_decl("gpu");
    
    strncpy(f.name, "Fork", 256);
    f.period = period_a;
    
    strncpy(j.name, "Join", 256);
    j.period = period_a;
    
    fid = yas_task_decl(&f, &myfork, (void*)&resid);
    jid = yas_task_decl(&j, &join, (void*)&resid);
    
    yas_hwaccel_use(fid, 0, resid);
    yas_hwaccel_use(jid, 0, resid);
    
    mt_stdout_2_devnull();
    ck_assert(yas_start());
    
    yas_utils_nanosleep(exec_time);
    
    yas_stop();
    mt_stdout_2_console();
    while(!mt_are_ready_queues_empty()) yas_utils_nanosleep(1000);
    
    for(i=0 ; i < trace_dyn_index ; ++i) {
        if(trace_dyn_buf[i].trid == TRACE_BEGIN_ACTIVATE_TASK && trace_dyn_buf[i].elid == fid) {
            ++cnt_start_task_f;
            
            last_start_f = trace_dyn_buf[i].time;
            ck_assert_uint_lt(last_end_f, last_start_f);
        }
        else if(trace_dyn_buf[i].trid == TRACE_END_FINALIZE_TASK && trace_dyn_buf[i].elid == fid) {
            ++cnt_end_task_f;
            ck_assert_int_eq(cnt_start_task_f, cnt_end_task_f);
            
            last_end_f = trace_dyn_buf[i].time;
            ck_assert_uint_lt(last_start_f, last_end_f);
        }
        
        else if(trace_dyn_buf[i].trid == TRACE_BEGIN_ACTIVATE_TASK && trace_dyn_buf[i].elid == jid) {
            ++cnt_start_task_j;
            
            last_start_j = trace_dyn_buf[i].time;
            ck_assert_uint_lt(last_end_j, last_start_j);
            
            ck_assert_uint_lt(last_end_f, last_start_j);
        }
        else if(trace_dyn_buf[i].trid == TRACE_END_FINALIZE_TASK && trace_dyn_buf[i].elid == jid) {
            ++cnt_end_task_j;
            ck_assert_int_eq(cnt_start_task_j, cnt_end_task_j);
            
            last_end_j = trace_dyn_buf[i].time;
            ck_assert_uint_lt(last_start_j, last_end_j);
        }
        
        else if(trace_dyn_buf[i].trid == TRACE_RESOURCE_ACQUIRE && trace_dyn_buf[i].elid == resid) {
            ++cnt_acq_res;
            ck_assert_uint_eq(cnt_acq_res, cnt_rel_res+1);
        }
        else if(trace_dyn_buf[i].trid == TRACE_RESOURCE_RELEASE && trace_dyn_buf[i].elid == resid) {
            ++cnt_rel_res;
            ck_assert_uint_eq(cnt_acq_res, cnt_rel_res);
        }
        
    }
    
    mt_stdout_2_devnull();
    yas_cleanup(); // free trace_dyn_buf
    mt_stdout_2_console();
    
    ck_assert_int_eq(cnt_start_task_f, cnt_end_task_f);
    
    ck_assert_int_eq(cnt_start_task_j, cnt_end_task_j);
    
    ck_assert_int_eq(cnt_start_task_f+cnt_start_task_j, cnt_acq_res);
    
} END_TEST

Suite *test_suite() {
    Suite *s;
    TCase *tc_core;
    s = suite_create("Single core NoPreempt Resources");
    tc_core = tcase_create("Core");

    tcase_add_test(tc_core, SeparateResources);
    tcase_add_test(tc_core, SingleSharedResource);
    
    suite_add_tcase(s, tc_core);
    return s;
}

int main(void) {
    int number_failed;
    Suite *s;
    SRunner *sr;

    s = test_suite();
    sr = srunner_create(s);
    
    srunner_set_fork_status(sr, CK_NOFORK);

    srunner_run_all(sr, CK_NORMAL);
    number_failed = srunner_ntests_failed(sr);
    srunner_free(sr);
    return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}

