/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include <stdlib.h>
#include <check.h>
#include "job_pool.h"
#include "../blockcalls.h"

void print_heap(y_task_pool_t *pool) {
    size_t i;
    printf("%d->", pool->q_elt_pool_cnt);
    for(i = 1 ; i <= pool->q_elt_pool_cnt ; ++i)
        printf("%d-%d|", pool->q_elt_pool[i].key, pool->q_elt_pool[i].data->_internal_id);
    printf("\n");
}

START_TEST (test_task_pool_create) {
    y_task_pool_t pool;
    y_job_pool_init(&pool);
    
    ck_assert_int_eq(pool.q_elt_pool_cnt, 0);
    ck_assert_int_eq(y_job_pool_head_priority(&pool), YAS_LOWEST_PRIORITY);
    
    y_job_pool_cleanup(&pool);
} END_TEST

START_TEST (test_task_pool_cleanup) {
    y_task_pool_t pool;
    y_job_pool_init(&pool);
    y_job_pool_cleanup(&pool);
    
    ck_assert_int_eq(pool.q_elt_pool_cnt, 0);
    
} END_TEST

START_TEST (test_task_pool_add) {
    y_task_pool_t pool;
    yas_intern_task_t t1, t2, t3, t4, t5, t6;
    y_task_init(&t1);
    y_task_init(&t2);
    y_task_init(&t3);
    y_task_init(&t4);
    y_task_init(&t5);
    y_task_init(&t6);
    
    t1._internal_id = 1;
    t1.priority = 10;    
    t2._internal_id = 2;
    t2.priority = 20;    
    t3._internal_id = 3;
    t3.priority = 30;    
    t4._internal_id = 4;
    t4.priority = 2;    
    t5._internal_id = 5;
    t5.priority = 9;    
    t6._internal_id = 6;
    t6.priority = 25;
    
    y_job_pool_init(&pool);
        
    y_job_pool_push_unlocked(&pool, &t1);
    ck_assert_int_eq(pool.q_elt_pool_cnt, 1);
    ck_assert_ptr_eq(pool.q_elt_pool[1].data, &t1);
    
    y_job_pool_push_unlocked(&pool, &t2);
    ck_assert_int_eq(pool.q_elt_pool_cnt, 2);
    ck_assert_ptr_eq(pool.q_elt_pool[1].data, &t1);
    ck_assert_ptr_eq(pool.q_elt_pool[2].data, &t2);
    
    y_job_pool_push_unlocked(&pool, &t3);
    ck_assert_int_eq(pool.q_elt_pool_cnt, 3);
    ck_assert_ptr_eq(pool.q_elt_pool[1].data, &t1);
    ck_assert_ptr_eq(pool.q_elt_pool[2].data, &t2);
    ck_assert_ptr_eq(pool.q_elt_pool[3].data, &t3);
    
    y_job_pool_push_unlocked(&pool, &t4);
    ck_assert_int_eq(pool.q_elt_pool_cnt, 4);
    ck_assert_ptr_eq(pool.q_elt_pool[1].data, &t4);
    ck_assert_ptr_eq(pool.q_elt_pool[2].data, &t1);
    ck_assert_ptr_eq(pool.q_elt_pool[3].data, &t3);
    ck_assert_ptr_eq(pool.q_elt_pool[4].data, &t2);
    
    y_job_pool_push_unlocked(&pool, &t5);
    ck_assert_int_eq(pool.q_elt_pool_cnt, 5);
    ck_assert_ptr_eq(pool.q_elt_pool[1].data, &t4);
    ck_assert_ptr_eq(pool.q_elt_pool[2].data, &t5);
    ck_assert_ptr_eq(pool.q_elt_pool[3].data, &t3);
    ck_assert_ptr_eq(pool.q_elt_pool[4].data, &t2);
    ck_assert_ptr_eq(pool.q_elt_pool[5].data, &t1);
    
    y_job_pool_push_unlocked(&pool, &t6);
    ck_assert_int_eq(pool.q_elt_pool_cnt, 6);
    ck_assert_ptr_eq(pool.q_elt_pool[1].data, &t4);
    ck_assert_ptr_eq(pool.q_elt_pool[2].data, &t5);
    ck_assert_ptr_eq(pool.q_elt_pool[3].data, &t6);
    ck_assert_ptr_eq(pool.q_elt_pool[4].data, &t2);
    ck_assert_ptr_eq(pool.q_elt_pool[5].data, &t1);
    ck_assert_ptr_eq(pool.q_elt_pool[6].data, &t3);
    
    y_job_pool_cleanup(&pool);
} END_TEST

START_TEST (test_task_pool_doubleadd) {
    y_task_pool_t pool;
    yas_intern_task_t t1, t2, t3, t4, t5, t6;
    y_task_init(&t1);
    y_task_init(&t2);
    y_task_init(&t3);
    y_task_init(&t4);
    y_task_init(&t5);
    y_task_init(&t6);
    
    t1._internal_id = 1;
    t1.priority = 10;    
    t2._internal_id = 2;
    t2.priority = 20;    
    t3._internal_id = 3;
    t3.priority = 30;    
    t4._internal_id = 4;
    t4.priority = 5;    
    t5._internal_id = 5;
    t5.priority = 9;    
    t6._internal_id = 6;
    t6.priority = 25;
    
    y_job_pool_init(&pool);
        
    y_job_pool_push_unlocked(&pool, &t1);
    y_job_pool_push_unlocked(&pool, &t2);
    y_job_pool_push_unlocked(&pool, &t3);
    y_job_pool_push_unlocked(&pool, &t4);
    y_job_pool_push_unlocked(&pool, &t5);
    y_job_pool_push_unlocked(&pool, &t6);
    
    y_job_pool_push_unlocked(&pool, &t2);
    ck_assert_int_eq(pool.q_elt_pool_cnt, 6);
    ck_assert_ptr_eq(pool.q_elt_pool[1].data, &t4);
    ck_assert_ptr_eq(pool.q_elt_pool[2].data, &t5);
    ck_assert_ptr_eq(pool.q_elt_pool[3].data, &t6);
    ck_assert_ptr_eq(pool.q_elt_pool[4].data, &t2);
    ck_assert_ptr_eq(pool.q_elt_pool[5].data, &t1);
    ck_assert_ptr_eq(pool.q_elt_pool[6].data, &t3);
    
    t3.priority = 4;
    y_job_pool_push_unlocked(&pool, &t3);
    ck_assert_int_eq(pool.q_elt_pool_cnt, 6);
    ck_assert_ptr_eq(pool.q_elt_pool[1].data, &t3);
    ck_assert_ptr_eq(pool.q_elt_pool[2].data, &t5);
    ck_assert_ptr_eq(pool.q_elt_pool[3].data, &t4);
    ck_assert_ptr_eq(pool.q_elt_pool[4].data, &t2);
    ck_assert_ptr_eq(pool.q_elt_pool[5].data, &t1);
    ck_assert_ptr_eq(pool.q_elt_pool[6].data, &t6);
    
    t5.priority = 3;
    y_job_pool_push_unlocked(&pool, &t5);
    ck_assert_int_eq(pool.q_elt_pool_cnt, 6);
    ck_assert_ptr_eq(pool.q_elt_pool[1].data, &t5);
    ck_assert_ptr_eq(pool.q_elt_pool[2].data, &t3);
    ck_assert_ptr_eq(pool.q_elt_pool[3].data, &t4);
    ck_assert_ptr_eq(pool.q_elt_pool[4].data, &t2);
    ck_assert_ptr_eq(pool.q_elt_pool[5].data, &t1);
    ck_assert_ptr_eq(pool.q_elt_pool[6].data, &t6);
    
    t3.priority = 50;
    y_job_pool_push_unlocked(&pool, &t3);
    ck_assert_int_eq(pool.q_elt_pool_cnt, 6);
    ck_assert_ptr_eq(pool.q_elt_pool[1].data, &t5);
    ck_assert_ptr_eq(pool.q_elt_pool[2].data, &t1);
    ck_assert_ptr_eq(pool.q_elt_pool[3].data, &t4);
    ck_assert_ptr_eq(pool.q_elt_pool[4].data, &t2);
    ck_assert_ptr_eq(pool.q_elt_pool[5].data, &t3);
    ck_assert_ptr_eq(pool.q_elt_pool[6].data, &t6);
    
    y_job_pool_cleanup(&pool);
} END_TEST

void block_pop_wrapper(void *ppool) {
    y_task_pool_t *pool = (y_task_pool_t*)ppool;
    yas_intern_task_t *r;
    y_job_pool_pop(pool, &r);
}

START_TEST (test_task_pool_rem) {
   y_task_pool_t pool;
    yas_intern_task_t t1, t2, t3, t4, t5, t6;
    y_task_init(&t1);
    y_task_init(&t2);
    y_task_init(&t3);
    y_task_init(&t4);
    y_task_init(&t5);
    y_task_init(&t6);
    
    t1._internal_id = 1;
    t1.priority = 10;    
    t2._internal_id = 2;
    t2.priority = 20;    
    t3._internal_id = 3;
    t3.priority = 30;    
    t4._internal_id = 4;
    t4.priority = 5;    
    t5._internal_id = 5;
    t5.priority = 9;    
    t6._internal_id = 6;
    t6.priority = 25;
    
    y_job_pool_init(&pool);
        
    y_job_pool_push_unlocked(&pool, &t1);
    y_job_pool_push_unlocked(&pool, &t2);
    y_job_pool_push_unlocked(&pool, &t3);
    y_job_pool_push_unlocked(&pool, &t4);
    y_job_pool_push_unlocked(&pool, &t5);
    y_job_pool_push_unlocked(&pool, &t6);
    
    ck_assert(blockcalls(block_pop_wrapper, (void*)&pool, 1) == CALL_NOTBLOCKED);
    ck_assert_int_eq(pool.q_elt_pool_cnt, 5);
    ck_assert_ptr_eq(pool.q_elt_pool[1].data, &t5);
    ck_assert_ptr_eq(pool.q_elt_pool[2].data, &t1);
    ck_assert_ptr_eq(pool.q_elt_pool[3].data, &t6);
    ck_assert_ptr_eq(pool.q_elt_pool[4].data, &t2);
    ck_assert_ptr_eq(pool.q_elt_pool[5].data, &t3);
    
    ck_assert(blockcalls(block_pop_wrapper, (void*)&pool, 1) == CALL_NOTBLOCKED);
    ck_assert_int_eq(pool.q_elt_pool_cnt, 4);
    ck_assert_ptr_eq(pool.q_elt_pool[1].data, &t1);
    ck_assert_ptr_eq(pool.q_elt_pool[2].data, &t2);
    ck_assert_ptr_eq(pool.q_elt_pool[3].data, &t6);
    ck_assert_ptr_eq(pool.q_elt_pool[4].data, &t3);
    
    ck_assert(blockcalls(block_pop_wrapper, (void*)&pool, 1) == CALL_NOTBLOCKED);
    ck_assert_int_eq(pool.q_elt_pool_cnt, 3);
    ck_assert_ptr_eq(pool.q_elt_pool[1].data, &t2);
    ck_assert_ptr_eq(pool.q_elt_pool[2].data, &t3);
    ck_assert_ptr_eq(pool.q_elt_pool[3].data, &t6);
    
    ck_assert(blockcalls(block_pop_wrapper, (void*)&pool, 1) == CALL_NOTBLOCKED);
    ck_assert_int_eq(pool.q_elt_pool_cnt, 2);
    ck_assert_ptr_eq(pool.q_elt_pool[1].data, &t6);
    ck_assert_ptr_eq(pool.q_elt_pool[2].data, &t3);
    
    ck_assert(blockcalls(block_pop_wrapper, (void*)&pool, 1) == CALL_NOTBLOCKED);
    ck_assert_int_eq(pool.q_elt_pool_cnt, 1);
    ck_assert_ptr_eq(pool.q_elt_pool[1].data, &t3);
    
    ck_assert(blockcalls(block_pop_wrapper, (void*)&pool, 1) == CALL_NOTBLOCKED);
    ck_assert_int_eq(pool.q_elt_pool_cnt, 0);
    
    ck_assert(blockcalls(block_pop_wrapper, (void*)&pool, 1) == CALL_BLOCKED);
    
    y_job_pool_cleanup(&pool);
} END_TEST

START_TEST(test_task_pool_bad_size) {
    y_task_pool_t pool;
    yas_intern_task_t t1, t2, t3, t4, t5, t6, t7;
    y_task_init(&t1);
    y_task_init(&t2);
    y_task_init(&t3);
    y_task_init(&t4);
    y_task_init(&t5);
    y_task_init(&t6);
    y_task_init(&t7);
    
    t1._internal_id = 1;
    t1.priority = 10;    
    t2._internal_id = 2;
    t2.priority = 20;    
    t3._internal_id = 3;
    t3.priority = 30;    
    t4._internal_id = 4;
    t4.priority = 5;    
    t5._internal_id = 5;
    t5.priority = 9;    
    t6._internal_id = 6;
    t6.priority = 25;
    t7._internal_id = 7;
    t7.priority = 100;
    
    y_job_pool_init(&pool);
        
    y_job_pool_push_unlocked(&pool, &t1);
    y_job_pool_push_unlocked(&pool, &t2);
    y_job_pool_push_unlocked(&pool, &t3);
    y_job_pool_push_unlocked(&pool, &t4);
    y_job_pool_push_unlocked(&pool, &t5);
    y_job_pool_push_unlocked(&pool, &t6);
    y_job_pool_push_unlocked(&pool, &t7);
    
    ck_assert(0); //shouldn't reach this point
} END_TEST

Suite *test_suite() {
    Suite *s;
    TCase *tc_core;
    s = suite_create("Task Pool");
    tc_core = tcase_create("Core");

    tcase_add_test(tc_core, test_task_pool_create);
    tcase_add_test(tc_core, test_task_pool_cleanup);
    tcase_add_test(tc_core, test_task_pool_add);
    tcase_add_test(tc_core, test_task_pool_doubleadd);
    tcase_add_test(tc_core, test_task_pool_rem);
    tcase_add_test_raise_signal(tc_core, test_task_pool_bad_size, SIGFPE);
    
    tcase_set_timeout(tc_core, 15);
    
    suite_add_tcase(s, tc_core);
    return s;
}

int main(void) {
    int number_failed;
    Suite *s;
    SRunner *sr;

    s = test_suite();
    sr = srunner_create(s);
    
    srunner_set_fork_status(sr, CK_FORK); //need to fork to catch the signal

    srunner_run_all(sr, CK_NORMAL);
    number_failed = srunner_ntests_failed(sr);
    srunner_free(sr);
    return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
