/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   config.h
 * Author: brouxel
 *
 * Created on 7 décembre 2020, 09:32
 */

#ifndef CONFIG_H
#define CONFIG_H

#include <time.h>
#include <pthread.h>

#include "constants.h"

#define YAS_PERIODIC_TASK_SIZE 5
#define YAS_TASK_POOL_JOBS_SIZE 5

#define YAS_SYNCHRO_FORCE_POSIX
#endif /* CONFIG_H */

