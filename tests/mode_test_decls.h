/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   mode_test_decls.h
 * Author: brouxel
 *
 * Created on 9 décembre 2020, 09:13
 */

#ifndef MODE_TEST_DECLS_H
#define MODE_TEST_DECLS_H

#ifdef __cplusplus
extern "C" {
#endif
    
#include "yasmin_api.h"
#include "task.h"
    

//------------------------------------------------------------------------------
//--- coord_api_online.c
yas_time_t mt_online_get_sched_period();
y_task_sort_func_t mt_online_get_sort_func();
ybool mt_online_is_running();
yas_intern_task_t* mt_get_recurring_tasks();
size_t mt_get_recurring_task_size();
yas_intern_task_t* mt_get_nonrecurring_tasks();
size_t mt_get_nonrecurring_task_size();

//------------------------------------------------------------------------------
//-- thread_management.c
ybool mt_are_ready_queues_empty();

//------------------------------------------------------------------------------
//-- api_tests.c
void mt_stdout_2_devnull();
void mt_stdout_2_console();

size_t mt_get_nb_resources();
char * mt_get_resource_name(size_t i);

#ifdef __cplusplus
}
#endif

#endif /* MODE_TEST_DECLS_H */

