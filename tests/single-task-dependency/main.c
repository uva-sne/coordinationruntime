/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include <check.h>
#include "yasmin_api.h"
#include "trace.h"
#include "../blockcalls.h"
#include "../mode_test_decls.h"

#define MS2NS(X) (X*1000000)
#define S2NS(X)  (X*1000000000l)

yas_time_t exec_time = S2NS(5);

yas_time_t period_a = MS2NS(10);

struct token {
    int value;
};

yas_channel_decl(fl, int, 0);
yas_channel_decl(fj, struct token *, 1);
yas_channel_decl(fj2, int, 1);
yas_channel_decl(fr, float, 4);
yas_channel_decl(lj, struct token, 2);
yas_channel_decl(rj, int *, 3);


struct token t[4] = { {.value = 12}, {.value=13 }, {.value=14}, {.value=15}};
void myfork(void *d) {
    int a = 42;
    float b = MAXFLOAT/4.0f;
    int c = 4;
    int i;
//    printf("Start fork\n");

    yas_channel_push(fj, &t[0]);
    yas_channel_push(fj2,c);
    for(i = 0 ; i < 4 ; ++i)
        yas_channel_push(fr,b);
//    printf("End fork\n");
}
void join(void *d) {
    struct token *t;
    int size = 0;
    int accu = 0;
    int i;
    struct token left1, left2;
    int * right[3];
    
//    printf("Start join\n");
    
    yas_channel_pop(fj, t);
    yas_channel_pop(fj2, size);
    
    ck_assert_int_eq(size, 4);
    for(i=0 ; i < 4 ; ++i) {
        accu += t[i].value;
    }
    ck_assert_int_eq(accu, 12+13+14+15);
    
    yas_channel_pop(lj, left1);
    yas_channel_pop(lj, left2);
    ck_assert_int_eq(left1.value, left2.value*42);
    
    for(i = 0 ; i < 3 ; ++i) {
        yas_channel_pop(rj, right[i]);
    }
    ck_assert_int_eq(*(right[0]), 7);
    ck_assert_int_eq(*(right[1]), 8);
    ck_assert_int_eq(*(right[2]), 9);
    
//    printf("End join\n");
}
void left(void *d) {
    struct token left1, left2;
//    printf("Start left\n");
    
    left2.value = 456;
    left1.value = left2.value*42;
    
    yas_channel_push(lj, left1);
    yas_channel_push(lj, left2);
    
//    printf("End left\n");
}

int r1=7, r2=8, r3=9;
void right(void *d) {
    float f[4];
    int i;
    
//    printf("Start right\n");
    
    for(i = 0 ; i < 4 ; ++i) 
        yas_channel_pop(fr, f[i]);
    
    ck_assert_float_eq(f[0]+f[1]+f[2]+f[3], MAXFLOAT);
    
    yas_channel_push(rj, &r1);
    yas_channel_push(rj, &r2);
    yas_channel_push(rj, &r3);
    
//    printf("End right\n");
}

START_TEST(ForkJoin) {
    yas_task_id_t fid, jid, lid, rid;
    yas_task_data_t f, j, l, r;
    int i;
    uint32_t cnt_start_task_f=0, cnt_end_task_f=0, 
            cnt_start_task_l=0, cnt_end_task_l=0, 
            cnt_start_task_r=0, cnt_end_task_r=0,
            cnt_start_task_j=0, cnt_end_task_j=0;
    yas_time_t last_start_f=0, last_end_f=0, 
                last_start_l=0, last_end_l=0, 
                last_start_r=0, last_end_r=0, 
                last_start_j=0, last_end_j=0;
    
    yas_init();
    
    strncpy(f.name, "Fork", 256);
    f.period = period_a;
    
    strncpy(j.name, "Join", 256);
    j.period = 0;
    
    strncpy(l.name, "Left", 256);
    l.period = 0;
    
    strncpy(r.name, "Right", 256);
    r.period = 0;
    
    fid = yas_task_decl(&f, &myfork, NULL);
    jid = yas_task_decl(&j, &join, NULL);
    lid = yas_task_decl(&l, &left, NULL);
    rid = yas_task_decl(&r, &right, NULL);
    
    yas_channel_connect(fid, lid, fl);
    yas_channel_connect(fid, rid, fr);
    yas_channel_connect(lid, jid, lj);
    yas_channel_connect(rid, jid, rj);
    yas_channel_connect(fid, jid, fj);
    yas_channel_connect(fid, jid, fj2);
    
    ck_assert(!mt_online_is_running());
    ck_assert_int_eq(mt_get_recurring_task_size(), 1);
    ck_assert_int_eq(mt_get_nonrecurring_task_size(), 3);
    
    ck_assert(yas_start());
    ck_assert(mt_online_is_running());
    ck_assert(mt_online_get_sched_period() == period_a);
    
    yas_utils_nanosleep(exec_time);
    
    yas_stop();
    ck_assert(!mt_online_is_running());
    
    for(i=0 ; i < trace_dyn_index ; ++i) {
        if(trace_dyn_buf[i].trid == TRACE_BEGIN_ACTIVATE_TASK && trace_dyn_buf[i].elid == fid) {
            ++cnt_start_task_f;
            
            last_start_f = trace_dyn_buf[i].time;
            ck_assert_uint_lt(last_end_f, last_start_f);
        }
        else if(trace_dyn_buf[i].trid == TRACE_END_FINALIZE_TASK && trace_dyn_buf[i].elid == fid) {
            ++cnt_end_task_f;
            ck_assert_int_eq(cnt_start_task_f, cnt_end_task_f);
            
            last_end_f = trace_dyn_buf[i].time;
            ck_assert_uint_lt(last_start_f, last_end_f);
        }
        
        else if(trace_dyn_buf[i].trid == TRACE_BEGIN_ACTIVATE_TASK && trace_dyn_buf[i].elid == lid) {
            ++cnt_start_task_l;
            
            last_start_l = trace_dyn_buf[i].time;
            ck_assert_uint_lt(last_end_l, last_start_l);
            
            ck_assert_uint_lt(last_end_f, last_start_l);
        }
        else if(trace_dyn_buf[i].trid == TRACE_END_FINALIZE_TASK && trace_dyn_buf[i].elid == lid) {
            ++cnt_end_task_l;
            ck_assert_int_eq(cnt_start_task_l, cnt_end_task_l);
            
            last_end_l = trace_dyn_buf[i].time;
            ck_assert_uint_lt(last_start_l, last_end_l);
        }
        
        else if(trace_dyn_buf[i].trid == TRACE_BEGIN_ACTIVATE_TASK && trace_dyn_buf[i].elid == rid) {
            ++cnt_start_task_r;
            
            last_start_r = trace_dyn_buf[i].time;
            ck_assert_uint_lt(last_end_r, last_start_r);
            
            ck_assert_uint_lt(last_end_f, last_start_r);
        }
        else if(trace_dyn_buf[i].trid == TRACE_END_FINALIZE_TASK && trace_dyn_buf[i].elid == rid) {
            ++cnt_end_task_r;
            ck_assert_int_eq(cnt_start_task_r, cnt_end_task_r);
            
            last_end_r = trace_dyn_buf[i].time;
            ck_assert_uint_lt(last_start_r, last_end_r);
        }
        
        else if(trace_dyn_buf[i].trid == TRACE_BEGIN_ACTIVATE_TASK && trace_dyn_buf[i].elid == jid) {
            ++cnt_start_task_j;
            
            last_start_j = trace_dyn_buf[i].time;
            ck_assert_uint_lt(last_end_j, last_start_j);
            
            ck_assert_uint_lt(last_end_f, last_start_j);
            ck_assert_uint_lt(last_end_r, last_start_j);
            ck_assert_uint_lt(last_end_l, last_start_j);
        }
        else if(trace_dyn_buf[i].trid == TRACE_END_FINALIZE_TASK && trace_dyn_buf[i].elid == jid) {
            ++cnt_end_task_j;
            ck_assert_int_eq(cnt_start_task_j, cnt_end_task_j);
            
            last_end_j = trace_dyn_buf[i].time;
            ck_assert_uint_lt(last_start_j, last_end_j);
        }
    }
    
    mt_stdout_2_devnull();
    yas_cleanup(); // free trace_dyn_buf
    mt_stdout_2_console();
    
    ck_assert_int_eq(cnt_start_task_f, cnt_end_task_f);
    
    ck_assert_int_eq(cnt_start_task_l, cnt_end_task_l);
    ck_assert_int_eq(cnt_start_task_l, cnt_start_task_f);
    
    ck_assert_int_eq(cnt_start_task_r, cnt_end_task_r);
    ck_assert_int_eq(cnt_start_task_r, cnt_start_task_f);
    
    ck_assert_int_eq(cnt_start_task_j, cnt_end_task_j);
    ck_assert_int_eq(cnt_start_task_j, cnt_start_task_f);
    
} END_TEST

Suite *test_suite() {
    Suite *s;
    TCase *tc_core;
    s = suite_create("Single core Task dependency");
    tc_core = tcase_create("Core");

    tcase_add_test(tc_core, ForkJoin);
    
    suite_add_tcase(s, tc_core);
    return s;
}

int main(void) {
    int number_failed;
    Suite *s;
    SRunner *sr;

    s = test_suite();
    sr = srunner_create(s);
    
    srunner_set_fork_status(sr, CK_NOFORK);

    srunner_run_all(sr, CK_NORMAL);
    number_failed = srunner_ntests_failed(sr);
    srunner_free(sr);
    return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}

