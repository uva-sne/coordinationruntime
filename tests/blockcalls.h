/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   blockcalls.h
 * Author: brouxel
 *
 * Created on 8 décembre 2020, 09:34
 */

#ifndef BLOCKCALLS_H
#define BLOCKCALLS_H

#ifdef __cplusplus
extern "C" {
#endif
    
#include "threading.h"
#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
    
#define CALL_BLOCKED 1
#define CALL_NOTBLOCKED 0
    
    typedef void   (*BlockCallFunc)          (void *);
    
    struct wrapper_arg {
        BlockCallFunc func;
        void *args;
    };

    void* thread_wrapper(volatile void *pcall) {
        
        struct wrapper_arg *call = (struct wrapper_arg*)pcall;
        call->func(call->args);
        return NULL;
    }
    
    short blockcalls(BlockCallFunc func, void *data, int timeout) {
        y_system_thread thrd;
        int check;
        struct wrapper_arg *wa = (struct wrapper_arg*) malloc(sizeof(struct wrapper_arg));
        wa->func = func;
        wa->args = data;
        if(!y_system_thread_create(&thrd, thread_wrapper, (void*)wa, -1, -1)) {
            printf("Couldn't create the thread to check a potentially blocking call\n");
            return -1;
        }
        yas_utils_nanosleep(timeout*1000000000);
        check = pthread_tryjoin_np(thrd, NULL);
        free(wa);
        if(check == 0) {
            return CALL_NOTBLOCKED;
        }
        pthread_cancel(thrd);
        return CALL_BLOCKED;
    }


#ifdef __cplusplus
}
#endif

#endif /* BLOCKCALLS_H */

