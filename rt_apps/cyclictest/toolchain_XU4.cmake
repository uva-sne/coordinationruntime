set(GCC_INSTALL "/home/brouxel/Projects/Software/cross-compile/odroid/gcc-linaro-7.4.1-2019.02-x86_64_arm-linux-gnueabihf")

set(CMAKE_FIND_ROOT_PATH ${GCC_INSTALL})
set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY)

set(CMAKE_SYSTEM_NAME Linux)
set(CMAKE_SYSTEM_PROCESSOR arm)
set(CMAKE_C_COMPILER "${GCC_INSTALL}/bin/arm-linux-gnueabihf-gcc")

set(CMAKE_C_FLAGS " -g -O0 -Wall -Werror -Wno-unused-parameter -Wno-unused-function -I${GCC_INSTALL}include ${CMAKE_C_FLAGS} ")
set(CMAKE_EXE_LINKER_FLAGS "-static -L${GCC_INSTALL}lib")

set(CMAKE_SHARED_LIBRARY_LINK_C_FLAGS "") #remove -rdynamic from linking option
#set(CMAKE_EXE_EXPORTS_C_FLAG "")

set(CMAKE_SYSTEM_NAME "Generic")
