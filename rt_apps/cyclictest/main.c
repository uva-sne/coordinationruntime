/*
 * Copyright (C) 2020 Benjamin Rouxel <benjamin.rouxel@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


/*
 cyclictest options for a fair comparison:
 * -m as mlockall is automatic in yasmin
 * -n use nanosleep
 * -c CLOCK must be in concordance with config.h
 * -p PRIO idem
 * -t NUM idem
 * -y POLY idem
 * -t maxcpus - 1 with affinity in accordance to config.h 
 * -S/-a  if config.h as partitioned scheduling
 *        and not if it is global scheduling
 */

/*
 * to check further
 * "-T TRACE --tracer=TRACER   set tracing function\n"
	       "    configured tracers: %s\n"
 */

/*
 ./cyclictest -t 6 -l 10000 -d 0 -i 10000 -p99
 ./cyclictest_yasmin -l 10000 -d 0 -i 10000
 ./cyclictest_litmus  -t 6 -l 10000 -d 0 -i 10000 -p99
  stress-ng -C 8 -c 8 -T 8 -y 8
 */

#include "yasmin_api.h"

#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <error.h>
#include <string.h>
#include <sched.h>
#include <unistd.h>


#define VERSION_STRING 0.1
#define USEC_PER_SEC		1000000l
#define NSEC_PER_SEC		1000000000l

#define US2NS(X) (X*1000)
#define MS2NS(X) (X*1000000l)
#define S2NS(X)  (X*NSEC_PER_SEC)
#define NS2US(X) (X/1000)

#define DEFAULT_INTERVAL 1000
#define DEFAULT_DISTANCE 500

/* Struct for statistics */
struct thread_stat {
	yas_time_t cycles;
	long long min;
	yas_time_t max;
	long long act;
	yas_time_t avg;
	yas_task_id_t tid;
        yas_time_t exec_time;
};

/* Struct to transfer parameters to the thread */
struct thread_param {
	int prio;
	yas_time_t max_cycles;
	struct thread_stat *stats;
	unsigned long interval;
        yas_time_t next;
        yas_time_t prev;
};


static yas_time_t stop;

static int shutdown;
static yas_time_t duration = 0;
static int use_nsecs = 0;
static yas_time_t max_cycles = 0;
static int quiet = 0;
static int periodic_activation = 0;
static int verbose = 0;
static yas_time_t interval = DEFAULT_INTERVAL;
static yas_time_t distance = DEFAULT_DISTANCE;

static int latency_target_fd = -1;
static int32_t latency_target_value = 0;

/* Latency trick
 * if the file /dev/cpu_dma_latency exists,
 * open it and write a zero into it. This will tell
 * the power management system not to transition to
 * a high cstate (in fact, the system acts like idle=poll)
 * When the fd to /dev/cpu_dma_latency is closed, the behavior
 * goes back to the system default.
 *
 * Documentation/power/pm_qos_interface.txt
 */
static void set_latency_target(void)
{
	struct stat s;
	int ret;

	if (stat("/dev/cpu_dma_latency", &s) == 0) {
		latency_target_fd = open("/dev/cpu_dma_latency", O_RDWR);
		if (latency_target_fd == -1)
			return;
		ret = write(latency_target_fd, &latency_target_value, 4);
		if (ret == 0) {
			(void)printf("# error setting cpu_dma_latency to %d!: %s\n", latency_target_value, strerror(errno));
			(void)close(latency_target_fd);
			return;
		}
		(void)printf("# /dev/cpu_dma_latency set to %dus\n", latency_target_value);
	}
}

static char *policyname(int policy)
{
	char *policystr = "";

	switch(policy) {
	case SCHED_OTHER:
		policystr = "other";
		break;
	case SCHED_FIFO:
		policystr = "fifo";
		break;
	case SCHED_RR:
		policystr = "rr";
		break;
	case SCHED_BATCH:
		policystr = "batch";
		break;
	case SCHED_IDLE:
		policystr = "idle";
		break;
	}
	return policystr;
}

/*
 * parse an input value as a base10 value followed by an optional
 * suffix. The input value is presumed to be in seconds, unless
 * followed by a modifier suffix: m=minutes, h=hours, d=days
 *
 * the return value is a value in seconds
 */
static yas_time_t parse_time_string(const char *val) {
	char *end;
	yas_time_t t = strtol(val, &end, 10);
	if (end) {
            switch (*end) {
		case 'm':
		case 'M':
			t *= 60;
			break;

		case 'h':
		case 'H':
			t *= 60*60;
			break;

		case 'd':
		case 'D':
			t *= 24*60*60;
			break;

            }
	}
	return S2NS(t);
}

static void timertask_periodic(void *param) {
    yas_time_t now;
    long long diff;
    struct thread_param *par; 
    struct thread_stat *stat; 
    
    now = yas_utils_now();
    
    par = (struct thread_param *)param;
    stat = par->stats;
    
    if(par->next > 0) {
        diff = now  - par->next;

        if (diff < stat->min)
            stat->min = diff;
        if (diff > 0 && diff > stat->max)
            stat->max = diff;
        stat->avg += diff > 0 ? diff : -1 * diff;
        stat->act = diff;
        stat->cycles++;

        if (duration && now >= stop)
            shutdown++;

        if (par->max_cycles && par->max_cycles == stat->cycles)
            shutdown++;
//    printf("------------------------> %llu %llu %lld %lld %llu %llu %llu \n", 
//    par->next, now, diff, stat->min, stat->max, stat->avg, (stat->avg/stat->cycles));
//    printf("\t %lld\n", now-par->prev);
    }
    
    par->next = now+US2NS(par->interval);
    par->prev= now;
    yas_time_t endexec = yas_utils_now();
    stat->exec_time = endexec - now;
}

// original cyclic_test waits and then compute the diff, in my case it feals weird
// it should compute the difference, then wait, then activate next task, therefore
// the displayed latency truly shows the time spend in Yasmin
//static void timertask_selfactivation(void *param) { 
//    struct thread_param *par = (struct thread_param *)param;
//    yas_time_t now, interval, stop;
//    long long diff;
//    struct thread_stat *stat = par->stats;
//
//    interval = US2NS(par->interval);
//
//    now = yas_utils_now();
//    par->next = now + interval;
//    
//// start by sleeping
//    yas_utils_nanosleep(interval);
//    now = yas_utils_now();
//// compute stats
//
//    if(par->next > 0) {
//        diff = now  - par->next;
//
//        if (diff < stat->min)
//            stat->min = diff;
//        if (diff > 0 && diff > stat->max)
//            stat->max = diff;
//        stat->avg += diff > 0 ? diff : -1 * diff;
//        stat->act = diff;
//        stat->cycles++;
//
//        if (duration && now - stop >= 0)
//            shutdown++;
//
//        if (par->max_cycles && par->max_cycles == stat->cycles)
//            shutdown++;
////        printf("------------------------> %llu %llu %lld %lld %llu %llu %llu \n", 
////        par->next, now, diff, stat->min, stat->max, stat->avg, (stat->avg/stat->cycles));
////        printf("\t %lld\n", now-par->prev);
//    }
//    
//    par->prev= now;
//    yas_time_t endexec = yas_utils_now();
//    stat->exec_time = endexec - now;
//    
//// activate next
//    if(!shutdown)
//        yas_task_activate(stat->tid);
//}

static void timertask_selfactivation(void *param) { 
    struct thread_param *par = (struct thread_param *)param;
    yas_time_t now, interval;
    long long diff;
    struct thread_stat *stat = par->stats;

    interval = US2NS(par->interval);

    now = yas_utils_now();
    
    if(par->next > 0) {
        diff = now  - par->next;

        if (diff < stat->min)
            stat->min = diff;
        if (diff > 0 && diff > stat->max)
            stat->max = diff;
        stat->avg += diff > 0 ? diff : -1 * diff;
        stat->act = diff;
        stat->cycles++;

        if (duration && now >= stop)
            shutdown++;

        if (par->max_cycles && par->max_cycles == stat->cycles)
            shutdown++;
//        printf("------------------------> %llu %llu %lld %lld %llu %llu %llu \n", 
//        par->next, now, diff, stat->min, stat->max, stat->avg, (stat->avg/stat->cycles));
//        printf("\t %lld\n", now-par->prev);
    }
    
    yas_time_t endexec = yas_utils_now();
    par->prev= now;
    par->next = endexec + interval;
    stat->exec_time = endexec - now;
    
    if(!shutdown)
        yas_utils_nanosleep(interval);
    
    if(!shutdown)
        yas_task_activate(stat->tid);
}

/* Print usage information */
static void display_help(int error)
{
    (void)printf("cyclictest V %1.2f\n", VERSION_STRING);
    (void)printf("Usage:\n"
           "cyclictest <options>\n\n"
           "-d DIST  --distance=DIST   distance of thread intervals in us default=500\n"
           "-D       --duration=t      specify a length for the test run\n"
           "                           default is in seconds, but 'm', 'h', or 'd' maybe added\n"
           "                           to modify value to minutes, hours or days\n"
           "-e       --latency=PM_QOS  write PM_QOS to /dev/cpu_dma_latency\n"
           "-i INTV  --interval=INTV   base interval of thread in us default=1000\n"
           "-N       --nsecs           print results in ns instead of us (default us)\n"
           "-q       --quiet           print only a summary on exit\n"
           "-p                         use a periodic alarm activation (similar to MODE_CYCLIC), "
           "                           if not present use the similar default self-activation pattern from "
           "                           the original cyclictest (MODE_CLOCK_NANOSLEEP)\n"
           "-v       --verbose         output values on stdout for statistics\n"
           "                           format: n:c:v n=tasknum c=count v=value in us\n"
    );
    if (error)
        exit(EXIT_FAILURE);
    exit(EXIT_SUCCESS);
}

/* Process commandline options */
static void process_options (int argc, char *argv[])
{
    for (;;) {
        int option_index = 0;
        /*
         * Options for getopt
         * Ordered alphabetically by single letter name
         */
        static struct option long_options[] = {
            {"distance",         required_argument, NULL, 'd'},
            {"duration",         required_argument, NULL, 'D'},
            {"latency",          required_argument, NULL, 'e'},
            {"interval",         required_argument, NULL, 'i'}, //period
            {"loops",            required_argument, NULL, 'l'},
            {"nsecs",            no_argument,       NULL, 'N'},
            {"quiet",            no_argument,       NULL, 'q'},
            {"periodic",         no_argument,       NULL, 'p'},
            {"verbose",          no_argument,       NULL, 'v'},
            {"help",             no_argument,       NULL, '?'},
            {NULL, 0, NULL, 0}
        };
        int c = getopt_long(argc, argv, "d:D:e:i:l:Nqpv", long_options, &option_index);
        if (c == -1)
                break;
        switch (c) {
            case 'd': distance = atoll(optarg); break;
            case 'D': duration = parse_time_string(optarg); break;
            case 'e': /* power management latency target value */
                      /* note: default is 0 (zero) */
                      latency_target_value = atoi(optarg);
                      if (latency_target_value < 0)
                              latency_target_value = 0;
                      break;
            case 'i': interval = atoll(optarg); break;
            case 'l': max_cycles = atoll(optarg); break;
            case 'N': use_nsecs = 1; break;
            case 'q': quiet = 1; break;
            case 'p': periodic_activation = 1; break;
            case 'v': verbose = 1; break;
            case '?': display_help(0); break;
        }
    }
}

static void print_stat(struct thread_param *par, int index, int verbose)
{
    struct thread_stat *stat = par->stats;

    if (!verbose) {
        if(!quiet) {
            char *fmt;
            fmt = "T:%2d (%5d) P:%2d I:%ld C:%7lu "
                    "Min:%7lld Act:%8lld Avg:%8llu Max:%8llu Exec: %8llu\n";
            if (use_nsecs) {
                (void)printf(fmt, index, stat->tid, par->prio,
                   par->interval, stat->cycles, stat->min, stat->act,
                   stat->cycles ?
                   (stat->avg/stat->cycles) : 0, stat->max, stat->exec_time);
            }
            else {
                (void)printf(fmt, index, stat->tid, par->prio,
                   par->interval, stat->cycles, NS2US(stat->min), NS2US(stat->act),
                   stat->cycles ?
                   (NS2US(stat->avg)/stat->cycles) : 0, NS2US(stat->max), NS2US(stat->exec_time));
            }
        }
    } 
}

int main(int argc, char **argv)
{
    int i, j, ret = -1, error = 0;
    
    yas_task_data_t tasks[YAS_THREAD_SIZE];
    struct thread_param **parameters;
    struct thread_stat **statistics;
    struct thread_param *par;
    struct thread_stat *stat;
    yas_task_data_t *t;

    process_options(argc, argv);
    
    /* use the /dev/cpu_dma_latency trick if it's there */
    set_latency_target();

    parameters = calloc(YAS_THREAD_SIZE, sizeof(struct thread_param *));
    if (parameters != NULL) {
        statistics = calloc(YAS_THREAD_SIZE, sizeof(struct thread_stat *));
        if (statistics != NULL) {

            yas_init();

            for (i = 0; i < YAS_THREAD_SIZE; i++) {
                t = &tasks[i];

                par = malloc(sizeof(struct thread_param));
                if (par == NULL) {
                    (void)printf("error allocating thread_param struct for thread %d\n", i);
                    error = 1;
                    break;
                }
                memset(par, 0, sizeof(struct thread_param));
                parameters[i] = par;
                
                /* allocate the thread's statistics block */
                stat = malloc(sizeof(struct thread_stat));
                if (stat == NULL) {
                    (void)printf("error allocating thread status struct for thread %d\n", i);
                    error = 1;
                    break;
                }
                memset(stat, 0, sizeof(struct thread_stat));
                statistics[i] = stat;
                
                if(periodic_activation) {
                    t->period = US2NS(interval);
                    t->deadline = US2NS(interval);
                }
                else {
                    t->period = t->deadline = 0;
                }
                if(YAS_TASK_MAPPING == YAS_MAPPING_PARTITIONED_SCHEME) {
                    t->virt_core_id = i;
                }
                yas_build_task_name(t->name, "timerthread");

                par->interval = interval;
                par->max_cycles = max_cycles;
                par->stats = stat;
                par->next = 0;

                stat->act = 0;
                stat->cycles = 0;
                stat->min = LONG_MAX;
                stat->max = 0;
                stat->avg = 0;
                stat->exec_time = 0;

                if(periodic_activation)
                    stat->tid = yas_task_decl(t, timertask_periodic, (void*)par);
                else
                    stat->tid = yas_task_decl(t, timertask_selfactivation, (void*)par);

                interval += distance;
                if (verbose)
                    (void)printf("Thread %d Interval: %llu\n", i, interval);
            }
            
            if(!error) {

                if (duration) {
                    stop = yas_utils_now();
                    stop += duration;
                }
                (void)yas_start();
                if(!periodic_activation) {
                    for (i = 0; i < YAS_THREAD_SIZE; i++) {
                        yas_task_activate(parameters[i]->stats->tid);
                    }
                }
                usleep(interval);

                while (!shutdown) {
                    char lavg[256] = "";
                    int fd = -1, allstopped = 0;
                    ssize_t len = 0;
                    static char policystr[15] = "";
                    static char *slash = NULL;
                    static char *policystr2 = "";

                    if (!policystr) {
                        char *tmp = policyname(YAS_THREAD_SCHED_POLICY);
                        strncpy(policystr, "Linux", strlen(tmp));
                        strncat(policystr, tmp, strlen(tmp));
                        policystr[14] = '\0';
                    }

                    if (!slash) {
                        slash = "/";
                        switch(YAS_PRIORITY_ASSIGNMENT) {
                            case YAS_TASK_PRIORITY_RM:
                                if(YAS_TASK_MAPPING == YAS_MAPPING_GLOBAL_SCHEME)
                                    policystr2 = "G-RM";
                                else
                                    policystr2 = "P-RM";
                                break;
                            case YAS_TASK_PRIORITY_DM:
                                if(YAS_TASK_MAPPING == YAS_MAPPING_GLOBAL_SCHEME)
                                    policystr2 = "G-DM";
                                else
                                    policystr2 = "P-DM";
                                break;
                            case YAS_TASK_PRIORITY_EDF:
                                if(YAS_TASK_MAPPING == YAS_MAPPING_GLOBAL_SCHEME)
                                    policystr2 = "G-EDF";
                                else
                                    policystr2 = "P-EDF";
                                break;
                            case YAS_TASK_PRIORITY_FIFO:
                                if(YAS_TASK_MAPPING == YAS_MAPPING_GLOBAL_SCHEME)
                                    policystr2 = "G-FIFO";
                                else
                                    policystr2 = "P-FIFO";
                                break;
                            default:
                                policystr2 = "unknown";
                                break;
                        }
                    }

                    if (!verbose && !quiet) {
                            fd = open("/proc/loadavg", O_RDONLY, 0666);
                            len = read(fd, lavg, 255);
                            close(fd);
                            lavg[len-1] = 0x0;
                            (void)printf("policy: %s%s%s: loadavg: %s          \n\n",
                                   policystr, slash, policystr2, lavg);
                    }

                    for (i = 0; i < YAS_THREAD_SIZE; i++) {

                        print_stat(parameters[i], i, verbose);
                        if(max_cycles && statistics[i]->cycles >= max_cycles)
                            allstopped++;
                    }

                    yas_utils_nanosleep(MS2NS(10));
                    if (shutdown || allstopped)
                            break;
                    if (!verbose && !quiet)
                            (void)printf("\033[%dA", YAS_THREAD_SIZE + 2);

                }

                (void)yas_stop();
                ret = EXIT_SUCCESS;

                shutdown = 1;
                usleep(50000);

                if (quiet)
                        quiet = 2;
                for (i = 0; i < YAS_THREAD_SIZE; i++) {
                    if (quiet)
                            print_stat(parameters[i], i, 0);
                }

                for(i = 0 ; i < YAS_THREAD_SIZE ; ++i) {
                    free(parameters[i]);
                    free(statistics[i]);
                }
                free(parameters);
                free(statistics);
            }
            else {
                for(j = 0 ; j < i ; ++j) {
                    free(parameters[j]);
                    free(statistics[j]);
                }
                free(parameters);
                free(statistics);
            }
        }
        else {
            free(parameters);
        }
    }

    yas_cleanup();
    exit(ret);
}
