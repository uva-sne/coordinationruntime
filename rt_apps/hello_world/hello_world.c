/*
 * Copyright (C) 2020 Benjamin Rouxel <benjamin.rouxel@uva.nl>
 *                    University of Amsterdam
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>

#include "yasmin_config.h"
#include "yasmin_api.h"

#define MS2NS(X)  (X*1000000l)
#define S2NS(X)  (X*1000000000l)

static void hello_world(void *) {
    printf("Hello world!\n");
}

int main(int, char**) {
    yas_task_data_t hwtd;
    yas_task_id_t hwtid;
    
    yas_build_task_name(hwtd.name, "hello");
    hwtd.period = MS2NS(500);
    
    yas_init();
    
    hwtid = yas_task_decl(&hwtd, hello_world, NULL);
    yas_start();
    yas_utils_nanosleep(S2NS(2));
    yas_stop();
    
    yas_cleanup();

    return (EXIT_SUCCESS);
}

