#!/usr/bin/env python3

import sys
import subprocess
import queue
import glob
import os
import pprint
from pygnuplot import gnuplot
import pandas as pd
import pprint

def listmin(x):
    if len(x) == 0:
        return sys.maxsize
    tmin = sys.maxsize
    for v in x:
        if tmin > v:
            tmin = v
    return tmin

def getmin(x):
    if x == sys.maxsize:
        return 0
    else:
        return x

def listavg(x):
    if len(x) > 0:
        return sum(x) / len(x)
    else:
        return 0

def listmax(x):
    tmax = 0
    for v in x:
        if tmax < v:
            tmax = v
    return tmax

class CpuState:
    """Models the state of a CPU."""
    def __init__(self):
        self.last_begin_scheduling = False
        self.pending_signal = queue.Queue()
        self.last_begin_request_signal = False
        self.preemption_overhead = []
        self.preemption_back = []
        self.get_task = []
        self.act_task = []
        self.fin_task = []
        self.event_latency = False
        self.num_releases = 0

class CpuStateMollison:
    """Models the state of a CPU Mollison."""
    def __init__(self):
        self.last_begin_release_handler = False
        self.last_switch_away = "start_state"
        self.last_begin_scheduling = False
        self.pending_signal = False
        self.last_begin_request_signal = False
        self.num_releases = 0

class TraceMeasurements:
    
    def __init__(self, inputtracefile):
        
        self.measurements = {}

        is_offline_sched = False
        if "off" in inputtracefile:
            is_offline_sched = True
            
        records = self.extract_records(inputtracefile)
        self.extract_data(records, inputtracefile, is_offline_sched)
        
    def get_measurements(self):
        return self.measurements

    def extract_records(self, filename):
        raw_records = []
        subprocess.check_call(["sort -k2 " + filename + " -o " + filename], shell=True)
        file = open(filename,"r")
        raw_records += file.readlines()
        file.close()
    
        # Create the records array
        # Each entry in records is an array of the values from a single original record
        # An index for that record is also included as the last value in each record
        records = []
        for i,rec in enumerate(raw_records, start=1):
            nonewline = rec[:-1]
            lst = nonewline.split(" ")
            lst.append(i)
            records.append(lst)
        return records

    def extract_data(self, records, filename, is_offline_sched):
        # Core data model for the class
        scheduling_overheads = []
        context_switch_overheads = []
        signal_latencies = []
        request_overheads = []
        context_complete_preempt_time = []
        context_preempt_back = []
        get_task_latency = []
        act_task_latency = []
        fin_task_latency = []
        event_latency = []
        
        # Populate the data model
        cpus = {}
        tasks = {}
        line = 1

        for rec in records:
            if len(rec) < 3:
                continue
            if rec[2] not in cpus:
                cpus[rec[2]] = CpuState()
            if rec[0] == "TRACE_BEGIN_SCHEDULING":
                if rec[1] == '0':
                    continue
                if cpus[rec[2]].last_begin_scheduling is not False:
                    print("Invalid record: {}", rec)
                    exit()
                cpus[rec[2]].last_begin_scheduling = rec[1]
            if rec[0] == "TRACE_END_SCHEDULING":
                if cpus[rec[2]].last_begin_scheduling is False:
                    print("Invalid record: {}", rec)
                    exit()
                scheduling_overheads.append(int(rec[1]) - int(cpus[rec[2]].last_begin_scheduling))
                cpus[rec[2]].last_begin_scheduling = False
                
            if rec[0] == "TRACE_BEGIN_REQUEST_PREEMPTION":
                cpus[rec[2]].last_begin_request_signal = rec[1]
                
            if rec[0] == "TRACE_END_REQUEST_PREEMPTION":
                latency = int(rec[1]) - int(cpus[rec[2]].last_begin_request_signal)
                request_overheads.append(latency)
                
            if rec[0] == "TRACE_SEND_PREEMPTION_SIGNAL":
                cpus[rec[2]].pending_signal.put(rec[1])
                    
            if rec[0] == "TRACE_BEGIN_PREEMPTION":
                if(not cpus[rec[2]].pending_signal.empty()):
                    latency = int(rec[1]) - int(cpus[rec[2]].pending_signal.get())
                    signal_latencies.append(latency)
                
                cpus[rec[2]].preemption_overhead.append(rec[1])
                    
            if rec[0] == "TRACE_SWITCH_TO":
                if len(cpus[rec[2]].preemption_overhead) > 0:
                    latency = int(rec[1]) - int(cpus[rec[2]].preemption_overhead.pop())
                    context_switch_overheads.append(latency)
                
            if rec[0] == "TRACE_END_PREEMPTION":
                latency = int(rec[1]) - int(cpus[rec[2]].preemption_overhead.pop())
                context_complete_preempt_time.append(latency)
                
            if rec[0] == "TRACE_BEGIN_SWITCH_AWAY":
                cpus[rec[2]].preemption_back.append(rec[1])
                
            if rec[0] == "TRACE_BEGIN_SWITCH_AWAY":
                latency = int(rec[1]) - int(cpus[rec[2]].preemption_back.pop())
                context_preempt_back.append(latency)
                
            if rec[0] == "TRACE_BEGIN_GET_TASK":
                cpus[rec[2]].get_task.append(rec[1])
                
            if rec[0] == "TRACE_END_GET_TASK":
                if len(cpus[rec[2]].get_task) > 0:
                    latency = int(rec[1]) - int(cpus[rec[2]].get_task.pop())
                    get_task_latency.append(latency)
                
            if rec[0] == "TRACE_BEGIN_ACTIVATE_TASK":
                cpus[rec[2]].act_task.append(rec[1])
                cpus[rec[2]].num_releases += 1
                
            if rec[0] == "TRACE_END_ACTIVATE_TASK":
                if len(cpus[rec[2]].act_task) > 0:
                    latency = int(rec[1]) - int(cpus[rec[2]].act_task.pop())
                    act_task_latency.append(latency)
                
            if rec[0] == "TRACE_BEGIN_FINALIZE_TASK":
                cpus[rec[2]].fin_task.append(rec[1])
                
            if rec[0] == "TRACE_END_FINALIZE_TASK":
                if len(cpus[rec[2]].fin_task) > 0:
                    latency = int(rec[1]) - int(cpus[rec[2]].fin_task.pop())
                    fin_task_latency.append(latency)
                
            if rec[0] == "TRACE_EVENT_LATENCY":
                if cpus[rec[2]].event_latency is False: #forget about the first one, as we just started
                    cpus[rec[2]].event_latency = rec[1]
                else:
                    event_latency.append(int(rec[1]))
            line = line + 1


        measurements = {
            "Scheduling Overheads": 
                {"min": listmin(scheduling_overheads), 
                "avg": listavg(scheduling_overheads), 
                "nbmeasure": len(scheduling_overheads),
                "max": listmax(scheduling_overheads)},
            "Context 'Switch To' Overheads": 
                {"min": listmin(context_switch_overheads), 
                "avg": listavg(context_switch_overheads), 
                "nbmeasure": len(context_switch_overheads),
                "max": listmax(context_switch_overheads)},
            "Preemption Signal Latencies": 
                {"min": listmin(signal_latencies), 
                "avg": listavg(signal_latencies), 
                "nbmeasure": len(signal_latencies),
                "max": listmax(signal_latencies)},
            "Request Overheads": 
                {"min": listmin(request_overheads), 
                "avg": listavg(request_overheads), 
                "nbmeasure": len(request_overheads),
                "max": listmax(request_overheads)},
            "Complete preemption handler time when no switch context": 
                {"min": listmin(context_complete_preempt_time), 
                "avg": listavg(context_complete_preempt_time), 
                "nbmeasure": len(context_complete_preempt_time),
                "max": listmax(context_complete_preempt_time)},
            "Time after switching back": 
                {"min": listmin(context_preempt_back), 
                "avg": listavg(context_preempt_back), 
                "nbmeasure": len(context_preempt_back),
                "max": listmax(context_preempt_back)},
            "Latency to get a task from the queue": 
                {"min": listmin(get_task_latency), 
                "avg": listavg(get_task_latency), 
                "nbmeasure": len(get_task_latency),
                "max": listmax(get_task_latency)},
            "Latency to activate a task after getting it": 
                {"min": listmin(act_task_latency), 
                "avg": listavg(act_task_latency), 
                "nbmeasure": len(act_task_latency),
                "max": listmax(act_task_latency)},
            "Latency to finalize a task before looking for a new one": 
                {"min": listmin(fin_task_latency), 
                "avg": listavg(fin_task_latency), 
                "nbmeasure": len(fin_task_latency),
                "max": listmax(fin_task_latency)},
            "Event latencies, time overhead between push in ready queue and actual period activation time": 
                {"min": listmin(event_latency), 
                "avg": listavg(event_latency), 
                "nbmeasure": len(event_latency),
                "max": listmax(event_latency)},
        }
       
        self.measurements = {
            "Task overhead": {
                "file": filename,
                "min": sched_time_min
                        +get_task_min
                        +getmin(measurements["Latency to activate a task after getting it"]["min"])
                        +getmin(measurements["Latency to finalize a task before looking for a new one"]["min"]),
                "avg": sched_time_avg
                        +get_task_avg
                        +measurements["Latency to activate a task after getting it"]["avg"]
                        +measurements["Latency to finalize a task before looking for a new one"]["avg"],
                "nbmeasure": 1,
                "max": sched_time_max
                        +get_task_max
                        +measurements["Latency to activate a task after getting it"]["max"]
                        +measurements["Latency to finalize a task before looking for a new one"]["max"]
            }
        }
#        if len(records) > 0:
#            print(filename)
#            print("\tSched time: "+str(sched_time_max))
#            print("\tLatency to get a task from the queue: "+str(measurements["Latency to get a task from the queue"]["max"]))
#            print("\tLatency to activate a task after getting it: "+str(measurements["Latency to activate a task after getting it"]["max"]))
#            print("\tLatency to finalize a task before looking for a new one: "+str(measurements["Latency to finalize a task before looking for a new one"]["max"]))

class TraceMeasurementsMollison:
    
    def __init__(self, inputtracefile):
        
        self.measurements = {}

        records = self.extract_records(inputtracefile)
        self.extract_data(records, inputtracefile)
        
    def get_measurements(self):
        return self.measurements

    def extract_records(self, filename):
        raw_records = []
        subprocess.check_call(["sort -k2 " + filename + " -o " + filename], shell=True)
        file = open(filename,"r")
        raw_records += file.readlines()
        file.close()
    
        # Create the records array
        # Each entry in records is an array of the values from a single original record
        # An index for that record is also included as the last value in each record
        records = []
        for i,rec in enumerate(raw_records, start=1):
            nonewline = rec[:-1]
            lst = nonewline.split(" ")
            lst.append(i)
            records.append(lst)
        return records

    def extract_data(self, records, filename):
        # Core data model for the class
        release_overheads = []
        scheduling_overheads = []
        context_switch_overheads = []
        event_latencies = []
        signal_latencies = []
        request_overheads = []
        
        # Populate the data model
        cpus = {}
        line = 1
        
        for rec in records:
            if rec[2] not in cpus:
                cpus[rec[2]] = CpuStateMollison()
            if len(rec) >= 4 and rec[3] not in cpus:
                cpus[rec[3]] = CpuStateMollison()
            if rec[0] == "BEGIN_RELEASE_HANDLER":
                if cpus[rec[2]].last_begin_release_handler is not False:
                    print(line, " Invalid record: {}", rec, " -- ", cpus[rec[2]].last_begin_release_handler)
                    exit()
                cpus[rec[2]].last_begin_release_handler = rec[1]
                cpus[rec[2]].num_releases += 1
            if rec[0] == "END_RELEASE_HANDLER":
                if cpus[rec[2]].last_begin_release_handler is False:
                    print("Invalid record: {}", rec)
                    exit()
                latency = int(rec[1]) - int(cpus[rec[2]].last_begin_release_handler)
                release_overheads.append(latency)
                #self.append_outliers(rec,latency,999)
                cpus[rec[2]].last_begin_release_handler = False
            if rec[0] == "BEGIN_REQUEST_SIGNAL":
                if cpus[rec[2]].last_begin_request_signal is not False:
                    print("Invalid record: {}", rec)
                    exit()
                cpus[rec[2]].last_begin_request_signal = rec[1]
            if rec[0] == "END_REQUEST_SIGNAL":
                if cpus[rec[2]].last_begin_request_signal is False:
                    print("Invalid record: {}", rec)
                    exit()
                latency = int(rec[1]) - int(cpus[rec[2]].last_begin_request_signal)
                request_overheads.append(latency)
                cpus[rec[2]].last_begin_request_signal = False
            if rec[0] == "EVENT_LATENCY":
                    latency = int(rec[3])
                    event_latencies.append(latency)
            if rec[0] == "BEGIN_SCHEDULING":
                if cpus[rec[2]].last_begin_scheduling is not False:
                    print("Invalid record: {}", rec)
                    pass
                cpus[rec[2]].last_begin_scheduling = rec[1]
            if rec[0] == "END_SCHEDULING":
                if cpus[rec[2]].last_begin_scheduling is False:
                    print("Invalid record: {}", rec)
                    exit()
                scheduling_overheads.append(int(rec[1]) - int(cpus[rec[2]].last_begin_scheduling))
                cpus[rec[2]].last_begin_scheduling = False
            if rec[0] == "SWITCH_AWAY":
                if cpus[rec[2]].last_switch_away != "empty":
                    print(line, "Invalid record: {}", rec)
                else:
                    cpus[rec[2]].last_switch_away = rec[1]
            if rec[0] == "SWITCH_TO":
                if cpus[rec[2]].last_switch_away == "empty":
                    cpus[rec[2]].pending_signal = False
                elif cpus[rec[2]].last_switch_away == "start_state":
                    pass
                else:
                    latency = int(rec[1]) - int(cpus[rec[2]].last_switch_away)
                    context_switch_overheads.append(latency)
                cpus[rec[2]].last_switch_away = "empty"
            if rec[0] == "SEND_SIGNAL":
                if cpus[rec[3]].pending_signal is False:
                    cpus[rec[3]].pending_signal = rec[1]
            if rec[0] == "RECEIVE_SIGNAL":
                if cpus[rec[2]].pending_signal is not False:
                    latency = int(rec[1]) - int(cpus[rec[2]].pending_signal)
                    signal_latencies.append(latency)
                    cpus[rec[2]].pending_signal = False
            line = line + 1


        measurements = {
            "Scheduling Overheads": 
                {"min": listmin(scheduling_overheads), 
                "avg": listavg(scheduling_overheads), 
                "nbmeasure": len(scheduling_overheads),
                "max": listmax(scheduling_overheads)},
            "Context 'Switch To' Overheads": 
                {"min": listmin(context_switch_overheads), 
                "avg": listavg(context_switch_overheads), 
                "nbmeasure": len(context_switch_overheads),
                "max": listmax(context_switch_overheads)},
            "Preemption Signal Latencies": 
                {"min": listmin(signal_latencies), 
                "avg": listavg(signal_latencies), 
                "nbmeasure": len(signal_latencies),
                "max": listmax(signal_latencies)},
            "Request Overheads": 
                {"min": listmin(request_overheads), 
                "avg": listavg(request_overheads), 
                "nbmeasure": len(request_overheads),
                "max": listmax(request_overheads)},
            "Event latencies, time overhead between push in ready queue and actual period activation time": 
                {"min": listmin(event_latencies), 
                "avg": listavg(event_latencies), 
                "nbmeasure": len(event_latencies),
                "max": listmax(event_latencies)},
            "Release handler": 
                {"min": listmin(release_overheads), 
                "avg": listavg(release_overheads), 
                "nbmeasure": len(release_overheads),
                "max": listmax(release_overheads)},
        }
        self.measurements = {
            "Task overhead": {
                "file": filename,
                "min": measurements["Release handler"]["min"]
                        +measurements["Request Overheads"]["min"]
                        +measurements["Preemption Signal Latencies"]["min"]
                        +measurements["Scheduling Overheads"]["min"],
                "avg": measurements["Release handler"]["avg"]
                        +measurements["Request Overheads"]["avg"]
                        +measurements["Preemption Signal Latencies"]["avg"]
                        +measurements["Scheduling Overheads"]["avg"],
                "nbmeasure": 1,
                "max": measurements["Release handler"]["max"]
                        +measurements["Request Overheads"]["max"]
                        +measurements["Preemption Signal Latencies"]["max"]
                        +measurements["Scheduling Overheads"]["max"]
            }
        }

class Stats:

    def __init__(self, dir, filepattern, data_extractor):
        self.bycores = {}
        self.bytasks = {}
        self.byutil = {}
        self.all = {}
        
        for f in glob.glob(dir+"/"+filepattern+"*.trace"):
#            print(f)
            data = data_extractor(f)
            tmp = os.path.basename(f)
            tmp = tmp[len(filepattern):len(tmp)-len(".trace")]
            tmp = tmp.split("_")
            idtaskset = tmp[0]
            nbcore = tmp[1]
            nbtask = tmp[2]
            utilisation = tmp[3]
#            if data.get_measurements()["Task overhead"]["min"] == 0:
#                print("min is zero for ", f)
#            if data.get_measurements()["Task overhead"]["max"] == 0:
#                print("max is zero for ", f)
#            if data.get_measurements()["Task overhead"]["avg"] == 0:
#                print("avg is zero for ", f)
            self.append(data.get_measurements(), idtaskset, nbcore, nbtask, utilisation)
#             print(f,": id=", idtaskset, " nbcore=", nbcore, " nbtask=", nbtask, " util=", utilisation)
            self.all[f] = data.get_measurements()
            
        self.find_pathological_cases()
    
    def get_bycores(self):
        return self.bycores
    def get_bytasks(self):
        return self.bytasks
    def get_byutil(self):
        return self.byutil
        
    def min(self, stat, data, key):
        if data[key]["min"] < stat[key]["min"]:
            stat[key]["min"] = data[key]["min"]
            stat[key]["minfile"] = data[key]["file"]
            
    def max(self, stat, data, key):
        if data[key]["max"] > stat[key]["max"]:
            stat[key]["max"] = data[key]["max"]
            stat[key]["maxfile"] = data[key]["file"]
            
    def avg(self, stat, data, key):
        nbmeasure = stat[key]["nbmeasure"] + data[key]["nbmeasure"]
        if nbmeasure > 0:
            stat[key]["avg"] = (stat[key]["nbmeasure"] * stat[key]["avg"] + data[key]["nbmeasure"] * data[key]["avg"]) / nbmeasure
            stat[key]["nbmeasure"] = nbmeasure

    def append(self, data, idtaskset, nbcore, nbtask, utilisation):
        if not nbcore in self.bycores:
            self.bycores[nbcore] = {}
        if not nbtask in self.bytasks:
            self.bytasks[nbtask] = {}
        if not utilisation in self.byutil:
            self.byutil[utilisation] = {}
            
        for k in data:
            if not k in self.bycores[nbcore]:
                self.bycores[nbcore][k] = {"min": sys.maxsize, "avg": 0, "nbmeasure": 0, "max": 0, "minfile": "", "maxfile": ""}
            self.min(self.bycores[nbcore], data, k)
            self.avg(self.bycores[nbcore], data, k)
            self.max(self.bycores[nbcore], data, k)
        for k in data:
            if not k in self.bytasks[nbtask]:
                self.bytasks[nbtask][k] = {"min": sys.maxsize, "avg": 0, "nbmeasure": 0, "max": 0, "minfile": "", "maxfile": ""}
            self.min(self.bytasks[nbtask], data, k)
            self.avg(self.bytasks[nbtask], data, k)
            self.max(self.bytasks[nbtask], data, k)
        for k in data:
            if not k in self.byutil[utilisation]:
                self.byutil[utilisation][k] = {"min": sys.maxsize, "avg": 0, "nbmeasure": 0, "max": 0, "minfile": "", "maxfile": ""}
            self.min(self.byutil[utilisation], data, k)
            self.avg(self.byutil[utilisation], data, k)
            self.max(self.byutil[utilisation], data, k)
            
    def find_pathological_cases(self):
        threshold = 500000
        for f in self.all:
            if threshold < self.all[f]["Task overhead"]["max"]:
                print("Threshold over: ", self.all[f]["Task overhead"]["max"], " case ", f)

def to_plot(policy, platforms, measurements, label, xlabel, column_labels):
    f = open(dir+policy+"_"+label+".dat", "w")
    f.write("0")
    for platform in platforms:
        f.write(" "+column_labels[platform]) #/avg
        f.write(" "+column_labels[platform]+"/min")
        f.write(" "+column_labels[platform]+"/max")
    for platform in platforms:
        f.write(" "+column_labels[platform]+"/minfile")
        f.write(" "+column_labels[platform]+"/maxfile")
    f.write("\n")
    xtics = {}
    listxtic = list(measurements.keys())
#     listxtic.sort()
    listxtic = sorted(listxtic, key=float)
    counter = 1
    for r in listxtic:
        xtics[counter] = r;
        counter += 1
        
    for k in xtics:
        r = xtics[k]
        f.write(str(k)+" ")
        for platform in platforms:
            if platform in measurements[r]:
                f.write(str(int(measurements[r][platform]["Task overhead"]["avg"]))+" ")
                f.write(str(int(measurements[r][platform]["Task overhead"]["min"]))+" ")
                f.write(str(int(measurements[r][platform]["Task overhead"]["max"]))+" ")
            else:
                f.write("- - - ")
        for platform in platforms:
            if platform in measurements[r]:
                f.write(os.path.basename(measurements[r][platform]["Task overhead"]["minfile"])+" ")
                f.write(os.path.basename(measurements[r][platform]["Task overhead"]["maxfile"])+" ")
            else:
                f.write("- - ")
        f.write("\n")
    f.close()
    
    sxtics = ""
    if len(xtics) > 0:
        for k in xtics:
            sxtics += "'"+str(xtics[k])+"' "+str(k)+","
        sxtics = "("+sxtics[0:len(sxtics)-1]+")"
    print(dir+policy+"_"+label+".png")
    print(sxtics)
    
    df = pd.read_csv(dir+policy+"_"+label+'.dat', index_col = 0, sep=' ', comment='#')
    g = gnuplot.Gnuplot(log=True)
    g.set(
        terminal = 'pngcairo transparent font "arial,10" fontscale 1.0 size 600, 400',
        output = "'"+dir+policy+"_"+label+".png'",
        key = "outside right center Left",
        format='y "%.0f"',
        ylabel="'Overhead'",
        xlabel="'"+xlabel+"'",
        xtics=sxtics+" nomirror out rotate by -45  autojustify",
        ytics="nomirror out",
        logscale="y",
        size="1, 1",
        datafile="missing '-'"
    )
    g.plot_data(df, "using ($1):(1*100000):17 linetype -3 notitle,\
                    for [i=2:"+str(len(column_labels)*3)+":3] '' using ($1+i*0.01):i:i+1:i+2 with yerrorbars title columnhead(i+1),\
                    ''    using ($1+1):(1*100000):17 linetype -3 notitle")
    
def populate(stats, prefix, platform, bycores, bytasks, byutil):
    for r in stats.get_bycores():
        if not r in bycores:
            bycores[r] = {}
        bycores[r][prefix+"-"+platform] = stats.get_bycores()[r]
    for r in stats.get_bytasks():
        if not r in bytasks:
            bytasks[r] = {}
        bytasks[r][prefix+"-"+platform] = stats.get_bytasks()[r]
    for r in stats.get_byutil():
        if not r in byutil:
            byutil[r] = {}
        byutil[r][prefix+"-"+platform] = stats.get_byutil()[r]
    
if __name__ == "__main__":
#     dir = "/home/brouxel/Projects/TeamPlay/TeamPlay_Tools/CoordinationTools/CoordLibvsMollison/"
    dir = "/home/brouxel/Projects/TeamPlay/TeamPlay_Tools/CoordinationTools/CoordLibvsMollison/ECRTS/"
    
        # Start the table, and describe the columns
    
#     platforms = ("big",  "LITTLE", "tk1", "tx2", "tx2_denver")
    platforms = []
    platforms.append("odroid-big")
#     policies = ("gedf", "pedf", "gfp", "pfp", "off")
    policies = ("gedf", "pedf", "gfp", "pfp", "off")

    extract_mollison = lambda a: TraceMeasurementsMollison(a)
    extract_coord = lambda a: TraceMeasurements(a)
    
    column_labels = {
#         "coord-big": "OBC", "coord-LITTLE": "OLC", "mol-big": "OBM", "mol-LITTLE": "OLM",
#         "coord-tk1": "AKC", "mol-tk1": "AKM",
#         "coord-tx2": "NXC", "mol-tx2": "NXM",
#         "coord-tx2_denver": "NDC", "mol-tx2_denver": "NDM",
        "coord-odroid-big": "OBC", "mol-odroid-big": "OBM"
    }
    
    for policy in policies:
        bycores = {}
        bytasks = {}
        byutil = {}
        platform = "odroid-big"
        stats = Stats(dir+platform, "coord_"+policy+"_taskset_", extract_coord)
        populate(stats, "coord", platform, bycores, bytasks, byutil)
            
        if policy == "gedf":
            stats = Stats(dir+platform, "mol_taskset_", extract_mollison)
            populate(stats, "mol", platform, bycores, bytasks, byutil)
        
#         to_plot(policy, list(column_labels.keys()), bycores, "bycores", "Number of cores", column_labels)
        to_plot(policy, list(column_labels.keys()), bytasks, "bytasks", "Number of Tasks", column_labels)
        to_plot(policy, list(column_labels.keys()), byutil, "byutil", "Utilisation", column_labels)
        
    
    column_labels = {
        "coord-gedf": "G-EDF", "coord-pedf": "P-EDF", "coord-gfp": "G-FP", "coord-pfp": "P-FP", "coord-off": "Off-Line"
    }
    bycores = {}
    bytasks = {}
    byutil = {}
    for policy in policies:
        if policy == "":
            continue
        stats = Stats(dir+"big", "coord_"+policy+"_taskset_", extract_coord)
        populate(stats, "coord", policy, bycores, bytasks, byutil)
    
    to_plot("big", list(column_labels.keys()), bycores, "bycores", "Number of cores", column_labels)
    to_plot("big", list(column_labels.keys()), bytasks, "bytasks", "Number of Tasks", column_labels)
    to_plot("big", list(column_labels.keys()), byutil, "byutil", "Utilisation", column_labels)
