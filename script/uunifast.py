#!/usr/bin/python
"""
Tools for generating task sets.
Copied and Adapted from https://github.com/MaximeCheramy/simso/blob/master/simso/generator/task_generator.py
"""

import numpy as np
import random
import math
import sys
import itertools
import os.path 

sys.path.append(r'/home/brouxel/Projects/Software/drs/drs')

from drs import drs

def gen_utilisation(n, u, nsets):
    utilisations = []
    lower_bounds = [0.01 for i in range(0,n)]
    for i in range(0, nsets):
        utilisations.append(drs(n, u, None, lower_bounds))
#         print("===> ", u, ": ", utilisations[i])
    return utilisations

def next_arrival_poisson(period):
    return -math.log(1.0 - random.random()) * period


def gen_arrivals(period, min_, max_, round_to_int=False):
    def trunc(x, p):
        return int(x * 10 ** p) / float(10 ** p)

    dates = []
    n = min_ - period
    while True:
        n += next_arrival_poisson(period) + period
        if round_to_int:
            n = int(round(n))
        else:
            n = trunc(n, 6)
        if n > max_:
            break
        dates.append(n)
    return dates


def gen_periods_loguniform(n, nsets, min_, max_, round_to_int=False):
    """
    Generate a list of `nsets` sets containing each `n` random periods using a
    loguniform distribution.

    Args:
        - `n`: The number of tasks in a task set.
        - `nsets`: Number of sets to generate.
        - `min_`: Period min.
        - `max_`: Period max.
    """
    periods = np.exp(np.random.uniform(low=np.log(min_), high=np.log(max_),
                                       size=(nsets, n)))
    if round_to_int:
        return np.rint(periods).tolist()
    else:
        return periods.tolist()


def gen_periods_uniform(n, nsets, min_, max_, round_to_int=False):
    """
    Generate a list of `nsets` sets containing each `n` random periods using a
    uniform distribution.

    Args:
        - `n`: The number of tasks in a task set.
        - `nsets`: Number of sets to generate.
        - `min_`: Period min.
        - `max_`: Period max.
    """
    periods = np.random.uniform(low=min_, high=max_, size=(nsets, n))

    if round_to_int:
        return np.rint(periods).tolist()
    else:
        return periods.tolist()


def gen_periods_discrete(n, nsets, periods):
    """
    Generate a matrix of (nsets x n) random periods chosen randomly in the
    list of periods.

    Args:
        - `n`: The number of tasks in a task set.
        - `nsets`: Number of sets to generate.
        - `periods`: A list of available periods.
    """
    try:
        return np.random.choice(periods, size=(nsets, n)).tolist()
    except AttributeError:
        # Numpy < 1.7:
        p = np.array(periods)
        return p[np.random.randint(len(p), size=(nsets, n))].tolist()


def gen_tasksets(utilizations, periods):
    """
    Take a list of task utilization sets and a list of task period sets and
    return a list of couples (c, p) sets. The computation times are truncated
    at a precision of 10^-10 to avoid floating point precision errors.

    Args:
        - `utilization`: The list of task utilization sets. For example::

            [[0.3, 0.4, 0.8], [0.1, 0.9, 0.5]]
        - `periods`: The list of task period sets. For examples::

            [[100, 50, 1000], [200, 500, 10]]

    Returns:
        For the above example, it returns::

            [[(30.0, 100), (20.0, 50), (800.0, 1000)],
             [(20.0, 200), (450.0, 500), (5.0, 10)]]
    """
    def trunc(x, p):
        return int(x * 10 ** p) / float(10 ** p)

    return [[(trunc(ui * pi, 6), trunc(pi, 6)) for ui, pi in zip(us, ps)]
            for us, ps in zip(utilizations, periods)]
        
def genTaskName(length=4, characters='ABCDEFGHIJKLMNOPQRSTUVWXYZ'):
    for s in itertools.product(characters, repeat=length):
        yield ''.join(s)            

def get_prefix(deadline_monotonic, partitioned, offline):
    prefix = "gedf"
    if partitioned:
        prefix = "pedf"
    if deadline_monotonic:
        prefix = "gfp"
        if partitioned:
           prefix = "pfp"
    if offline:
        prefix = "off"
    return prefix

def gen_coord_file(outputdir, compiler, arch, mainMapping, tasksets, nbcores, cores, exp_time_sec, nbtasks, util):
    for taskset in tasksets:
        namegen = genTaskName()
        fcoord = open(outputdir+"/gen_taskset_"+str(tasksets.index(taskset))+"_"+str(nbcores)+"_"+str(nbtasks)+"_"+str(util)+".tey", "w")
        fcoord.write("app gen"+str(tasksets.index(taskset))+" {\n")
        fcoord.write("components {\n")
        for task in taskset:
            fcoord.write("\ttask"+next(namegen)+" {\n")
            fcoord.write("\t\tperiod="+str(task[1])+"ms;\n")
            fcoord.write("\t\tdeadline="+str(task[1])+"ms;\n")
            fcoord.write("\t\tversions {\n")
            fcoord.write("\t\t\ta {\n")
            fcoord.write("\t\t\tWCET="+str(task[0])+"ms;\n")
            fcoord.write("\t\t\t"+'cname="work_loop";'+"\n")
            fcoord.write("\t\t\t"+'csignature="int wcet '+str(task[0])+'";'+"\n")
            fcoord.write("\t\t}}\n")
            fcoord.write("\t}\n")
        fcoord.write("}}")
        fcoord.close()
        
def gen_config_file(outputdir, compiler, arch, mainMapping, tasksets, nbcores, cores, exp_time_sec, nbtasks, util, prefix):
    for taskset in tasksets:
        fconfig = open(outputdir+"/gen_"+prefix+"_taskset_"+str(tasksets.index(taskset))+"_"+str(nbcores)+"_"+str(nbtasks)+"_"+str(util)+".xml", "w")
        fconfig.write('<?xml version="1.0" encoding="UTF-8" ?>'+"\n")
        fconfig.write("<appl>\n")
        fconfig.write("\t<config>\n")
        fconfig.write("\t\t<architecture>\n")
        fconfig.write("\t\t\t<processors>\n")
        for idcore in range(nbcores):
            fconfig.write("\t\t\t\t"+'<processor id="C'+str(idcore)+'" system-id="'+str(cores[idcore])+'" />'+"\n")
        fconfig.write("\t\t\t</processors>\n")
        fconfig.write("\t\t</architecture>\n")
        fconfig.write("\t</config>\n")
        if prefix == "pedf" or prefix == "pfp":
            fconfig.write("\t"+'<partition id="greedy" />'+"\n")
        if prefix == "off":
            fconfig.write("\t"+'<scheduler solver="fls" type="ideal" allow_migration="false" timeout="300" solve="true" />'+"\n")
        fconfig.write("\t"+'<generator id="yasmin" channel-type="shared-memory"'+"\n")
        fconfig.write("\t\t"+'yasminpath="~/Projects/TeamPlay/TeamPlay_Tools/CoordinationTools/CoordRuntime"'+"\n")
        fconfig.write("\t\t"+'language="c" compiler="'+compiler+'" linker-opts="" compiler-opts="-I'+outputdir+'/ -fno-stack-protector -O0"'+"\n")
        if len(arch) > 0:
            fconfig.write("\t\t"+'arch="'+arch+'"'+"\n")
        fconfig.write("\t\t"+'includes="work.h" additional-srcs="'+outputdir+'/work.c"'+"\n")
        fconfig.write("\t\t"+'execuntilreturn="yas_utils_nanosleep((unsigned long long)'+str(exp_time_sec)+'*1000000000l)"'+"\n")
        fconfig.write("\t\t"+'preemptive="true" main-mapping="'+str(mainMapping)+'" trace="true"'+"\n")
        fconfig.write("\t/>\n")
        fconfig.write("</appl>\n")
        fconfig.close()
        
def gen_mollison_file(outputdir, tasksets, nbcores, cores, nbtasks, util):
    for taskset in tasksets:
        namegen = genTaskName()
        f = open(outputdir+"/gen_taskset_"+str(tasksets.index(taskset))+"_"+str(nbcores)+"_"+str(nbtasks)+"_"+str(util)+".mollison", "w")
        f.write(str(nbcores)+"\n")
        for idcore in range(nbcores):
            f.write(str(cores[idcore])+" ")
        f.write("\n")
        for task in taskset:
            f.write("0 "+str(task[0])+" "+str(task[1])+" "+str(task[1])+"\n")
        f.close()
        
def generate_launch(outputdir, mollisonbin, maxnbcores, maxnbtasks, tasksets_msec, nbsets, cycles_per_ns, iterations_per_ms, exp_time_sec, prefix):
    nbtestcases = -1
    for nbcores in range(2,maxnbcores+1,1):
        for Knbtasks in range(10, maxnbtasks+2, 2):
            for util in np.arange(0.2, nbcores+0.2, 0.2):
                if len(str(util)) <= 3:
                    nbtestcases += 1
    nbtestcases *= nbsets
    
    flaunch = open(outputdir+"/Makefile."+prefix, "w")
    flaunch.write("all:\n")
    flaunch.write("\techo 'mol tp build coord stats print'\n\n")
    
    if prefix == "gedf":
        flaunch.write("mol: ")
        for nbcores in range(2,maxnbcores+1,1):
            for Knbtasks in range(10, maxnbtasks+2, 2):
                nbtasks = nbcores*Knbtasks
                for util in np.arange(0.2, nbcores+0.2, 0.2):
                    if len(str(util)) > 3:
                        continue
                    for taskset in tasksets_msec:
                        flaunch.write("mol_"+prefix+"_taskset_"+str(tasksets_msec.index(taskset))+"_"+str(nbcores)+"_"+str(nbtasks)+"_"+str(util)+".trace ")
        flaunch.write("\n\n")
        
        i = 0
        for nbcores in range(2,maxnbcores+1,1):
            for Knbtasks in range(10, maxnbtasks+2, 2):
                nbtasks = nbcores*Knbtasks
                for util in np.arange(0.2, nbcores+0.2, 0.2):
                    if len(str(util)) > 3:
                        continue
                    for taskset in tasksets_msec:
                        flaunch.write("\t"+'echo "=====================> '+str(int(i*100/nbtestcases))+'%\r"'+"\n")
                        flaunch.write("mol_"+prefix+"_taskset_"+str(tasksets_msec.index(taskset))+"_"+str(nbcores)+"_"+str(nbtasks)+"_"+str(util)+".trace: \n")
                        flaunch.write("\txargs ")
                        flaunch.write(" < gen_"+prefix+"_taskset_"+str(tasksets_msec.index(taskset))+"_"+str(nbcores)+"_"+str(nbtasks)+"_"+str(util)+".mollison")
                        flaunch.write(" sudo "+mollisonbin)
                        flaunch.write(" "+str(cycles_per_ns)+" "+str(iterations_per_ms)+" "+str(exp_time_sec))
                        flaunch.write(" > mol_"+prefix+"_taskset_"+str(tasksets_msec.index(taskset))+"_"+str(nbcores)+"_"+str(nbtasks)+"_"+str(util)+".trace\n")
                        i += 1
    flaunch.write("\n")
                
    flaunch.write("tp: ")
    for nbcores in range(2,maxnbcores+1,1):
        for Knbtasks in range(10, maxnbtasks+2, 2):
            nbtasks = nbcores*Knbtasks
            for util in np.arange(0.2, nbcores+0.2, 0.2):
                if len(str(util)) > 3:
                    continue
                for taskset in tasksets_msec:
                    flaunch.write("mol_taskset_"+str(tasksets_msec.index(taskset))+"_"+str(nbcores)+"_"+str(nbtasks)+"_"+str(util)+".trace ")
    flaunch.write("\n\n")
    
    i = 0
    for nbcores in range(2,maxnbcores+1,1):
        for Knbtasks in range(10, maxnbtasks+2, 2):
            nbtasks = nbcores*Knbtasks
            for util in np.arange(0.2, nbcores+0.2, 0.2):
                if len(str(util)) > 3:
                    continue
                for taskset in tasksets_msec:
                    flaunch.write("\t"+'echo "=====================> '+str(int(i*100/nbtestcases))+'%\r"'+"\n")
                    flaunch.write("mol_taskset_"+str(tasksets_msec.index(taskset))+"_"+str(nbcores)+"_"+str(nbtasks)+"_"+str(util)+".trace: \n")
                    flaunch.write("\txargs ")
                    flaunch.write(" < gen_taskset_"+str(tasksets_msec.index(taskset))+"_"+str(nbcores)+"_"+str(nbtasks)+"_"+str(util)+".mollison")
                    flaunch.write(" sudo "+mollisonbin)
                    flaunch.write(" "+str(cycles_per_ns)+" "+str(iterations_per_ms)+" "+str(exp_time_sec))
                    flaunch.write(" > mol_taskset_"+str(tasksets_msec.index(taskset))+"_"+str(nbcores)+"_"+str(nbtasks)+"_"+str(util)+".trace\n")
                    i += 1
    flaunch.write("\n")
                
    flaunch.write("tp: ")
    for prefix in ("gedf", "gfp", "pedf", "pfp", "off"):
        for nbcores in range(2,maxnbcores+1,1):
            for Knbtasks in range(2, maxnbtasks+2, 2):
                nbtasks = nbcores*Knbtasks
                for util in np.arange(0.2, nbcores+0.2, 0.2):
                    if len(str(util)) > 3:
                        continue
                    for taskset in tasksets_msec:
                        tmp = str(int(util*10))
                        if len(tmp) == 1:
                            tmp = "0"+tmp
                        flaunch.write("codegen/gen"+prefix+"taskset"+str(tasksets_msec.index(taskset))+str(nbcores)+str(nbtasks)+tmp+"-yasmin/yasmin_config.h ")
    flaunch.write("\n\n")
    for prefix in ("gedf", "gfp", "pedf", "pfp", "off"):
        for nbcores in range(2,maxnbcores+1,1):
            for Knbtasks in range(2, maxnbtasks+2, 2):
                nbtasks = nbcores*Knbtasks
                for util in np.arange(0.2, nbcores+0.2, 0.2):
                    if len(str(util)) > 3:
                        continue
                    for taskset in tasksets_msec:
                        tmp = str(int(util*10))
                        if len(tmp) == 1:
                            tmp = "0"+tmp
                        flaunch.write("codegen/gen"+prefix+"taskset"+str(tasksets_msec.index(taskset))+str(nbcores)+str(nbtasks)+tmp+"-yasmin/yasmin_config.h: \n")
                        flaunch.write("\tcp gen_taskset_"+str(tasksets_msec.index(taskset))+"_"+str(nbcores)+"_"+str(nbtasks)+"_"+str(util)+".tey gen_"+prefix+"_taskset_"+str(tasksets_msec.index(taskset))+"_"+str(nbcores)+"_"+str(nbtasks)+"_"+str(util)+".tey\n")
                        flaunch.write("\t~/Projects/TeamPlay/TeamPlay_Tools/CoordinationTools/compiler/build/dist/methane")
                        flaunch.write(" --config gen_"+prefix+"_taskset_"+str(tasksets_msec.index(taskset))+"_"+str(nbcores)+"_"+str(nbtasks)+"_"+str(util)+".xml")
                        flaunch.write(" --coord gen_"+prefix+"_taskset_"+str(tasksets_msec.index(taskset))+"_"+str(nbcores)+"_"+str(nbtasks)+"_"+str(util)+".tey\n")
                        flaunch.write("\trm gen_"+prefix+"_taskset_"+str(tasksets_msec.index(taskset))+"_"+str(nbcores)+"_"+str(nbtasks)+"_"+str(util)+".tey\n")
                        if prefix == "gfp" or prefix == "pfp":
                            flaunch.write("\tsed -i -e 's/YAS_TASK_PRIORITY_EDF/YAS_TASK_PRIORITY_DM/' $@\n")
    flaunch.write("\n")
                
    flaunch.write("build: ")
    for nbcores in range(2,maxnbcores+1,1):
        for Knbtasks in range(10, maxnbtasks+2, 2):
            nbtasks = nbcores*Knbtasks
            for util in np.arange(0.2, nbcores+0.2, 0.2):
                if len(str(util)) > 3:
                    continue
                for taskset in tasksets_msec:
                    tmp = str(int(util*10))
                    if len(tmp) == 1:
                        tmp = "0"+tmp
                    flaunch.write("build/"+prefix+"_taskset_"+str(tasksets_msec.index(taskset))+str(nbcores)+str(nbtasks)+str(int(util*10))+"/gen"+prefix+"taskset"+str(tasksets_msec.index(taskset))+str(nbcores)+str(nbtasks)+tmp+" ")
    flaunch.write("\n")
    for nbcores in range(2,maxnbcores+1,1):
        for Knbtasks in range(10, maxnbtasks+2, 2):
            nbtasks = nbcores*Knbtasks
            for util in np.arange(0.2, nbcores+0.2, 0.2):
                if len(str(util)) > 3:
                    continue
                for taskset in tasksets_msec:
                    tmp = str(int(util*10))
                    if len(tmp) == 1:
                        tmp = "0"+tmp
                    flaunch.write("build/"+prefix+"_taskset_"+str(tasksets_msec.index(taskset))+str(nbcores)+str(nbtasks)+str(int(util*10))+"/gen"+prefix+"taskset"+str(tasksets_msec.index(taskset))+str(nbcores)+str(nbtasks)+tmp+":\n")
                    flaunch.write("\tmkdir -p build/"+prefix+"_taskset_"+str(tasksets_msec.index(taskset))+str(nbcores)+str(nbtasks)+str(int(util*10))+"\n")
                    flaunch.write("\tpushd build/"+prefix+"_taskset_"+str(tasksets_msec.index(taskset))+str(nbcores)+str(nbtasks)+str(int(util*10))+" ; \\\n")
                    tmp = str(int(util*10))
                    if len(tmp) == 1:
                        tmp = "0"+tmp
                    if prefix == "gfp" or prefix == "pfp":
                        flaunch.write("\tsed -i -e 's/COORD_TASK_PRIORITY_EDF/COORD_TASK_PRIORITY_DM/' ../../codegen/gen"+prefix+"taskset"+str(tasksets_msec.index(taskset))+str(nbcores)+str(nbtasks)+tmp+"-yasmin/config.h ; \\\n")
                    flaunch.write("\tcmake ../../codegen/gen"+prefix+"taskset"+str(tasksets_msec.index(taskset))+str(nbcores)+str(nbtasks)+tmp+"-yasmin ; \\\n")
                    flaunch.write("\tmake ; \\\n")
                    flaunch.write("\tpopd\n")
    flaunch.write("\n")
                
    flaunch.write("coord: ")
    for nbcores in range(2,maxnbcores+1,1):
        for Knbtasks in range(10, maxnbtasks+2, 2):
            nbtasks = nbcores*Knbtasks
            for util in np.arange(0.2, nbcores+0.2, 0.2):
                if len(str(util)) > 3:
                    continue
                for taskset in tasksets_msec:
                    tmp = str(int(util*10))
                    if len(tmp) == 1:
                        tmp = "0"+tmp
                    flaunch.write("coord_"+prefix+"_taskset_"+str(tasksets_msec.index(taskset))+"_"+str(nbcores)+"_"+str(nbtasks)+"_"+str(util)+".trace ")
    flaunch.write("\n\n")
    i = 0
    for nbcores in range(2,maxnbcores+1,1):
        for Knbtasks in range(10, maxnbtasks+2, 2):
            nbtasks = nbcores*Knbtasks
            for util in np.arange(0.2, nbcores+0.2, 0.2):
                if len(str(util)) > 3:
                    continue
                for taskset in tasksets_msec:
                    tmp = str(int(util*10))
                    if len(tmp) == 1:
                        tmp = "0"+tmp
                    flaunch.write("coord_"+prefix+"_taskset_"+str(tasksets_msec.index(taskset))+"_"+str(nbcores)+"_"+str(nbtasks)+"_"+str(util)+".trace:\n")
                    flaunch.write("\t@echo '=====================> "+str(int(i*100/nbtestcases))+"%'\n")
                    flaunch.write("\ttimeout 30s build/"+prefix+"_taskset_"+str(tasksets_msec.index(taskset))+str(nbcores)+str(nbtasks)+str(int(util*10))+"/gen"+prefix+"taskset"+str(tasksets_msec.index(taskset))+str(nbcores)+str(nbtasks)+tmp)
                    flaunch.write(" > coord_"+prefix+"_taskset_"+str(tasksets_msec.index(taskset))+"_"+str(nbcores)+"_"+str(nbtasks)+"_"+str(util)+".trace\n")
                    i += 1
    flaunch.write("\n")
    
#     maxint = sys.maxsize * 2 + 1
#     flaunch.write("stats: \n")
#     flaunch.write("\techo '[[0, "+str(maxint)+", 0, 0], [0, "+str(maxint)+", 0, 0], [0, "+str(maxint)+", 0, 0], [0, "+str(maxint)+", 0, 0], [0, "+str(maxint)+", 0, 0], [0, "+str(maxint)+", 0, 0], [0, "+str(maxint)+", 0, 0], [0, "+str(maxint)+", 0, 0]]' > mol.stats\n")
#     flaunch.write("\tfor f in $$(ls mol_*.trace) ; do\\\n")
#     flaunch.write("\t\tpython ~/Projects/C-C++/MollisonUserSpaceSched/script/stats stats $$f >/dev/null ; \\\n")
#     flaunch.write("\t\tpython ~/Projects/C-C++/MollisonUserSpaceSched/script/stats merge mol.stats $${f%.trace}.stats >/dev/null ; \\\n")
#     flaunch.write("\tdone\n")
#     flaunch.write("\techo '[[0, "+str(maxint)+", 0, 0], [0, "+str(maxint)+", 0, 0], [0, "+str(maxint)+", 0, 0], [0, "+str(maxint)+", 0, 0], [0, "+str(maxint)+", 0, 0], [0, "+str(maxint)+", 0, 0], [0, "+str(maxint)+", 0, 0],[0, "+str(maxint)+", 0, 0],[0, "+str(maxint)+", 0, 0],[0, "+str(maxint)+", 0, 0]]' > coord.stats\n")
#     flaunch.write("\tfor f in $$(ls coord_*.trace) ; do\\\n")
#     flaunch.write("\t\tpython ~/Projects/TeamPlay/TeamPlay_Tools/CoordinationTools/CoordRuntime/script/stats.py stats $$f >/dev/null ; \\\n")
#     flaunch.write("\t\tpython ~/Projects/TeamPlay/TeamPlay_Tools/CoordinationTools/CoordRuntime/script/stats.py merge coord.stats $${f%.trace}.stats >/dev/null ; \\\n")
#     flaunch.write("\tdone\n")
#     
#     flaunch.write("print: \n")
#     flaunch.write("\techo '************** MOLLISON *************'\n")
#     flaunch.write("\tpython ~/Projects/C-C++/MollisonUserSpaceSched/script/stats print mol.stats\n")
#     flaunch.write("\techo ''")
#     flaunch.write("\techo '************** COORDINATION *************'\n")
#     flaunch.write("\tpython ~/Projects/TeamPlay/TeamPlay_Tools/CoordinationTools/CoordRuntime/script/stats.py print coord.stats\n")
#     flaunch.write("\techo ''\n")
    
    flaunch.close()
    
def generate_c_work_files(outputdir, iterations_per_ms):
    #From Mollison main.c file
    fc = open(outputdir+"/work.c", "w")
    fc.write('#include "work.h"'+"\n")
    fc.write("int iterations_per_ms="+str(iterations_per_ms)+";\n")
    fc.write("void do_work_one_ms() {\n")
    fc.write("\tvolatile int nums[4096];\n")
    fc.write("\tfor (int i=0; i < iterations_per_ms; i++) {\n")
    fc.write("\t\tfor (int j=0; j < 4096; j++) {\n")
    fc.write("\t\t\tnums[j] += 1;\n")
    fc.write("\t\t\tnums[j] = nums[j] * 77;\n")
    fc.write("\t\t}\n")
    fc.write("\t}\n")
    fc.write("}\n")
    fc.write("void work_loop(int param) {\n")
    fc.write("\tfor (int i=0; i < param; i++)\n")
    fc.write("\t\tdo_work_one_ms();\n")
    fc.write("}\n")
    fc.close()
    
    fh = open(outputdir+"/work.h", "w")
    fh.write("void work_loop(int);\n")
    fh.close()

def main(argv):
    nbsets = 5
    minperiod = 1
    maxperiod = 50
    maxnbtasks = 40
    exp_time_sec = 30
    
# intel my machine
#    nbsets = 1
#    maxnbcores = 2
#    syscoresid = [4,5,6,7]
#    minperiod = 1
#    maxperiod = 50
#    maxnbtasks = 10
#    exp_time_sec = 2
#    iterations_per_ms = 410
#    mainMapping = 1
#    outputdir = "/tmp/plop"
#    compiler = "/usr/bin/gcc"   
#    mollisonbin = "~/Projects/C-C++/MollisonUserSpaceSched/main"
#    arch = ""
    
# xu4 big cores
    maxnbcores = 3
    syscoresid = [5,6,7]
    iterations_per_ms = 130
    mainMapping = 4
    outputdir = "/home/brouxel/Projects/TeamPlay/TeamPlay_Tools/CoordinationTools/CoordLibvsMollison/ECRTS/odroid-big"
    compiler = "/home/brouxel/Projects/Software/cross-compile/odroid/gcc-linaro-7.4.1-2019.02-x86_64_arm-linux-gnueabihf/bin/arm-linux-gnueabihf-gcc"
    mollisonbin = "/home/odroid/Mollison/main"
    arch = "arm"
  
# xu4 LITTLE cores
#    maxnbcores = 4
#    syscoresid = [0,1,2,3]
#    iterations_per_ms = 25
#    mainMapping = 4
#    outputdir = "/home/brouxel/Projects/TeamPlay/TeamPlay_Tools/CoordinationTools/CoordLibvsMollison/LITTLE"
#    compiler = "/home/brouxel/Projects/Software/cross-compile/odroid/gcc-linaro-7.4.1-2019.02-x86_64_arm-linux-gnueabihf/bin/arm-linux-gnueabihf-gcc"
#    mollisonbin = "/home/odroid/Mollison/main"
#    arch = "arm"
    
# TK1 cores
#     maxnbcores = 2
#     syscoresid = [2,3]
#     iterations_per_ms = 49
#     mainMapping = 1
#     outputdir = "/home/brouxel/Projects/TeamPlay/TeamPlay_Tools/CoordinationTools/CoordLibvsMollison/tk1"
#     compiler = "/home/brouxel/Projects/Software/cross-compile/odroid/gcc-linaro-7.4.1-2019.02-x86_64_arm-linux-gnueabihf/bin/arm-linux-gnueabihf-gcc"
#     mollisonbin = "/home/ubuntu/Mollison/main"
#     arch = "arm"

# TX2 denver cores
#    maxnbcores = 2
#    syscoresid = [1,2]
#    iterations_per_ms = 192
#    mainMapping = 3
#    outputdir = "/home/brouxel/Projects/TeamPlay/TeamPlay_Tools/CoordinationTools/CoordLibvsMollison/tx2_denver"
#    compiler = "/home/brouxel/Projects/Software/cross-compile/gcc-linaro-7.3.1-2018.05-x86_64_aarch64-linux-gnu/bin/aarch64-linux-gnu-gcc"
#    mollisonbin = "/home/tx2/Mollison/main"
#    arch = "aarch64"
    
# TX2 other cores
#    maxnbcores = 3
#    syscoresid = [3,4,5]
#    iterations_per_ms = 180
#    mainMapping = 1
#    outputdir = "/home/brouxel/Projects/TeamPlay/TeamPlay_Tools/CoordinationTools/CoordLibvsMollison/tx2"
#    compiler = "/home/brouxel/Projects/Software/cross-compile/gcc-linaro-7.3.1-2018.05-x86_64_aarch64-linux-gnu/bin/aarch64-linux-gnu-gcc"
#    mollisonbin = "/home/tx2/Mollison/main"
#    arch = "aarch64"
    
    
    cycles_per_ns = 1
    
    if not os.path.exists(outputdir):
        os.makedirs(outputdir)
        
    policies = ("gedf", "pedf", "pfp", "gfp", "off")
    
    for nbcores in range(2,maxnbcores+1,1):
        for Knbtasks in range(10, maxnbtasks+2, 2):
            nbtasks = nbcores*Knbtasks
            for util in np.arange(0.2, nbcores+0.2, 0.2):
                utilisation = gen_utilisation(nbtasks,util,nbsets)
                periods = gen_periods_uniform(nbtasks, nbsets, minperiod, maxperiod, True)
                tasksets_sec = gen_tasksets(utilisation, periods)
                tasksets_msec = [[(int(C), int(T)) for C, T in tasksets_sec[i]]
                    for i in range(nbsets)]
                for prefix in policies: 
                    gen_coord_file(outputdir, compiler, arch, mainMapping, tasksets_msec, nbcores, syscoresid, exp_time_sec, nbtasks, util, prefix)
                gen_mollison_file(outputdir, tasksets_msec, nbcores, syscoresid, nbtasks, util, "gedf")
    
    for prefix in policies: 
        generate_launch(outputdir, mollisonbin, maxnbcores, maxnbtasks, tasksets_msec, nbsets, cycles_per_ns, iterations_per_ms, exp_time_sec, prefix)
    generate_c_work_files(outputdir, iterations_per_ms)
    
    
main(sys.argv)