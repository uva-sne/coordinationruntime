# Copyright (C) 2020  Benjamin Rouxel, University of Amsterdam
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

if(NOT EXISTS ${YAS_RUNTIME_APP_PATH}/yasmin_config.h)
    message(FATAL_ERROR "YAS_RUNTIME_APP_PATH: \"${YAS_RUNTIME_APP_PATH}\" is not well set, it should point to the directory where the yasmin_config.h of the app is located")
endif()

#Mostly useful for the test as it allows to build the library multiple times under different names depending on the test configuration
if(NOT YAS_CUSTOM_LIB_NAME)
    set(YAS_CUSTOM_LIB_NAME yasmin_runtime_core)
endif()

enable_language(C ASM)

set(OLD_CMAKE_CURRENT_SOURCE_DIR ${CMAKE_CURRENT_SOURCE_DIR})
set(CMAKE_CURRENT_SOURCE_DIR ${CMAKE_CURRENT_LIST_DIR}/../)

message("-- change current source dir ${OLD_CMAKE_CURRENT_SOURCE_DIR} -> ${CMAKE_CURRENT_SOURCE_DIR}")

# Configure the Debug build type
set(CMAKE_C_FLAGS_DEBUG "${CMAKE_C_FLAGS_DEBUG} -D_GNU_SOURCE -fno-stack-protector -g -Og -D_DEBUG")
set(CMAKE_C_FLAGS_RELEASE "${CMAKE_C_FLAGS_RELEASE} -D_GNU_SOURCE -fno-stack-protector")
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS_RELEASE} --std=c99")

set(CMAKE_ASM_FLAGS_DEBUG "${CMAKE_C_FLAGS_DEBUG}")
set(CMAKE_ASM_FLAGS_RELEASE "${CMAKE_C_FLAGS_RELEASE}")
set(CMAKE_ASM_FLAGS "${CMAKE_ASM_FLAGS_RELEASE}")

# The cmake/ directory includes a few macros for finding architecture specificities
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_CURRENT_SOURCE_DIR}/cmake/")

# Generate list of define "#DEC_X" where X is between 0 and YAS_NB_ITER_MAX_FOR_PP_LOOP
if(NOT YAS_NB_ITER_MAX_FOR_PP_LOOP)
    set(YAS_NB_ITER_MAX_FOR_PP_LOOP 1024)
endif()
if(NOT EXISTS "${CMAKE_CURRENT_BINARY_DIR}/pp-loop-dec.h")
    execute_process(
        COMMAND /bin/bash -c "echo '#define DEC_0 0' ; for i in $(seq 1 ${YAS_NB_ITER_MAX_FOR_PP_LOOP}) ; do echo '#define DEC_'$(expr $i)' '$(expr $i - 1) ; done"
        OUTPUT_VARIABLE VarPPLoopDecContent
    )
    file(WRITE "${CMAKE_CURRENT_BINARY_DIR}/pp-loop-dec.h" "${VarPPLoopDecContent}")
endif()

# Find C source files.
file(GLOB_RECURSE YAS_RUNTIME_SOURCES 
        ${CMAKE_CURRENT_SOURCE_DIR}/lib/src/*.c 
        ${CMAKE_CURRENT_SOURCE_DIR}/lib/arch/*.c 
        ${CMAKE_CURRENT_SOURCE_DIR}/lib/arch/*/*.c 
        ${CMAKE_CURRENT_SOURCE_DIR}/lib/os/*/*.c)

string(FIND "${CMAKE_SYSTEM_PROCESSOR}" "arm" TARGET_ARCH)
if(NOT ${TARGET_ARCH} EQUAL -1)
    message("-- Include arm assembly")
    file(GLOB_RECURSE YAS_RUNTIME_SOURCES_ASM
        ${CMAKE_CURRENT_SOURCE_DIR}/lib/arch/arm/*.S)
elseif(${CMAKE_SYSTEM_PROCESSOR} STREQUAL "aarch64")
    message("-- Include aarch64 assembly")
    file(GLOB_RECURSE YAS_RUNTIME_SOURCES_ASM
        ${CMAKE_CURRENT_SOURCE_DIR}/lib/arch/aarch64/*.S)
elseif(${CMAKE_SYSTEM_PROCESSOR} STREQUAL "x86_64")
    message("-- Include x86/x86_64 assembly")
    file(GLOB_RECURSE YAS_RUNTIME_SOURCES_ASM
        ${CMAKE_CURRENT_SOURCE_DIR}/lib/arch/x86/*.S)
else()
    message("-- Include noarch assembly -- if incorrect try to set CMAKE_SYSTEM_PROCESSOR - current is: ${CMAKE_SYSTEM_PROCESSOR}")
    file(GLOB_RECURSE YAS_RUNTIME_SOURCES_ASM
        ${CMAKE_CURRENT_SOURCE_DIR}/lib/arch/noarch/*.S)
endif()


set_source_files_properties(${CMAKE_CURRENT_SOURCE_DIR}/lib/src/utils.c  PROPERTIES COMPILE_FLAGS -Wno-pointer-arith)
set_source_files_properties(${CMAKE_CURRENT_SOURCE_DIR}/lib/src/task_pool.c PROPERTIES COMPILE_FLAGS "-Wno-discarded-qualifiers -Wno-incompatible-pointer-types")

# Define a library that contains most of the important methane code.
add_library(${YAS_CUSTOM_LIB_NAME} ${YAS_RUNTIME_SOURCES} ${YAS_RUNTIME_SOURCES_ASM})

find_library(LIBRT rt) 
if(LIBRT)
  target_link_libraries(${YAS_CUSTOM_LIB_NAME} PUBLIC ${LIBRT})
endif()

set(THREADS_PREFER_PTHREAD_FLAG ON)
find_package(Threads REQUIRED)
if(THREADS_HAVE_PTHREAD_ARG)
  target_compile_options(${YAS_CUSTOM_LIB_NAME} PUBLIC "-pthread")
endif()
if(CMAKE_THREAD_LIBS_INIT)
  target_link_libraries(${YAS_CUSTOM_LIB_NAME} PUBLIC "${CMAKE_THREAD_LIBS_INIT}")
endif()

# Add all necessary include directories.
target_include_directories(${YAS_CUSTOM_LIB_NAME} PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/lib/include/)
target_include_directories(${YAS_CUSTOM_LIB_NAME} PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/lib/arch/)
target_include_directories(${YAS_CUSTOM_LIB_NAME} PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/lib/os/)
target_include_directories(${YAS_CUSTOM_LIB_NAME} PUBLIC ${YAS_RUNTIME_APP_PATH}/) #for yasmin_config.h
target_include_directories(${YAS_CUSTOM_LIB_NAME} PUBLIC ${CMAKE_CURRENT_BINARY_DIR}/) #for pp-loop-dec.h

set_target_properties(
    ${YAS_CUSTOM_LIB_NAME} PROPERTIES
    PUBLIC_HEADER ${CMAKE_CURRENT_SOURCE_DIR}/lib/include/
)

set(CMAKE_CURRENT_SOURCE_DIR ${OLD_CMAKE_CURRENT_SOURCE_DIR})
