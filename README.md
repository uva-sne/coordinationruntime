# YASMIN: Yet Another Scheduler MIddleware for heterogeNeous COTS platforms

_yasmin_ is a middleware  to schedule end-user applications with real-time 
requirements in user space and on behalf of the operating system. YASMIN provides 
an easy-to-use programming interface and portability. It treats heterogeneity on 
COTS heterogeneous embedded platforms as a first-class citizen: It supports 
multiple functionally equivalent task implementations with distinct extra-functional 
behaviour. This enables the system designer to quickly explore different scheduling 
policies and task-to-core mappings, and thus, to improve overall system performance.

## Dependencies

_yasmin_ can be built to run on any POSIX compliant system. It doesn't require
any specific dependencies.

## Usage

For an example checkout the "Hello World" application in rt_apps/hello_world.

To use _yasmin_ you need to create at least 3 files for your application:
- CMakeLists.txt: compilation instruction for CMake
- main.c: a file containing the main function
- yasmin_config.h: contains pre-procecssor definition that will configure the library.

### CMake

A minima, the `CMakeFileLists.txt' must contain:

''' 
set(YAS_RUNTIME_APP_PATH "${CMAKE_SOURCE_DIR}") #which points to the folder where the yasmin_config.h is
find_package(yasmin_runtime_core REQUIRED)

add_executable(${CMAKE_TARGET_NAME} ${YAS_RUNTIME_APP_PATH}/hello_world.c) # list all C files require for the compilation
target_link_libraries(${CMAKE_TARGET_NAME} PUBLIC yasmin_runtime_core) # link your application with the library
'''

### main.c

You can name your main file as you want (e.g. hello_world.c for the Hello World example)..
This file should contain at least the main function that initialises the library, 
declares the tasks ... .

'''
#include "yasmin_config.h"
#include "yasmin_api.h"

#define MS2NS(X)  (X*1000000l)

static void mySuperTask(void *) {
    //do something meaningfull
}

int main(int, char**) {

    // Declare a task and set its parameters
    yas_task_data_t td;
    yas_task_id_t tid;
    
    yas_build_task_name(td.name, "My Super Task");
    hwtd.period = MS2NS(500); //500ms
    
    // Initialise _yasmin_
    yas_init();
    
    // Declare the task to the scheduler
    hwtid = yas_task_decl(&td, mySuperTask, NULL);
    // Start the scheduler
    yas_start();
    // wait for some event, it's also possible to wait for a key press if you prefer
    yas_utils_nanosleep(S2NS(2));
    // Stop the scheduler
    yas_stop();
    
    // Do some cleanup
    yas_cleanup();

    return (EXIT_SUCCESS);
}

## Compilation

_yasmin_ compilation is heavily dependent on the application it will be further
linked to via a configuration file with the hardcoded name `yasmin_config.h'. 
Hence, the library cannot be compiled on its own. 

'''
$ cd toProject
$ mkdir build
$ cd build
$ cmake ..
$ make
$ ./target

## Troubleshooting

### Threads are not created

By default threads are created in the system using the SCHED_FIFO. This can be
a problem if your user doesn't have the permission. To resolve it, either run
the application with root (or sudo), or try to set the capabilities of your user
to user RT scheduling algorithm (don't remember how). Also grant the rights to
use RT priorities to your user in /etc/limits.conf.